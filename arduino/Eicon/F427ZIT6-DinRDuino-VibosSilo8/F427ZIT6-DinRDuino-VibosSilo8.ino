#include <IWatchdog.h>
#include <EEPROM.h>
#include <Wire.h>
#include <time.h>
#include <LiquidCrystal_I2C.h>

#define HAVE_HWSERIAL1
#define HAVE_HWSERIAL2
#define HAVE_HWSERIAL3
#include <HardwareSerial.h>
#include <HardwareTimer.h>

//#define SER1_RX_PIN  97 // PC7 // 77 // PD8 // PD0
//#define SER1_TX_PIN  96 // PC6 // 76 // PD9 // PD1
//HardwareSerial Serial1(SER1_RX_PIN, SER1_TX_PIN);   // RX, TX
HardwareSerial Serial1(USART6);           // USART6 Remote Serial port
//HardwareSerial   Serial1(USART3);         // USART3 FTDI Serial port
HardwareSerial serialPort = Serial1;

#define TIMER_DAC_OUT TIM4
//  static stimer_t TimHandle;

#define BAUD_RATE        57600
//#define BAUD_RATE       9600

#define SPEED_DIVIDER   1                                 // Used to effect the speed at run-time
                                                          // A value of 1 means normal speed
#define FF_CPU          1000000 / SPEED_DIVIDER

// ----------------------------------------------------------------------------------------------------------
// Used to retrieve the CPU UID during startup --------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
uint16_t flashsize;
uint32_t UnitID_Part_1;
// ----------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------
// Connected optional hardware
// ----------------------------------------------------------------------------------------------------------
//#define CHUTE_AND_LOCK_CONNECTED
// ----------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------
// Testing Values (simulates button presses)-----------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------

// Simulates the Mode switch being in Auto Mode
//#define AUTOTEST

// Simulates the Mode switch being in LOCAL Mode
//#define LOCALTEST

// Simulates the Mode switch being in REMOTE Mode
#define REMOTETEST

// If this is enabled, 
// the remote application will need to constantly update the timeout value, 
// in order for the Unit to stay in REMOTE mode.
#define REMOTETIMEOUT     

#define OPEN_CHUTE_TEST

// ----------------------------------------------------------------------------------------------------------
// Definition of Plant (ie how many Silos and their names) --------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define NBR_SILOS                       6
const char *SILO_NAME[NBR_SILOS+1]      {"One  ", "Two  ", "Three", "Four ", "Five ", "Six  ", "None "};
const char *SILO_STATE[3]               {"Off", "Pick", "Hold"};
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// General use bits -----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define OFF                             0
#define ON                              1
const char *ON_OFF_ARRAY[2]             {"OFF", "ON"};
#define LED_OFF                         0
#define LED_ON                          1
#define SW_OFF                          1
#define SW_ON                           0
#define BLINK                           2
#define BLINK_SLOW                      2
#define RELEASE                         1
#define HOLD                            2
#define PICK                            3
#define OPEN                            HIGH
#define CLOSED                          LOW
#define UNLOCKED                        HIGH
#define LOCKED                          LOW
const char ledAsciiLookup[3]            {' ', '*', '#'};
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// Default Config Values ------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define            DEFAULT_SWEEP_TIME                       12.5
#define            DEFAULT_SILO_TIME                         5 // 20 // 2 * 60.0
#define            DEFAULT_SILO_PAUSE_TIME                  25 // 30 * 60.0 - DEFAULT_SILO_TIME * NBR_SILOS
#define            DEFAULT_INVERT_CH2_AUTO                  ON
#define            DEFAULT_INVERT_CH2_LOCAL                 ON
#define            DEFAULT_PATTERN_NBR_REMOTE               9
const unsigned int DEFAULT_PATTERN_NBR_AUTO_LIST[]        = { 2,7,10,11,'#' };
#define            DEFAULT_PATTERN_NBR_AUTO                 DEFAULT_PATTERN_NBR_AUTO_LIST[0] // 6 // 13
const unsigned int DEFAULT_PATTERN_NBR_AUTO_LIST_SIZE     = sizeof(DEFAULT_PATTERN_NBR_AUTO_LIST);
unsigned int       DEFAULT_PATTERN_NBR_AUTO_LIST_POS      = 0;
unsigned int       sweepArrayPatternNbrAutoList[10];
unsigned int       sweepArrayPatternNbrAutoListSize       = 0;
unsigned int       sweepArrayPatternNbrAutoListPos        = 0;

const unsigned int DEFAULT_PATTERN_NBR_LOCAL_LIST[]       = { 1, 12,13,'#' };
#define            DEFAULT_PATTERN_NUMBER_LOCAL             DEFAULT_PATTERN_NBR_LOCAL_LIST[0] // 9 // 10
const unsigned int DEFAULT_PATTERN_NBR_LOCAL_LIST_SIZE    = sizeof(DEFAULT_PATTERN_NBR_LOCAL_LIST);
unsigned int       DEFAULT_PATTERN_NBR_LOCAL_LIST_POS     = 0;
unsigned int       sweepArrayPatternNbrLocalList[10];
unsigned int       sweepArrayPatternNbrLocalListSize      = 0;
unsigned int       sweepArrayPatternNbrLocalListPos       = 0;

#define            DEFAULT_CYLCE_PATTERN_AUTO               ON
#define            DEFAULT_CYLCE_PATTERN_LOCAL              ON
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// PICK&HOLD Event Timing -----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// How long to use PICK current before moving to hold current 
// when opperating solanoids.
#define PICK_TIME                       0.5
#define PICK_DURATION                   PICK_TIME           * FF_CPU
//-----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// RAMP Event Timing ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Time taken to ramp to full output
// and time to ramp from full to none
#define RAMP_TIME                       1.0
#define RAMP_DURATION                   RAMP_TIME           * FF_CPU
//-----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// EVENTS ------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Switch ON/OFF Default Scheduled events. Comment an event to dissable it.
// Opperational events.
#define BLINK_EVENT                     // Turn this on to enable blinking LEDs
#define BLINK_BUITLIN_EVENT             // Turn this on to enable blinking builtin LEDs
#define SERIAL_EVENT                    // Turn this on to enable Serial commands
#define DIO_READ_EVENT                  // Turn this on enable reading the Digital Input states
#define DIO_WRITE_EVENT                 // Turn this on enable writing the Digital Output states
#define SILO_EVENT                      // Turn this on to enable cycling through Silos
#define SILO_PAUSE_EVENT                // Turn this on to enable a pause between cycles when in Auto mode
unsigned int siloPauseEvent = ON;
unsigned int openChuteSiloPauseEvent = OFF;
#define SWEEP_STEP_EVENT                // Turn this on to enable srtepping through the sweep steps
#define INTERUPT_OUTPUT_EVENT           // Turn this on to enable fast Interupt driven DAC output
//#define OUTPUT_EVENT                  // Turn this on to enable Slow Schedule driven DAC output - For debugging

// Watchdog related events
#define TICKLE_WATCHDOG_EVENT           // Turn this ON to tickle the watchdog at regular intervals
//#define TRIP_WATCHDOG_EVENT           // Turn this ON to intentionally trip the watchdog

// Logging related events - Startup state - These can be overriden by Serial commands
//#define LOG_PIN_ASSIGNMENTS           // Log the PIN assignments during Startup      
//#define LOG_LCD_EVENT                 // Log status to the LCD
//#define LOG_STATUS_EVENT              // Log the general status
//#define LOG_PICK_AND_HOLD_EVENT       // Log the Pick & Hold states
//#define LOG_LED_EVENT                 // Log the LED states
//#define LOG_DIO_EVENT                 // Log the Digital IO states
//#define LOG_DAC_OUTPUT_EVENT          // Log the DAC output
//#define LOG_DAC_OUTPUT_RT             // Log the DAC output in realtime
//#define LOG_SWEEP_STATUS              // Log sweep statuspro
//#define LOG_SWEEP_STEP_STATUS         // Log sweep step status
//#define LOG_SILO_STATUS               // Log the Silo status
//#define LOG_MONITOR_EVENT             // Log data-points for the Arduino Plotter
//#define REMOTE_STATUS_EVENT           // Turn on status updates for the control room application


// ----------------------------------------------------------------------------------------------------------
// Chute and Chute-Testing Event Timing ---------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define OPEN_CHUTE_TIME               2.0                                 // How long to vibrate a Silo with an open chute.
#define OPEN_CHUTE_DURATION           OPEN_CHUTE_TIME          * FF_CPU

#define OPEN_CHUTE_TEST_TIME_ON       10  // 13.0                          // How long to open the chute during testing
#define OPEN_CHUTE_TEST_DURATION_ON   OPEN_CHUTE_TEST_TIME_ON  * FF_CPU

#define OPEN_CHUTE_TEST_TIME_OFF      10 // 38.0                          // How long to close the chute during testing
#define OPEN_CHUTE_TEST_DURATION_OFF  OPEN_CHUTE_TEST_TIME_OFF * FF_CPU

const unsigned int  minAfterChuteVibrationTime  = 4;                      // Minumum time to bother vibrating a Silo after a chute closes

unsigned int        chuteTestSilo         = 5;
unsigned long       openChuteTime;
unsigned long       openChuteTestTimeOn;
unsigned long       openChuteTestTimeOff;


// ----------------------------------------------------------------------------------------------------------
// SERIAL ---------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define PORT1_RX_PIN  PD9
#define PORT1_TX_PIN  PD8
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// INPUT PINs -----------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define SW_MODE_LOCAL_PIN                        PD4  // J05-01 14U                    // Input
#define SW_MODE_LOCAL_PIN_NAME                  "PD4"
#define SW_MODE_AUTO_PIN                         PD5  // J05-02 15U                    // Input
#define SW_MODE_AUTO_PIN_NAME                   "PD5"
#define SW_VIB_1_PIN                             PD3 // PD11 // J10-06 45L                    // Input
#define SW_VIB_1_PIN_NAME                       "PD3" // "PD11"
#define SW_VIB_2_PIN                             PD2 //PG15 // J10-03 42L                    // Input
#define SW_VIB_2_PIN_NAME                       "PG2" // "PG15"

const byte SW_SILO_PIN[NBR_SILOS]               {  PB2,   PA0,   PC3,   PC2,   PC12,    PD10  };   // Input
const char *SW_SILO_PIN_NAME[NBR_SILOS]         { "PB2", "PA0", "PC3", "PC2", "PC12",  "PD10" };
const byte SW_CHUTE_PIN[NBR_SILOS]              {  PE1,   PB9,   PB7,   PB5,   PG12,   PD7   };   // Input
const char *SW_CHUTE_PIN_NAME[NBR_SILOS]        { "PE1", "PB9", "PB7", "PB5", "PG12", "PD7"  };
const byte SW_CHUTE_LOCK_PIN[NBR_SILOS]         {  PE0,   PB8,   PB6,   PB4,   PG9,    PD6   };   // Input
const char *SW_CHUTE_LOCK_PIN_NAME[NBR_SILOS]   { "PE0", "PB8", "PB6", "PB4", "PG9",  "PD6"  };
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// OUTPUT PINs ----------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define CPU_LED_PIN_1                            PD12  // Indicate Running Condition. 
#define CPU_LED_PIN_1_NAME                      "PD12" // Blink once per second under normal conditions
#define CPU_LED_PIN_2                            PD13  // Blink 10 times a second to indicate 
#define CPU_LED_PIN_2_NAME                      "PD13" // a Watchdog has caused a reboot.

#define CPU_LED_PIN_3                            PD14  // Spare CPU Status LEDs
#define CPU_LED_PIN_3_NAME                      "PD14" // Used to indicate Watchdow has been tripped
#define CPU_LED_PIN_4                            PD15
#define CPU_LED_PIN_4_NAME                      "PD15"

#define AIR_SOL_PIN                              PC10                                  // Output
#define AIR_SOL_PIN_NAME                        "PC10"
#define AIR_REG_1_PIN                            PF8   // PC8                          // Output
#define AIR_REG_1_PIN_NAME                      "Pf8"  // "PC8"
#define AIR_REG_2_PIN                            PF10  // PA12                         // Output
#define AIR_REG_2_PIN_NAME                      "PF10" // "PA12"
#define VIB_LED_1_PIN                            PC8   // PD11                         // Output
#define VIB_LED_1_PIN_NAME                      "PC8"  // "PD11"
#define VIB_LED_2_PIN                            PC9   // PG15                         // Output
#define VIB_LED_2_PIN_NAME                      "PC9"  // "PG15"
#define LOCAL_LED_PIN                            PA8                                   // Output
#define LOCAL_LED_PIN_NAME                      "PA8"
#define AUTO_LED_PIN                             PA15                                  // Output
#define AUTO_LED_PIN_NAME                       "PA15"

const byte  SILO_LED_PIN[NBR_SILOS]                  {  PG7,   PG6,   PG5,   PG4,   PG3,   PC11  }; // Output
const char *SILO_LED_PIN_NAME[NBR_SILOS]             { "PG7", "PG6", "PG5", "PG4", "PG3", "PC11" };

const byte  AIR_MANIFOLD_1_PICK_PIN[NBR_SILOS]       {  PE2,   PE4,   PE6,   PF2,   PF4,   PF6   }; // Output - Silos 1 through 6, Vibrator 1
const char *AIR_MANIFOLD_1_PICK_PIN_NAME[NBR_SILOS]  { "PE2", "PE4", "PE6", "PF2", "PF4", "PF6"  };
const byte  AIR_MANIFOLD_1_HOLD_PIN[NBR_SILOS]       {  PE3,   PE5,   PC13,  PF3,   PF5,   PF7   }; // Output
const char *AIR_MANIFOLD_1_HOLD_PIN_NAME[NBR_SILOS]  { "PE3", "PE5", "PC13","PF3", "PF5", "PF7"  };
const byte  AIR_MANIFOLD_2_PICK_PIN[NBR_SILOS]       {  PF11,  PF13,  PF15,  PG1,   PE8,   PE10  }; // Output - Silos 1 through 6, Vibrator 2
const char *AIR_MANIFOLD_2_PICK_PIN_NAME[NBR_SILOS]  { "PF11","PF13","PF15","PG1", "PE8", "PE10" };
const byte  AIR_MANIFOLD_2_HOLD_PIN[NBR_SILOS]       {  PF12,  PF14,  PG0,   PE7,   PE9,   PE11  }; // Output
const char *AIR_MANIFOLD_2_HOLD_PIN_NAME[NBR_SILOS]  { "PF12","PF14","PG0", "PE7", "PE9", "PE11" };

#define ONBOARD_DAC_1                           PA4 // Not using these at the moment
#define ONBOARD_DAC_2                           PA5
// ----------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------
// Wire (I2C) interfaces
// ----------------------------------------------------------------------------------------------------------
// These lines must be added to variant.h
// to get I2C working using Wire.h
// ----------------------------------------------------------------------------------------------------------
//#define PIN_WIRE_SDA         (PF0)
//#define PIN_WIRE_SCL         (PF1)
// ----------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------
// Use TwoWire library rather than Wire to use 2 I2C interfaces:
// ----------------------------------------------------------------------------------------------------------
TwoWire Wire0(PF0, PF1);    // Onboard I2C interface
TwoWire Wire1(PB7, PB6);    // Expansion I2C interface
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// DACs -----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define DAC_MCP4725
//#define DAC_PCF8591

#ifdef DAC_MCP4725  
  #define DAC_I2C_WIRE                Wire0
  #define DAC_CMD_WRITEDAC            0x40
  #define DAC_ADDR1                   0x60
  #define DAC_ADDR2                   0x61
  #define DAC_MIN                     8
  #define DAC_MAX                     120
  #define DAC_RANGE                   DAC_MAX - DAC_MIN
  #define DAC_FREQ_MIN                9
  #define DAC_FREQ_MAX                41
  #define DAC_FREQ_RANGE              DAC_FREQ_MAX - DAC_FREQ_MIN
#else
  #define DAC_I2C_WIRE                Wire
  #define DAC_CMD_WRITEDAC            0x40
  #define DAC_ADDR1                   0x48
  #define DAC_ADDR2                   0x4A
  #define DAC_MIN                     8
  #define DAC_MAX                     250 // 225 // 120
  #define DAC_RANGE                   DAC_MAX - DAC_MIN
  #define DAC_FREQ_MIN                9
  #define DAC_FREQ_MAX                41
  #define DAC_FREQ_RANGE              DAC_FREQ_MAX - DAC_FREQ_MIN
#endif
//-----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// LCD SCREEN -----------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
#define SCREEN_I2C_WIRE               Wire
#define SCREEN_ADDR                   0x27
LiquidCrystal_I2C lcd(SCREEN_ADDR, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
// ----------------------------------------------------------------------------------------------------------


// Used to tickle event states at runtime
unsigned int logRemoteStatusEvent = OFF;
unsigned int logLcdEvent          = OFF;
unsigned int logMonitorEvent      = OFF;
unsigned int logStatusEvent       = OFF;
unsigned int logPickAndHoldEvent  = OFF;
unsigned int logLedEvent          = OFF;
unsigned int logDioEvent          = OFF;
unsigned int logDacOutputEvent    = OFF;
unsigned int logDacOutputRt       = OFF;
unsigned int logSweepStatus       = OFF;
unsigned int logSweepStepStatus   = OFF;
unsigned int logSiloStatus        = OFF;
unsigned int tickleWatchdogEvent  = ON;
unsigned int tripWatchdogEvent    = OFF;
//-----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// FIXED Event Timing ---------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
//#define TRIP_WATCHDOG_TIME            5 * 60.0 //  5 minutes
#define TRIP_WATCHDOG_TIME              10.0     // 10 seconds
#define TICKLE_WATCHDOG_TIME            01.0
#define BLINK_TIME                      00.15
#define SERIAL_TIME                     00.5
#define LOG_LCD_TIME                    00.25
#define LOG_MONITOR_TIME                00.1 // 10 times a second
#define LOG_STATUS_TIME                 01.0
#define LOG_DIO_TIME                    00.5
#define LOG_LED_TIME                    00.5
#define LOG_DAC_OUTPUT_TIME             00.2
#define DIO_READ_TIME                   00.2
#define DIO_WRITE_TIME                  01.0 // 00.2
#define OUTPUT_TIME                     01.0
#define REMOTE_STATUS_TIME              01.0 // 5 times a second // was 0.5

// Calculated durations used by event schedule
#define TRIP_WATCHDOG_DURATION          TRIP_WATCHDOG_TIME       * FF_CPU
#define TICKLE_WATCHDOG_DURATION        TICKLE_WATCHDOG_TIME / 2 * FF_CPU
#define BLINK_DURATION                  BLINK_TIME           / 2 * FF_CPU
#define SERIAL_DURATION                 SERIAL_TIME              * FF_CPU
#define LOG_LCD_DURATION                LOG_LCD_TIME             * FF_CPU
#define LOG_MONITOR_DURATION            LOG_MONITOR_TIME         * FF_CPU
#define LOG_STATUS_DURATION             LOG_STATUS_TIME          * FF_CPU
#define LOG_DIO_DURATION                LOG_DIO_TIME             * FF_CPU
#define LOG_LED_DURATION                LOG_LED_TIME             * FF_CPU
#define LOG_DAC_OUTPUT_DURATION         LOG_DAC_OUTPUT_TIME      * FF_CPU
#define DIO_READ_DURATION               DIO_READ_TIME            * FF_CPU
#define DIO_WRITE_DURATION              DIO_WRITE_TIME           * FF_CPU
#define OUTPUT_DURATION                 OUTPUT_TIME              * FF_CPU
#define REMOTE_STATUS_DURATION          REMOTE_STATUS_TIME       * FF_CPU
#define SECOND_DURATION                 1.0                      * FF_CPU
//-----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// VARIABLE Event Timing ------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
float         blinkBuiltinTime        = 0.0;
unsigned long blinkBuiltinDuration    = blinkBuiltinTime / 2     * FF_CPU;

float         siloTime                = 0.0;
unsigned long siloDuration            = siloTime                 * FF_CPU;

float         siloPauseTime           = 10.0;
unsigned long siloPauseDuration       = siloPauseTime            * FF_CPU;

float         sweepStepTime           = 0.0;
unsigned long sweepStepDuration       = sweepStepTime            * FF_CPU;

float         sweepTotalTime          = 0.0;
float         sweepTimeAllSteps       = 0.0;

float         remoteTimeoutTime       = 10;
float         remoteTimeoutDuration   = remoteTimeoutTime        * FF_CPU;
//-----------------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------------
// Used to strobe the Silo LEDs back and forth when paused---------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
#define LED_KIT_TIME                    1.0 // LOG_LED_TIME
#define LED_KIT_DURATION                LED_KIT_TIME        * FF_CPU
unsigned int ledKitSilo               = 0;
unsigned int ledKitMod                = 1;
//-----------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------
// Minumum and Maximum Sweep Time and Silo Time -------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Min and Max allowed Silo-Time, Silo-Pause-Time and Sweep-Time
#define SWEEP_TIME_MIN                  1
#define SWEEP_TIME_MAX                  60
#define SILO_TIME_MIN                   RAMP_TIME * 2 + 1
#define SILO_TIME_MAX                   99
#define SILO_PAUSE_TIME_MIN             RAMP_TIME * 2 + 1
#define SILO_PAUSE_TIME_MAX             999
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// Event timers ---------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Time since event last fired
unsigned long now;
unsigned long lastTripWatchdogHop;
unsigned long lastTickleWatchdogHop;
unsigned long lastBlinkHop;
unsigned long lastBlinkBuiltinHop;
unsigned long lastSerialHop;
unsigned long lastLogLcdHop;
unsigned long lastLogMonitorHop;
unsigned long lastLogStatusHop;
unsigned long lastLogDioHop;
unsigned long lastLogLedHop;
unsigned long lastLogDacOutputHop;
unsigned long lastDioReadHop;
unsigned long lastDioWriteHop;
unsigned long lastOutputHop;
unsigned long lastRemoteStatusHop;
unsigned long lastSweepStepHop;
unsigned long lastSweepHop;
unsigned long lastSiloHop;
unsigned long lastPickHop;
unsigned long vib1PickTime;
unsigned long vib2PickTime;
unsigned long vib1ReleaseTime;
unsigned long vib2ReleaseTime;
unsigned long vib1ReleasedTime;
unsigned long vib2ReleasedTime;
unsigned long siloPauseHop;
unsigned long lastLedKitHop;
unsigned long openChuteSiloTime = 0;
float         remoteStateStart;
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// Switch and LED states ------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
unsigned int swModeLocalState              = SW_OFF;
unsigned int swModeAutoState               = SW_OFF;
unsigned int swModeRemoteState             = SW_OFF;
unsigned int remoteState                   = OFF;
unsigned int swModeLocalStatePrevious      = SW_OFF;
unsigned int swModeAutoStatePrevious       = SW_OFF;
unsigned int swModeRemoteStatePrevious     = SW_OFF;

unsigned int swVib1State                   = SW_OFF;
unsigned int swVib2State                   = SW_OFF;
unsigned int swVibBothState                = SW_OFF;
unsigned int rmVib1State                   = SW_OFF;
unsigned int rmVib2State                   = SW_OFF;
unsigned int rmVibBothState                = SW_OFF;
unsigned int ledBlinkState                 = OFF;

byte ledBlinkBuiltinState          = OFF;

unsigned int swSiloState[NBR_SILOS]           = { SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF };
unsigned int rmSiloState[NBR_SILOS]           = { SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF };
unsigned int swSiloStatePrevious[NBR_SILOS]   = { SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF };
unsigned int rmSiloStatePrevious[NBR_SILOS]   = { SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF };
unsigned int swChuteState[NBR_SILOS]          = { CLOSED, CLOSED, CLOSED, CLOSED, CLOSED, CLOSED };
unsigned int swChuteStatePrevious[NBR_SILOS]  = { CLOSED, CLOSED, CLOSED, CLOSED, CLOSED, CLOSED };
unsigned int swChuteLockState[NBR_SILOS]      = { SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF, SW_OFF };
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// LED states -----------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
byte ledSiloState[NBR_SILOS]       = { LED_OFF, LED_OFF, LED_OFF, LED_OFF, LED_OFF, LED_OFF };
byte ledLocalState                 = 0;
byte ledAutoState                  = 0;
byte ledOffState                   = 0;
byte ledVib1State                  = 0;
byte ledVib2State                  = 0;
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// Manifold solernoids Pick & Hold states -------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
unsigned int  manifold1State[NBR_SILOS] {OFF,OFF,OFF,OFF,OFF,OFF};
unsigned int  manifold2State[NBR_SILOS] {OFF,OFF,OFF,OFF,OFF,OFF};
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// SWEEP related variables ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
float         sweepPercent;
float         sweepStepPercent;
unsigned long rampPercent;
unsigned long ramp1Percent;
unsigned long ramp2Percent;

unsigned int  sweepFrequencyRange            = 0; // 10;
unsigned int  sweepStartFrequency            = 0; // 20;
unsigned int  sweepEndFrequency              = 0; // 30;
unsigned int  sweepArrayPatternNbrRemote     = 0;
unsigned int  sweepArrayPatternNbrAuto       = 0;
unsigned int  sweepArrayPatternNbrAutoFixed  = 0;
unsigned int  sweepArrayPatternNbrLocal      = 0;
unsigned int  sweepArrayPatternNbrLocalFixed = 0;
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// SWEEP PATTERNS -------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// This is the percentage to offset the second chanel when it's inverted.
// A diferent value is required for each pattern.
double        sweepArrayPatternOffsetPercent[]  = {0.0, 9.5, -1.3, 25.4, 9.2, 30.0, 20.0, -24.0, -25.0, -25.0, -9.0, -24.0, 9.0, 5.0, 5.0, 5.0};
                                               //    0    1     2     3    4     5     6      7      8      9    10     11   12   13   14   15
// These define the patterns available
// The format is n number of elements of
// Start-Freq, End-Freq, Time
byte          sweepArrayPatterns[][12][3]= {  {{15,15,10},{15,33,10},{33,33,10},{33,15,10}},

                                             {{18,22,10},{22,26,02},{26,29,03},{29,33,10},{33,33,15},
                                              {33,29,10},{29,25,02},{25,22,05},{22,18,10},{18,18,15}},

                                             {{15,20,100},{20,26,020},{26,29,030},{29,33,100},{33,33,150},
                                              {33,29,100},{29,25,020},{25,20,050},{20,15,100},{15,15,150}},

                                             {{23,25,10},{25,27,03},{27,29,02},{29,31,03},
                                              {31,33,10},{33,33,15},{33,31,10},{31,29,03},
                                              {29,27,02},{27,25,03},{25,23,10},{23,23,15}},

                                             {{18,22,04},{22,30,04},{30,33,04},{33,33,05},
                                              {33,30,04},{30,22,04},{22,18,04},{18,18,05}},
                                              
                                             {{22,25,03},{25,30,02},{30,33,03},{33,35,03},
                                              {35,33,03},{33,30,03},{30,25,02},{25,22,03}},
                                              
                                             {{20,33,02},{33,22,02},{22,32,02},{32,24,02},
                                              {24,30,02},{30,26,02},{26,28,02},{28,20,02}},

                                             {{15,15,10}, {15,20,05},{20,20,10}, {20,25,05},
                                              {25,25,10}, {25,20,05},{20,20,10}, {20,15,05}},

                                             {{15,17,05}, {17,15,05}, {15,20,05}, 
                                              {20,22,05}, {22,20,05}, {20,25,05},
                                              {25,23,05}, {23,25,05}, {25,20,05}, 
                                              {20,18,05}, {18,20,05}, {20,15,05}},

                                             {{17,20,05}, {20,15,05}, {15,20,05},
                                              {20,25,05}, {25,20,05}, {20,23,05},
                                              {23,20,05}, {20,25,05}, {25,20,05},
                                              {20,15,05}, {15,20,05}, {20,17,05}},

                                             {{15,30,10}, {30,30,2},
                                              {30,15,10}, {15,15,2}},

                                             {{15,25,10}, {25,25,2},
                                              {25,15,10}, {15,15,2}},

                                             {{20,30,10}, {30,30,2},
                                              {30,20,10}, {20,20,2}},
                                              
                                             {{12,38,10}, {38,12,10}},
                                             
                                             {{12,38,10}, {38,30,05}, {30,38,05}, 
                                              {38,12,10}, {12,20,05}, {20,12,05}},
           
                                             {{12,38,10}, {38,30,05}, {30,38,05}, {38,30,05}, {30,38,05}, 
                                              {38,12,10}, {12,20,05}, {20,12,05}, {12,20,05}, {20,12,05}}
                                             };
byte          sweepArrayPatternsSize = (sizeof(sweepArrayPatterns) / 12 / 3);

byte          sweepArray[][3]         = {{1,1,1}, {1,1,1}, {1,1,1},
                                         {1,1,1}, {1,1,1}, {1,1,1},
                                         {1,1,1}, {1,1,1}, {1,1,1},
                                         {1,1,1}, {1,1,1}, {1,1,1},};
byte          sweepArraySize          = (sizeof(sweepArray) / 3);
// ----------------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------------
// General Variables ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
int           sweepArrayPos           = 0;
int           sweepArrayOffset        = 0;
unsigned int  invertCh2               = ON;
unsigned int  invertCh2Auto           = ON;
unsigned int  invertCh2Local          = ON;
unsigned int  cyclePatternAuto        = OFF;
unsigned int  cyclePatternLocal       = OFF;
float         frequency               = sweepStartFrequency;
float         frequency0;
float         frequency1;
unsigned int  dacPercent              = 0;
unsigned int  dacRange                = DAC_RANGE;
unsigned long dacVal[2]               = { 0, 0 };
//unsigned long dacValOut[2]            = { 0, 0 };
float         dacValOut[2]            = { 0, 0 };
unsigned int  previousSilo            = NBR_SILOS - 1;
unsigned int  currentSilo             = 0;
unsigned int  nextSilo                = 0;
unsigned int  beforeOpenChuteSilo     = 0;
unsigned int  siloCount               = 0;
int           a,b,c;

// These are established during the interupt handler
// so we set flags, store the values and 
// output a message later.
unsigned int  sweepStepCompleteMsg    = false;
unsigned int  sweepCompleteMsg        = false;
float         sweepCompleteTime;

unsigned int  sweepStepCompleteStartFrequency;
unsigned int  sweepStepCompleteEndFrequency;
unsigned int  sweepStepCompleteFrequencyRange;
float         sweepStepCompleteTime;
bool          getNextSweepPatternAuto  = false;
bool          getNextSweepPatternLocal = false;

String        inStr;
unsigned int  monitorBlinkState       = OFF;
unsigned int  watchdogTripped         = false;
unsigned long watchdogTripCount       = 0;
unsigned long watchdogTripTotalCount  = 0;

bool          testModeAuto   = OFF;
bool          testModeLocal  = OFF;
bool          testModeRemote = OFF;

bool          releaseVib1 = false;
bool          releaseVib2 = false;
int           openChuteCount = 0;
unsigned int  openChuteSilo  = -1;

char strBuf[256];

// ----------------------------------------------------------------------------------------------------------

struct timeString{
  int day;
  int month;
  int yearA;
  int yearB;
  int hour;
  int minute;
  int second;
};

//-------------------------------------------------------------
// Return the Software compilation date
//-------------------------------------------------------------
struct timeString getCompileString(){
  struct timeString myTime;

  String dateString;
  String timeString;
  dateString = ( __DATE__);
  timeString = ( __TIME__);
  
// Day
  char dayCharArray[2];
  dayCharArray[0] = dateString[4];
  dayCharArray[1] = dateString[5];
  myTime.day = atoi(dayCharArray);
  
// Month
  String months[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  for (a=0;a<12;a++){
    if (dateString.substring(0,3) == months[a]){
      myTime.month = a+1;   
    }
  }
  
// Year
  char yearCharArrayA[2];
  char yearCharArrayB[2];
  yearCharArrayA[0] = dateString[7];
  yearCharArrayA[1] = dateString[8];
  yearCharArrayB[0] = dateString[9];
  yearCharArrayB[1] = dateString[10];
  myTime.yearA = atoi(yearCharArrayA);
  myTime.yearB = atoi(yearCharArrayB);

// Hour
  char hourCharArray[2];
  hourCharArray[0] = timeString[0];
  hourCharArray[1] = timeString[1];
  myTime.hour = atoi(hourCharArray);

// Minute
  char minuteCharArray[2];
  minuteCharArray[0] = timeString[3];
  minuteCharArray[1] = timeString[4];
  myTime.minute = atoi(minuteCharArray);
  
// Second
  char secondCharArray[2];
  secondCharArray[0] = timeString[6];
  secondCharArray[1] = timeString[7];
  myTime.second = atoi(secondCharArray);

/*
  serialPort.print("READ  Firmware Compilation Date-Time String:"); 
  serialPort.print(myTime.day);
  serialPort.print(myTime.month);
  serialPort.print(myTime.yearA);
  serialPort.print(myTime.yearB);
  serialPort.print(myTime.hour);
  serialPort.print(myTime.minute);
  serialPort.print(myTime.second);
  serialPort.println();
*/
  return (myTime);
}

//-------------------------------------------------------------
// Return the Software compilation date stored in FLASH
//-------------------------------------------------------------
struct timeString getCompileStringFlash(){
  struct timeString myTime;
  myTime.day    = EEPROM.read(50);
  myTime.month  = EEPROM.read(51);
  myTime.yearA  = EEPROM.read(52);
  myTime.yearB  = EEPROM.read(53);
  myTime.hour   = EEPROM.read(54);
  myTime.minute = EEPROM.read(55);
  myTime.second = EEPROM.read(56);

  serialPort.print("READ  EEPROM   Compilation Date-Time String:"); 
  serialPort.print(myTime.day);
  serialPort.print(myTime.month);
  serialPort.print(myTime.yearA);
  serialPort.print(myTime.yearB);
  serialPort.print(myTime.hour);
  serialPort.print(myTime.minute);
  serialPort.print(myTime.second);
  serialPort.println();

  return (myTime);
}

//-------------------------------------------------------------
// Save the Software compilation date to FLASH
//-------------------------------------------------------------
void saveCompileString(struct timeString myTime){
  saveConfigItem(50,  myTime.day);  
  saveConfigItem(51,  myTime.month);  
  saveConfigItem(52,  myTime.yearA);  
  saveConfigItem(53,  myTime.yearB);  
  saveConfigItem(54,  myTime.hour);  
  saveConfigItem(55,  myTime.minute);  
  saveConfigItem(56,  myTime.second);  

  serialPort.print("WRITE EEPROM   Compilation Date-Time String:"); 
  serialPort.print(myTime.day);
  serialPort.print(myTime.month);
  serialPort.print(myTime.yearA);
  serialPort.print(myTime.yearB);
  serialPort.print(myTime.hour);
  serialPort.print(myTime.minute);
  serialPort.print(myTime.second);
  serialPort.println();
}

//-------------------------------------------------------------
// Compare 2 Software-Compilation-Strings and return the result
//-------------------------------------------------------------
bool compareCompileStrings(struct timeString ctA, struct timeString ctB){
  if (  (ctA.day    == ctB.day)    &&
        (ctA.month  == ctB.month)  &&
        (ctA.yearA  == ctB.yearA)  &&
        (ctA.yearB  == ctB.yearB)  &&
        (ctA.hour   == ctB.hour)   &&
        (ctA.minute == ctB.minute) &&
        (ctA.second == ctB.second) )
  {
    return true;        
  }
  else {
    return false;
  }
}

 
//-------------------------------------------------------------
// Setup - Called once at startup.
//-------------------------------------------------------------
void setup() {
  Serial.begin (BAUD_RATE);
  serialPort.begin(BAUD_RATE);
  delay(200);
  flushSerial();
  Serial.print("\n\n\n");
  Serial.print("DinrDuino - STM32F427 - VIBOS - START - USART3\n");
  serialPort.print("\n\n\n");
  serialPort.print("DinrDuino - STM32F427 - VIBOS - START - USART3\n");
  delay(200);

  DAC_I2C_WIRE.begin();

#ifdef AUTOTEST
  testModeAuto   = ON;
#endif
#ifdef LOCALTEST
  testModeLocal  = ON;
#endif
#ifdef REMOTETEST
  testModeRemote = ON;
#endif
#ifdef LOG_PIN_ASSIGNMENTS
  logPinAssignments();
#endif

// Setup the DIOs
  initializeIO();

  for (int a=0;a<NBR_SILOS;a++){
    swChuteState[a]     = CLOSED;
    swChuteLockState[a] = LOCKED;
  }

// Get the unique Unit ID from the CPU
  serialPort.print("Getting UUID\n");
  getUid();
  serialPort.println();
  
// Load the config data
  serialPort.print("Loading CONFIG\n");
  loadConfig();

// Load the watchdog stats and report the status  
  setupWatchdog();

// Turn on the air
//  serialPort.print("Turning on AIR\n");
//  serialPort.println("Turning on the Air Solanoid.");
//  digitalWrite(AIR_SOL_PIN,   HIGH);
//  delay(2000);

// Turn on the Air Regulators
  serialPort.println("Turning on Air Regulator 1.");
  digitalWrite(AIR_REG_1_PIN, HIGH);
  delay(20);
  serialPort.println("Turning on Air Regulator 2.");
  digitalWrite(AIR_REG_2_PIN, HIGH);
  delay(500);
 
// Log the config data
  logConfig();

  currentSilo = 0;

// Load the first sweep step of the sweepArray
  sweepArrayPos = -1;

// Turn on any events that are on by default
  establishEvents();

// Initialize schedule timers  
  initializeScheduleTimers();
  
// Setup the LCD if it's connected
  setupLCD();

  siloPauseEvent = OFF;

//  // Setup Interupt
//  #ifdef INTERUPT_OUTPUT_EVENT
//    serialPort.print("Setting up INTERUPT\n");
//    TimHandle.timer = TIMER_DAC_OUT;
//    TimerHandleInit(&TimHandle, 10000 - 1, ((uint32_t)(getTimerClkFreq(TIMER_DAC_OUT) / (FF_CPU)) - 1));
//    attachIntHandle(&TimHandle, timer_dac_out_handler);
//  #endif

// Setup Interupt
#ifdef INTERUPT_OUTPUT_EVENT
    serialPort.print("Setting up INTERUPT\n");
    TIM_TypeDef *Instance = TIMER_DAC_OUT;                                                                       //  #define TIMER_DAC_OUT TIM4
    // Instantiate HardwareTimer object. Thanks to 'new' instanciation, HardwareTimer is not destructed when setup() function is finished.
    HardwareTimer *MyTim = new HardwareTimer(Instance);

    //MyTim->setOverflow(10000 - 1, ((uint32_t)(getTimerClkFreq() / (FF_CPU)) - 1));
    MyTim->setOverflow(10000, HERTZ_FORMAT); // 10 kHz
    MyTim->attachInterrupt(timer_dac_out_callback);
    MyTim->resume();

    // TimHandle.timer = TIMER_DAC_OUT;
    // TimerHandleInit(&TimHandle, 10000 - 1, ((uint32_t)(getTimerClkFreq(TIMER_DAC_OUT) / (FF_CPU)) - 1));
    // attachIntHandle(&TimHandle, timer_dac_out_handler);
#endif

// Setup WDT
  IWatchdog.begin(10000000); // 10 Second watchdog
  delay(50);

  serialPort.print("STARTING\n");
}

//-------------------------------------------------------------
// Setp the DIO directions and pullups
//-------------------------------------------------------------
void initializeIO(){
  serialPort.print("Setting up INPUTs\n");
// Setup INPUT PINs
  pinMode(SW_MODE_LOCAL_PIN,   INPUT_PULLUP);
  pinMode(SW_MODE_AUTO_PIN,    INPUT_PULLUP);
  pinMode(SW_VIB_1_PIN,        INPUT_PULLUP);
  pinMode(SW_VIB_2_PIN,        INPUT_PULLUP);
  for (int a=0;a<NBR_SILOS;a++){
    pinMode(SW_SILO_PIN[a],       INPUT_PULLUP);
  //  pinMode(SW_CHUTE_PIN[a],      INPUT_PULLUP);
  //  pinMode(SW_CHUTE_LOCK_PIN[a], INPUT_PULLUP);
  }

  serialPort.print("Setting up OUTPUTs\n");
// Setup OUTPUT PINs
  pinMode(CPU_LED_PIN_2,          OUTPUT);
  pinMode(CPU_LED_PIN_3,          OUTPUT);
  pinMode(CPU_LED_PIN_4,          OUTPUT);
  pinMode(CPU_LED_PIN_1,          OUTPUT);
  pinMode(AIR_SOL_PIN,            OUTPUT);
  pinMode(AIR_REG_1_PIN,          OUTPUT);
  pinMode(AIR_REG_2_PIN,          OUTPUT);
  pinMode(VIB_LED_1_PIN,          OUTPUT);
  pinMode(VIB_LED_2_PIN,          OUTPUT);
  pinMode(LOCAL_LED_PIN,          OUTPUT);
  pinMode(AUTO_LED_PIN,           OUTPUT);
  
  for (int a=0;a<NBR_SILOS;a++){
    pinMode(SILO_LED_PIN[a],             OUTPUT);
    pinMode(AIR_MANIFOLD_1_PICK_PIN[a],  OUTPUT);
    pinMode(AIR_MANIFOLD_1_HOLD_PIN[a],  OUTPUT);
    pinMode(AIR_MANIFOLD_2_PICK_PIN[a],  OUTPUT);
    pinMode(AIR_MANIFOLD_2_HOLD_PIN[a],  OUTPUT);
  }

  digitalWrite(CPU_LED_PIN_1, HIGH);
  digitalWrite(CPU_LED_PIN_2, HIGH);
  digitalWrite(CPU_LED_PIN_3, HIGH);
  digitalWrite(CPU_LED_PIN_4, HIGH);
  digitalWrite(AIR_SOL_PIN,   LOW);
  digitalWrite(AIR_REG_1_PIN, LOW);
  digitalWrite(AIR_REG_2_PIN, LOW);
  digitalWrite(VIB_LED_1_PIN, LOW);
  digitalWrite(VIB_LED_2_PIN, LOW);
  for (int a=0;a<NBR_SILOS;a++){
    digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], LOW);
    digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], LOW);
    digitalWrite(AIR_MANIFOLD_2_PICK_PIN[a], LOW);
    digitalWrite(AIR_MANIFOLD_2_HOLD_PIN[a], LOW);
  }
  for (a=0;a<5;a++){
    digitalWrite(CPU_LED_PIN_1, HIGH);
    digitalWrite(CPU_LED_PIN_2, HIGH);
    digitalWrite(CPU_LED_PIN_3, HIGH);
    digitalWrite(CPU_LED_PIN_4, HIGH);
    delay(50);
    digitalWrite(CPU_LED_PIN_1, LOW);
    digitalWrite(CPU_LED_PIN_2, LOW);
    digitalWrite(CPU_LED_PIN_3, LOW);
    digitalWrite(CPU_LED_PIN_4, LOW);
    delay(50);
  }
}

//-------------------------------------------------------------
// Load the watchdog stats and report the status  
//-------------------------------------------------------------
void setupWatchdog(){
  serialPort.print("Loading WATCHDOG\n");
  loadWatchdog();
  serialPort.println("Checking Watchdog Flags");
  if (IWatchdog.isReset(true)) {
    serialPort.println("Watchdog HAS been tripped since last soft reset !");
    blinkBuiltinTime = 0.10;
    watchdogTripped = true;
    watchdogTripCount++;
    watchdogTripTotalCount++;
  }
  else{
    serialPort.println("Watchdog HAS NOT been tripped since last soft reset");
    watchdogTripped = false;
    watchdogTripCount = 0;
    if (watchdogTripTotalCount > 0){
      serialPort.println("Watchdog HAS been tripped since last hard reset !");
      blinkBuiltinTime = 0.5;
    }
    else{
      serialPort.println("Watchdog HAS NOT been tripped since last hard reset");
    blinkBuiltinTime = 1.0;
    }
  }
  blinkBuiltinDuration = blinkBuiltinTime / 2    * FF_CPU;
  saveWatchdog();
  indicateWatchdog();  
}

//-------------------------------------------------------------
// Establish which events are turned on by default
//-------------------------------------------------------------
void establishEvents(){
#ifdef TICKLE_WATCHDOG_EVENT
  tickleWatchdogEvent  = ON;
#endif
#ifdef TRIP_WATCHDOG_EVENT
  tripWatchdogEvent    = ON;
#endif
#ifdef LOG_STATUS_EVENT
  logStatusEvent       = ON;
#endif
#ifdef LOG_PICK_AND_HOLD_EVENT
  logPickAndHoldEvent  = ON;
#endif
#ifdef LOG_LED_EVENT
  logLedEvent          = ON;
#endif
#ifdef LOG_DIO_EVENT
  logDioEvent          = ON;
#endif
#ifdef LOG_DAC_OUTPUT_EVENT
  logDacOutputEvent    = ON;
#endif
#ifdef LOG_DAC_OUTPUT_RT
  logDacOutputRt       = ON;
#endif
#ifdef LOG_SWEEP_STATUS
  logSweepStatus       = ON;
#endif
#ifdef LOG_SWEEP_STEP_STATUS
  logSweepStepStatus   = ON;
#endif
#ifdef LOG_SILO_STATUS
  logSiloStatus        = ON;
#endif
#ifdef LOG_MONITOR_EVENT
  logMonitorEvent      = ON;
#endif
#ifdef LOG_LCD_EVENT
  logLcdEvent          = ON;
#endif
#ifdef REMOTE_STATUS_EVENT
  logRemoteStatusEvent = ON;
#endif
}

//-------------------------------------------------------------
// Initialize all the timers used forscheduling events
//-------------------------------------------------------------
void initializeScheduleTimers(){
  now = micros();
  lastTripWatchdogHop   = now;
  lastTickleWatchdogHop = now;
  lastBlinkHop          = now;
  lastBlinkBuiltinHop   = now;
  lastSerialHop         = now;
  lastLogMonitorHop     = now;
  lastLogStatusHop      = now;
  lastLogDioHop         = now;
  lastLogLedHop         = now;
  lastDioReadHop        = now;
  lastOutputHop         = now;
  lastRemoteStatusHop   = now;
  lastSweepStepHop      = now;
  lastSweepHop          = now;
  lastSiloHop           = now;
  lastPickHop           = now;
  lastLogDacOutputHop   = now;
  siloPauseHop          = now;
  lastLedKitHop         = now;
  openChuteTime         = now;
  openChuteTestTimeOn   = now;
  openChuteTestTimeOff  = now;
  vib1ReleaseTime       = now - RAMP_DURATION * 2;
  vib2ReleaseTime       = now - RAMP_DURATION * 2;
  remoteStateStart      = now;
}

//-------------------------------------------------------------
// Setup the LCD if it's connected
//-------------------------------------------------------------
void setupLCD(){
  if (logLcdEvent == ON){
    serialPort.print("Setting up I2C LCD\n");
    SCREEN_I2C_WIRE.setSDA(PB7);
    SCREEN_I2C_WIRE.setSCL(PB6);
    //SCREEN_I2C_WIRE.setClock(10000);
    SCREEN_I2C_WIRE.begin();
    lcd.begin (20,4);
    lcd.clear();
    lcd.setCursor(5,1);
    lcd.print("LCD Init");
    delay(1000);
    lcd.clear();
  }
}

//-------------------------------------------------------------
// Get the Unique ID from the CPU
//-------------------------------------------------------------
uint32_t getUid(){
  flashsize = *(uint16_t *)(FLASHSIZE_BASE);
  UnitID_Part_1 = *(uint32_t*)(UID_BASE );
  serialPort.print("STM32F4-UID:["); serialPort.print(UnitID_Part_1); serialPort.println("]");
  return UnitID_Part_1;
}

//-------------------------------------------------------------
// Use the watchdog LEDs
// to indicate if a watchdog event has occurred
//-------------------------------------------------------------
void indicateWatchdog(){
  if (watchdogTripCount > 0)
    digitalWrite(CPU_LED_PIN_3, HIGH);
  else
    digitalWrite(CPU_LED_PIN_3, LOW);
  
  if (watchdogTripTotalCount > 0)
    digitalWrite(CPU_LED_PIN_4, HIGH);
  else
    digitalWrite(CPU_LED_PIN_4, LOW);
}

//-------------------------------------------------------------
// Apply the default config
//-------------------------------------------------------------
void defaultConfig(){
  serialPort.println("Using Default Config.");
  sweepTotalTime                 = DEFAULT_SWEEP_TIME;
  siloTime                       = DEFAULT_SILO_TIME;
  siloPauseTime                  = DEFAULT_SILO_PAUSE_TIME;

  invertCh2Auto                  = DEFAULT_INVERT_CH2_AUTO;
  invertCh2Local                 = DEFAULT_INVERT_CH2_LOCAL;
  sweepArrayPatternNbrRemote     = DEFAULT_PATTERN_NBR_REMOTE;
  sweepArrayPatternNbrAutoFixed  = DEFAULT_PATTERN_NBR_AUTO;
  sweepArrayPatternNbrAuto       = sweepArrayPatternNbrAutoFixed;
  sweepArrayPatternNbrLocalFixed = DEFAULT_PATTERN_NUMBER_LOCAL;
  sweepArrayPatternNbrLocal      = sweepArrayPatternNbrLocalFixed;
  cyclePatternAuto               = DEFAULT_CYLCE_PATTERN_AUTO;
  cyclePatternLocal              = DEFAULT_CYLCE_PATTERN_LOCAL;

  unsigned int pos = 0;
  for(pos = 0;DEFAULT_PATTERN_NBR_AUTO_LIST[pos] != '#';pos++){
    sweepArrayPatternNbrAutoList[pos] = DEFAULT_PATTERN_NBR_AUTO_LIST[pos];
  }
  sweepArrayPatternNbrAutoList[pos]='#';
  sweepArrayPatternNbrAutoListSize = pos;

  for(pos = 0;DEFAULT_PATTERN_NBR_LOCAL_LIST[pos] != '#';pos++){
    sweepArrayPatternNbrLocalList[pos] = DEFAULT_PATTERN_NBR_LOCAL_LIST[pos];
  }
  sweepArrayPatternNbrLocalList[pos]='#';
  sweepArrayPatternNbrLocalListSize = pos;
}

//-------------------------------------------------------------
// Load config data from EEPROM
//-------------------------------------------------------------
void loadConfig(){
  serialPort.println("Loading Config.");
  
  // Check if this is the first time we have run this firmware
  if (compareCompileStrings(getCompileString(), getCompileStringFlash())){
    sweepTotalTime                 = EEPROM.read(1);
    siloTime                       = EEPROM.read(2);
    siloPauseTime                  = EEPROM.read(3);
    invertCh2Auto                  = EEPROM.read(4);
    invertCh2Local                 = EEPROM.read(5);
    sweepArrayPatternNbrRemote     = EEPROM.read(6);
    sweepArrayPatternNbrAutoFixed  = EEPROM.read(7);
    sweepArrayPatternNbrAuto       = sweepArrayPatternNbrAutoFixed;
    sweepArrayPatternNbrLocalFixed = EEPROM.read(8);
    sweepArrayPatternNbrLocal      = sweepArrayPatternNbrLocalFixed;
    cyclePatternAuto               = EEPROM.read(9);
    cyclePatternLocal              = EEPROM.read(10);
  
    serialPort.println("Reading AutoList");
    int pos = 0;
    int posOffset=20;
    for(pos = 0;(EEPROM.read(pos+posOffset) != '#') && (pos < 10);pos++){
      sweepArrayPatternNbrAutoList[pos] = EEPROM.read(pos+posOffset);
      //serialPort.print("Read pos "); serialPort.print(pos+posOffset); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrAutoList[pos]);
    }
    sweepArrayPatternNbrAutoListSize = pos;
    sweepArrayPatternNbrAutoList[pos] = '#';
    //serialPort.print("Read pos "); serialPort.print(pos+posOffset); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrAutoList[pos]);

    serialPort.println("Reading LocalList");
    posOffset=30;
    for(pos = 0;(EEPROM.read(pos+posOffset) != '#') && (pos < 10);pos++){
      sweepArrayPatternNbrLocalList[pos] = EEPROM.read(pos+posOffset);
      //serialPort.print("Read pos "); serialPort.print(pos+posOffset); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrLocalList[pos]);
    }
    sweepArrayPatternNbrLocalListSize = pos;
    sweepArrayPatternNbrLocalList[pos] = '#';
    //serialPort.print("Read pos "); serialPort.print(pos+posOffset); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrLocalList[pos]);
    
    unsigned int configError = false;
    if (sweepTotalTime > SWEEP_TIME_MAX){
      serialPort.print("Invalid sweepTotalTime:"); 
      serialPort.print(sweepTotalTime);
      serialPort.print(" > ");
      serialPort.println(SWEEP_TIME_MAX);
      configError = true;
    }
    if (siloTime > SILO_TIME_MAX){
      serialPort.print("Invalid siloTime:"); 
      serialPort.print(siloTime);
      serialPort.print(" > ");
      serialPort.println(SILO_TIME_MAX);
      configError = true;
    }
    if (siloPauseTime > SILO_PAUSE_TIME_MAX){
      serialPort.print("Invalid siloPauseTime:"); 
      serialPort.print(siloPauseTime);
      serialPort.print(" > ");
      serialPort.println(SILO_PAUSE_TIME_MAX);
      configError = true;
    }
    if (invertCh2Auto > ON){
      serialPort.print("Invalid invertCh2Auto:"); 
      serialPort.println(invertCh2Auto);
      configError = true;
    }
    if (invertCh2Local > ON){
      serialPort.print("Invalid invertCh2Local:"); 
      serialPort.println(invertCh2Local);
      configError = true;
    }
    if (sweepArrayPatternNbrAuto > sweepArrayPatternsSize - 1){
      serialPort.print("Invalid sweepArrayPatternNbrAuto:"); 
      serialPort.print(sweepArrayPatternNbrAuto);
      serialPort.print(" > ");
      serialPort.println(sweepArrayPatternsSize - 1);
      configError = true;
    }
    if (sweepArrayPatternNbrLocal > sweepArrayPatternsSize - 1){
      serialPort.print("Invalid sweepArrayPatternNbrLocal:"); 
      serialPort.print(sweepArrayPatternNbrLocal);
      serialPort.print(" > ");
      serialPort.println(sweepArrayPatternsSize - 1);
      configError = true;
    }
    if (cyclePatternAuto > ON){
      serialPort.print("Invalid cyclePatternAuto:"); 
      serialPort.println(cyclePatternAuto);
      configError = true;
    }
    if (cyclePatternLocal > ON){
      serialPort.print("Invalid cyclePatternLocal:"); 
      serialPort.println(cyclePatternLocal);
      configError = true;
    }
    if (configError == true){
      serialPort.println("Invallid Config Data in EEPROM.\nLoading Defaults");
      defaultConfig();
      saveConfig();
      saveCompileString(getCompileString());
    }
  }
  else{
    serialPort.println("Incorrect firmware compilation Date-Time found in EEPROM");    
    defaultConfig();
    saveConfig();
    saveCompileString(getCompileString());
    resetWatchdog();
  }
  siloDuration = siloTime * FF_CPU;
  siloPauseDuration = siloPauseTime * FF_CPU;
}

//-------------------------------------------------------------
// Log the config to the Serial console
//-------------------------------------------------------------
void logConfig(){
  serialPort.print("\n--- Configuration Data ---------\n");
  delay(100);
  serialPort.print("sweep                     :"); serialPort.println(sweepTotalTime);
  delay(100);
  serialPort.print("silo                      :"); serialPort.println(siloTime);
  delay(100);
  serialPort.print("silo pause time           :"); serialPort.println(siloPauseTime);
  delay(100);
  serialPort.print("--------------------------------\n");
  delay(100);

  serialPort.print("remote-pattern            :"); serialPort.println(sweepArrayPatternNbrRemote);
  delay(100);

  serialPort.print("auto-pattern-fixed        :"); serialPort.println(sweepArrayPatternNbrAutoFixed);
  delay(100);
  for (unsigned int pos = 0;sweepArrayPatternNbrAutoList[pos] != '#';pos++) {
    serialPort.print("auto-pattern-list[");          serialPort.print(pos);
    delay(100);
    serialPort.print("]      :");                    serialPort.println(sweepArrayPatternNbrAutoList[pos]);
    delay(100);
  }
  serialPort.print("auto-pattern-list size    :"); serialPort.println(sweepArrayPatternNbrAutoListSize);
  delay(100);
  serialPort.print("auto-pattern-current      :"); serialPort.println(sweepArrayPatternNbrAuto);
  delay(100);
  serialPort.print("auto-invert-ch2           :"); serialPort.println(ON_OFF_ARRAY[invertCh2Auto]);
  delay(100);
  serialPort.print("auto-cylcle-pattern       :"); serialPort.println(ON_OFF_ARRAY[cyclePatternAuto]);
  delay(100);
  serialPort.print("--------------------------------\n");
  delay(100);
    
  serialPort.print("local-pattern-fixed       :"); serialPort.println(sweepArrayPatternNbrLocalFixed);
  delay(100);
  for (unsigned int pos = 0;sweepArrayPatternNbrLocalList[pos] != '#';pos++) {
    serialPort.print("local-pattern-list[");         serialPort.print(pos);
    delay(100);
    serialPort.print("]     :");                     serialPort.println(sweepArrayPatternNbrLocalList[pos]);
    delay(100);
  }
  serialPort.print("local-pattern-list size   :"); serialPort.println(sweepArrayPatternNbrLocalListSize);
  delay(100);
  serialPort.print("local-pattern-current     :"); serialPort.println(sweepArrayPatternNbrLocal);
  delay(100);
  serialPort.print("local-invert-ch2          :"); serialPort.println(ON_OFF_ARRAY[invertCh2Auto]);
  delay(100);
  serialPort.print("local-cylcle-pattern      :"); serialPort.println(ON_OFF_ARRAY[cyclePatternLocal]);
  delay(100);
  serialPort.print("--------------------------------\n");
  delay(100);

  serialPort.print("WD Trips since Power-On   :"); serialPort.println(watchdogTripCount);
  delay(100);
  serialPort.print("WD Trips since Install    :"); serialPort.println(watchdogTripTotalCount);
  delay(100);
  serialPort.print("--------------------------------\n");
  delay(100);

  serialPort.println("Running State");
  delay(100);
  if (siloPauseEvent == OFF){
    serialPort.println("ACTIVE STATE");
    delay(100);
    serialPort.print("Current Silo              :"); serialPort.println(SILO_NAME[currentSilo]);
    delay(100);
    serialPort.print("silo Time                 :"); serialPort.print(1.0 * (now - lastSiloHop) / FF_CPU);
    delay(100);
    serialPort.print(" / ");                         serialPort.println(1.0 * siloDuration / FF_CPU);
    delay(100);
  }
  else{
    int statusPauseMinutesTotal = (1.0 * siloPauseDuration    / FF_CPU / 60);
    int statusPauseSecondsTotal = (1.0 * siloPauseDuration    / FF_CPU) - (statusPauseMinutesTotal * 60);
    int statusPauseMinutes      = (1.0 * (now - siloPauseHop) / FF_CPU / 60);
    int statusPauseSeconds      = (1.0 * (now - siloPauseHop) / FF_CPU) - (statusPauseMinutes      * 60);
    serialPort.println("PAUSED STATE");
    delay(100);
    serialPort.print("silo Pause Time           :"); serialPort.print(statusPauseMinutes);
    delay(100);
    serialPort.print(":");                           serialPort.print(statusPauseSeconds);
    delay(100);
    serialPort.print(" / ");                         serialPort.print(statusPauseMinutesTotal);
    delay(100);
    serialPort.print(":");                           serialPort.println(statusPauseSecondsTotal);
    delay(100);
  }
  serialPort.print("--------------------------------\n");
  delay(100);
}

//-------------------------------------------------------------
// Save the config data to EEPROM
//-------------------------------------------------------------
void saveConfig(){
  serialPort.println("Saving Config.");
  saveConfigItem(0,  111);
  saveConfigItem(1,  sweepTotalTime);
  saveConfigItem(2,  siloTime);
  saveConfigItem(3,  siloPauseTime);
  saveConfigItem(4,  invertCh2Auto);
  saveConfigItem(5,  invertCh2Local);
  saveConfigItem(6,  sweepArrayPatternNbrRemote);
  saveConfigItem(7,  sweepArrayPatternNbrAutoFixed);
  saveConfigItem(8,  sweepArrayPatternNbrLocalFixed);
  saveConfigItem(9,  cyclePatternAuto);
  saveConfigItem(10,  cyclePatternLocal);

  IWatchdog.reload();

  unsigned int pos = 0;
  serialPort.println("Saving AutoList to EEPROM.");
  for (pos = 0;sweepArrayPatternNbrAutoList[pos] != '#';pos++){
    serialPort.print("Saving item "); serialPort.print(pos); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrAutoList[pos]);
    saveConfigItem(pos+20, sweepArrayPatternNbrAutoList[pos]);
    IWatchdog.reload();
  }
  serialPort.print("Saving item "); serialPort.print(pos); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrAutoList[pos]);
  saveConfigItem(pos+20,'#'); 

  serialPort.println("Saving LocalList to EEPROM.");
  for (pos = 0;sweepArrayPatternNbrLocalList[pos] != '#';pos++){
    serialPort.print("Saving item "); serialPort.print(pos); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrLocalList[pos]);
    saveConfigItem(pos+30, sweepArrayPatternNbrLocalList[pos]);
    IWatchdog.reload();
  }
  serialPort.print("Saving item "); serialPort.print(pos); serialPort.print(":"); serialPort.println(sweepArrayPatternNbrLocalList[pos]);
  saveConfigItem(pos+30,'#'); 
}

//-------------------------------------------------------------
// Save one config item to EEPROM
//-------------------------------------------------------------
bool saveConfigItem(unsigned int index, unsigned int value){
//  serialPort.print("Saving value:"); serialPort.print(value); serialPort.print(" to addr:"); serialPort.println(index);
  if (EEPROM.read(index) != value){
//    serialPort.print("Updating value:"); serialPort.print(value); serialPort.print(" to EEPROM addr:"); serialPort.println(index);
    EEPROM.write(index, value);
  }
  else{
//    serialPort.print("Verified value:"); serialPort.print(value); serialPort.print(" at EEPROM addr:"); serialPort.println(index);
  }
  return 0;
}

//-------------------------------------------------------------
// Read the watchdog stats from EEPROM
//-------------------------------------------------------------
void loadWatchdog(){
  serialPort.println("Loading Watchdog Stats.");
  if (EEPROM.read(0) == 111){
    watchdogTripCount           = EEPROM.read(40);
    watchdogTripTotalCount      = EEPROM.read(41);
  }
  else{
    watchdogTripCount           = 0;
    watchdogTripTotalCount      = 0;
  }
}

//-------------------------------------------------------------
// Save the watdog stats to EEPROM 
//-------------------------------------------------------------
void saveWatchdog(){
  serialPort.println("Saving Watchdog Stats.");
  saveConfigItem(40,  watchdogTripCount);
  saveConfigItem(41,  watchdogTripTotalCount);
}

//-------------------------------------------------------------
// Reset the watchdog stats stored in EEPROM
//-------------------------------------------------------------
void resetWatchdog(){
  serialPort.println("Resetting Watchdog Counters");
  watchdogTripCount         = 0;
  watchdogTripTotalCount    = 0;
  blinkBuiltinTime          = 1.0;
  saveWatchdog();
  blinkBuiltinDuration = blinkBuiltinTime / 2    * FF_CPU;
  indicateWatchdog();
}

//-------------------------------------------------------------
// Load a sweep array, given a sweep-pattern-number
//-------------------------------------------------------------
void loadSweepArray(unsigned int sweepArrayPatternNumber, unsigned int invert){
  int i   = 0;
  while ((i < (sizeof(sweepArrayPatterns[sweepArrayPatternNumber]) / 3)) 
      && (sweepArrayPatterns[sweepArrayPatternNumber][i][0] != 0) ){
    sweepArray[i][0] = sweepArrayPatterns[sweepArrayPatternNumber][i][0];
    sweepArray[i][1] = sweepArrayPatterns[sweepArrayPatternNumber][i][1];
    sweepArray[i][2] = sweepArrayPatterns[sweepArrayPatternNumber][i][2];
    i++;
  }
  sweepArraySize = i;
  invertCh2 = invert;
  sweepArrayOffset = 1.0 * (sweepArrayPatternOffsetPercent[sweepArrayPatternNumber]) / 100 * DAC_RANGE + DAC_MIN;
  if (logSweepStatus == ON){
    serialPort.print("STS>Loaded Sweep Pattern:"); serialPort.print(sweepArrayPatternNumber);
    serialPort.print(" - Sweep Inversion:");       serialPort.print(invertCh2);
    serialPort.print(" - Sweep Offset:");          serialPort.println(sweepArrayOffset);
    delay(10);
  }
  lastSweepHop = now;
}

//-------------------------------------------------------------
// Loop - Called endlessly 
//-------------------------------------------------------------
void loop() {
// Opperational events
#ifdef SWEEP_STEP_EVENT
  if (now - lastSweepStepHop >= sweepStepDuration){
    lastSweepStepHop = now;
    processNextSweepStepEvent();
  }
#endif
#ifdef BLINK_EVENT
  if (now - lastBlinkHop >= BLINK_DURATION){
    lastBlinkHop = now;
    processBlinkEvent();
  }
#endif
#ifdef BLINK_BUITLIN_EVENT
  if (now - lastBlinkBuiltinHop >= blinkBuiltinDuration){
    lastBlinkBuiltinHop = now;
    processBlinkBuiltinEvent();
  }
#endif
#ifdef OUTPUT_EVENT
  if (now - lastOutputHop >= OUTPUT_DURATION){
    lastOutputHop = now;
    processOutputEvent();
  }
#endif
//#ifdef REMOTE_STATUS_EVENT
//  if (now - lastRemoteStatusHop >= REMOTE_STATUS_DURATION){
//    lastRemoteStatusHop = now;
//    logRemoteStatus();
//  }
//#endif
#ifdef DIO_READ_EVENT
  if (now - lastDioReadHop >= DIO_READ_DURATION){
    lastDioReadHop = now;
    processDioReadEvent();
  }
#endif

// If no chutes are open carry on like normal.
if (openChuteCount == 0){
  #ifdef SILO_EVENT
    if (siloPauseEvent == ON){
      processSiloPauseEvent();
    }
    else{
      processSiloActiveEvent(); 
    }
  #endif
}
// If a chute is open, vibrate that silo only until the chute is closed.
else{
  processOpenChute();
}

#ifdef DIO_WRITE_EVENT
  if (now - lastDioWriteHop >= DIO_WRITE_DURATION){
    lastDioWriteHop = now;
    processDioWriteEvent();
  }
#endif
  
// Watchdog related events
  if (tickleWatchdogEvent == ON){
    if (now - lastTickleWatchdogHop >= TICKLE_WATCHDOG_DURATION){
      lastTickleWatchdogHop = now;
      IWatchdog.reload();
    }
  }
  if (tripWatchdogEvent == ON){
    if (now - lastTripWatchdogHop >= TRIP_WATCHDOG_DURATION){
      lastTripWatchdogHop = now;
      while (1){
        serialPort.println("*** Tripping Watchdog ***");
        delay(1000);
      }
    }
  }

// Serial processing event - Process commands issued at the terminal and output logging info
#ifdef SERIAL_EVENT
  if (now - lastSerialHop >= SERIAL_DURATION){
    lastSerialHop = now;
    processSerialEvent();
  }
#endif

// Various logging options which can be enabled/disabled via the serial interface and via MACROS
  if (logRemoteStatusEvent == ON){
    if (now - lastRemoteStatusHop >= REMOTE_STATUS_DURATION){
      lastRemoteStatusHop = now;
      logRemoteStatus();
    }
  }
  if (logLcdEvent == ON){
    if (now - lastLogLcdHop >= LOG_LCD_DURATION){
      lastLogLcdHop = now;
      processLogLcdEvent();
    }
  }
  if (logLedEvent == ON){
    if (now - lastLogLedHop >= LOG_LED_DURATION){
      lastLogLedHop = now;
      processLogLedEvent();
    }
  }

  if (logDioEvent == ON){
    if (now - lastLogDioHop >= LOG_DIO_DURATION){
      lastLogDioHop = now;
      processLogDioEvent();
    }
  }

  if (logMonitorEvent == ON){
    if (now - lastLogMonitorHop >= LOG_MONITOR_DURATION){
      lastLogMonitorHop = now;
      //processLogMonitorEvent();
      //processLogMonitorFullEvent();
      processLogMonitorPlotEvent();
    }
  }

  if (logStatusEvent == ON){
    if (now - lastLogStatusHop >= LOG_STATUS_DURATION){
      lastLogStatusHop = now;
      processLogStatusEvent();
    }
  }

  if (logDacOutputEvent == ON){
    if (now - lastLogDacOutputHop >= LOG_DAC_OUTPUT_DURATION){
      lastLogDacOutputHop = now;
      logDacOutput();
    }
  }
  
  if (logSweepStatus == ON){
    processLogSweepStatusEvent();
  }

// Remote Timeout
#ifdef REMOTETIMEOUT
  if ((int(remoteTimeoutTime) - int((now - remoteStateStart) / FF_CPU)) <= 0){
    if (remoteState == 1){
      remoteState = 0;
      serialPort.println("STS>################################################");
      delay(10);
      sprintf(strBuf, " - %06d = (%06d - %06d)", 
        int(remoteTimeoutTime) - int((now - remoteStateStart) / FF_CPU),
        int(remoteTimeoutTime),
        int((now - remoteStateStart) / FF_CPU)
        );
      serialPort.print(strBuf); 
      delay(10);      
      serialPort.print  ("STS>(now - remoteStateStart):"); serialPort.println(now - remoteStateStart);
      delay(10);
      serialPort.print  ("STS>remoteTimeoutDuration   :"); serialPort.println(remoteTimeoutDuration);
      delay(10);
      serialPort.println("STS>RemoteMode Timeout");
      delay(10);
      serialPort.println("STS>################################################");
      delay(10);
    }
  }
#else
  if (remoteState == 1){
    remoteStateStart = now;
  }
#endif
  
  now = micros();
}

//-------------------------------------------------------------
// Vibrate the Silo with the open chute only.
// This takes priority, so pauses the current opperation
//-------------------------------------------------------------
void processOpenChute(){
  //serialPort.print("Open Chute:"); serialPort.println(openChuteSilo);

  swModeLocalState    = SW_ON;
  swModeAutoState     = SW_ON;
  swModeRemoteState   = SW_ON;

  swVib1State         = SW_OFF;
  swVib2State         = SW_OFF;
  swVibBothState      = SW_ON;

  for (a=0;a<NBR_SILOS;a++){
    if (a == openChuteSilo){
      // Only vibrate for a time then just wait for the chute to close
      if (now - openChuteTime < OPEN_CHUTE_DURATION){
        swSiloState[a] = SW_ON;
      }
      else{
        swSiloState[a] = SW_OFF;
      }
    }
    else{
      swSiloState[a] = SW_OFF;
    }
  }

  siloPauseEvent      = OFF;
  currentSilo = openChuteSilo;
}

//-------------------------------------------------------------
// Pause State
// Pause before cycling throught all the selected Silos again
//-------------------------------------------------------------
void processSiloPauseEvent(){
  loadSweepArray(sweepArrayPatternNbrRemote, invertCh2Local);
  // Blink all the Silo LEDS to indicate we are paused
  if (now - lastLedKitHop >= LED_KIT_DURATION){
    lastLedKitHop = now;
    ledKitSilo+=ledKitMod;
    if (ledKitSilo <= 0){
      ledKitSilo = 0;
      ledKitMod = 1;
    }
    if (ledKitSilo >= NBR_SILOS-1){
      ledKitSilo = NBR_SILOS-1;
      ledKitMod = -1;
    }
    for (a=0;a<NBR_SILOS;a++){
      if (a == ledKitSilo){
        ledSiloState[a] = ON;
      }
      else{
        ledSiloState[a] = OFF;
      }
    }
  }

  // Check if it is time to stop pausing
  if (now - siloPauseHop >= siloPauseDuration){
    if (logSiloStatus == ON){
      serialPort.println("STS>Turning siloPauseEvent OFF");
    }
    siloPauseEvent = OFF;
    processPickAndHold();
    lastSiloHop = now;

    // Load the right sweep pattern
    if (swModeLocalState == SW_ON)
      loadSweepArray(sweepArrayPatternNbrLocal, invertCh2Local);  // Load the LOCAL sweep array
    else if (swModeAutoState == SW_ON)
      loadSweepArray(sweepArrayPatternNbrAuto, invertCh2Auto);    // Load the AUTO sweep array

    // Only start to ramp down if we are not already
    if (now - vib1ReleaseTime > RAMP_DURATION){
      vib1ReleaseTime = now;  // Force a soft release of previously held Silo          
    }
    if (now - vib2ReleaseTime > RAMP_DURATION){
      vib2ReleaseTime = now;  // Force a soft release of previously held Silo
    }

  }
}

//-------------------------------------------------------------
// Active State
// Move to the next Silo only when the air output has settled
//-------------------------------------------------------------
void processSiloActiveEvent(){ 
  if (now - lastSiloHop >= siloDuration){
    lastSiloHop = now;
    processNextSiloEvent();
    if (logLcdEvent == ON){
      lcd.begin(20,4);
      lcd.clear();
      lastLogLcdHop = now - LOG_LCD_DURATION;
    }
  }

  processPickAndHold();
  // Only change to the next pattern when the regulator has been wound down and the air has been stopped
  if ((getNextSweepPatternAuto == true) && (now - vib1ReleaseTime >= RAMP_DURATION)){
    nextSweepPatternAuto();
    getNextSweepPatternAuto = false;
  }
  if ((getNextSweepPatternLocal == true) && (now - vib1ReleaseTime >= RAMP_DURATION)){
    nextSweepPatternLocal();
    getNextSweepPatternLocal = false;
  }
}

//-------------------------------------------------------------
// Log the current status of the Frequency Sweep Algorithm
//-------------------------------------------------------------
void processLogSweepStatusEvent(){
  char strTime[16];
  if (sweepCompleteMsg == true){
    dtostrf(sweepCompleteTime, 2, 2, strTime);                          // Format the Float value
    sprintf(strBuf, "STS>Completed Sweep in %s seconds\n",              // Format the rest of the String
      strTime);
    serialPort.print(strBuf);                                               // Print the string
    sweepCompleteMsg = false;
    delay(10);
  }
  if (logSweepStepStatus == ON){
    if (sweepStepCompleteMsg == true){
      dtostrf(sweepStepTime, 2, 2, strTime);                            // Format the Float value
      sprintf(strBuf, "STS>Start:%02d End:%02d Range:%02d Time:%s\n",   // Format the rest of the String
        sweepStartFrequency, sweepEndFrequency, 
        sweepFrequencyRange, strTime);
      serialPort.print(strBuf);                                             // Print the string
      delay(10);
    }
    sweepStepCompleteMsg = false;
  }
}

//-------------------------------------------------------------
// Blink Builtin LEDs
//-------------------------------------------------------------
void processBlinkBuiltinEvent(){
//  serialPort.println("BLINK BUILTIN LEDs EVENT\n");

// ---- These are the onboard LEDs --------------------------------
  digitalWrite(CPU_LED_PIN_1, ledBlinkBuiltinState);
  digitalWrite(CPU_LED_PIN_2, !ledBlinkBuiltinState);
//-----------------------------------------------------------------
  ledBlinkBuiltinState=!ledBlinkBuiltinState;
  delay(10);
}

//-------------------------------------------------------------
// Blink the LEDs on the front pannel
//-------------------------------------------------------------
void processBlinkEvent(){
//  serialPort.println("BLINK EVENT\n");

/*
  if ((swVib1State == SW_ON) || (swVibBothState == SW_ON)){
    if (swSiloState[currentSilo] == SW_ON){
      ledVib1State = BLINK;
    }
    else{
      ledVib1State = ON;
    }
  }
  if ((swVib2State == SW_ON) || (swVibBothState == SW_ON)){
    if (swSiloState[currentSilo] == SW_ON){
      ledVib2State = BLINK;
    }
    else{
      ledVib2State = ON;
    }
  }
*/

// ---- These are the LEDs indicating current ---------------------
// ---- selection / opperating mode -------------------------------
  if (ledAutoState == BLINK)
    digitalWrite(AUTO_LED_PIN, ledBlinkState);
  else if (ledAutoState == ON)
    digitalWrite(AUTO_LED_PIN, LED_ON);
  else
    digitalWrite(AUTO_LED_PIN, LED_OFF);

  if (ledLocalState == BLINK)
    digitalWrite(LOCAL_LED_PIN, ledBlinkState);
  else if (ledLocalState == ON)
    digitalWrite(LOCAL_LED_PIN, LED_ON);
  else
    digitalWrite(LOCAL_LED_PIN, LED_OFF);
//-----------------------------------------------------------------

  
// ---- These LEDs indicate selected and active Vibrator/s --------
  if (ledVib1State == BLINK){
    digitalWrite(VIB_LED_1_PIN, ledBlinkState);
  }
  else if (ledVib1State == ON){
    digitalWrite(VIB_LED_1_PIN, LED_ON);
  }
  else{
    digitalWrite(VIB_LED_1_PIN, LED_OFF);
  }
  
  if (ledVib2State == BLINK){
    digitalWrite(VIB_LED_2_PIN, !ledBlinkState);
  }
  else if (ledVib2State == ON){
    digitalWrite(VIB_LED_2_PIN, LED_ON);
  }
  else{
    digitalWrite(VIB_LED_2_PIN, LED_OFF);
  }
//-----------------------------------------------------------------

// ---- These LEDs indicate the selected / active Silo/s ----------
  for (a=0;a<NBR_SILOS;a++){
    if (ledSiloState[a] == BLINK){
      if ((manifold1State[a] >= HOLD) || (manifold2State[a] >= HOLD))
        digitalWrite(SILO_LED_PIN[a], ledBlinkState);
      else
        digitalWrite(SILO_LED_PIN[a], ledBlinkBuiltinState);
    }
    else if (ledSiloState[a] == ON)
      digitalWrite(SILO_LED_PIN[a], LED_ON);
    else
      digitalWrite(SILO_LED_PIN[a], LED_OFF);
//-----------------------------------------------------------------
    
  }

  ledBlinkState=!ledBlinkState;
}

//-------------------------------------------------------------
// Process serial input (command interface)
//-------------------------------------------------------------
void processSerialEvent(){
  char inCharArray[256];
    
  while (serialPort.available() > 0) {
    // read incoming serial data:
    char inChar = serialPort.read();
    if (inChar != '\n'){
      //serialPort.print("["); serialPort.print(inChar); serialPort.print("]");
      inStr+=inChar;
    }
    else if (inChar == '\n'){
      delay(10);
      //serialPort.println();
      //serialPort.print("Command:["); serialPort.print(inStr); serialPort.println("]");
      
      if (inStr.substring(0,4) == "help"){
        serialPort.println("Help:");
        delay(100);
        serialPort.println("     reset                           (Reset the CPU)");
        delay(100);
        serialPort.println("     status                          (Log the running config)");
        delay(100);
        serialPort.println("     load                            (Load settings from EEPROM)");
        delay(100);
        serialPort.println("     save                            (Save settings to EEPROM)");
        delay(100);
        serialPort.println("     default                         (Save default settings to EEPROM)");
        delay(100);
        serialPort.println("     watchdog tickle on              (Enable/Disable tickling the watchdog)");
        delay(100);
        serialPort.println("     watchdog trip on                (Enable/Disable tickling the watchdog)");
        delay(100);
        serialPort.println("     watchdog reset                  (Reset the watchdog reset counters)");
        delay(100);
        serialPort.println("     sweep <##>                      (Set Sweep pattern duration)");
        delay(100);
        serialPort.println("     silo <##>                       (Set Silo vibrate duration)");
        delay(100);
        serialPort.println("     silopause <##>                  (Set Silo Pause duration)");
        delay(100);
        serialPort.println("     auto <##>                       (Set pattern for AUTO mode)");
        delay(100);
        serialPort.println("     local <##>                      (Set pattern for LOCAL mode)");
        delay(100);
        serialPort.println("     autolist (#,#,#,#)              (Set pattern-list for AUTO mode)");
        delay(100);
        serialPort.println("     locallist (#,#,#,#)             (Set pattern-list for LOCAL mode)");
        delay(100);
        serialPort.println("     autoinvert <on/off>             (Set chanel inversion on/off for AUTO mode)");
        delay(100);
        serialPort.println("     localinvert <on/off>            (Set chanel inversion on/off for LOCAL mode)");
        delay(100);
        serialPort.println("     autocycle <on/off>              (Set Pattern Cycling on/off for AUTO mode)");
        delay(100);
        serialPort.println("     localcycle <on/off>             (Set Pattern Cycling on/off for LOCAL mode)");
        delay(100);
        serialPort.println("     autotest <on/off>               (Set AUTO mode testing on/off)");
        delay(100);
        serialPort.println("     localtest <on/off>              (Set AUTO mode testing on/off)");
        delay(100);
        serialPort.println("     remotetest <on/off>             (Set REMOTE mode testing on/off)");
        delay(100);
        serialPort.println("     log monitor <on/off>            (Turn Log Monitor on/off)");
        delay(100);
        serialPort.println("     log led <on/off>                (Turn Log LED on/off)");
        delay(100);
        serialPort.println("     log dio <on/off>                (Turn Log DIO on/off)");
        delay(100);
        serialPort.println("     log status <on/off>             (Turn Log Status on/off)");
        delay(100);
        serialPort.println("     log remote <on/off>             (Turn Log Remote Status on/off)");
        delay(100);
        serialPort.println("     log manifold <on/off>           (Turn Log Manifold on/off)");
        delay(100);
        serialPort.println("     log dac <on/off>                (Turn Log DAC Output on/off)");
        delay(100);
        serialPort.println("     log sweep <on/off>              (Turn Log Sweep Status on/off)");
        delay(100);
        serialPort.println("     log sweepstep <on/off>          (Turn Log SweepStep Status on/off)");
        delay(100);
        serialPort.println("     log silo <on/off>               (Turn Log Silo Status on/off)");
        delay(100);
        serialPort.println("     remote state <on/off>           (Turn Remote opperation on/off)");
        delay(100);
        serialPort.println("     remote silo <on/off> <silo>     (Turn Silo <silo> on/off)");
        delay(100);
        serialPort.println("     remote vibe <on/off> <vibrator> (Turn Vibrator <vibe> on/off)");
        delay(100);
      }
      else if (inStr.substring(0,5)  == "reset"){
        softwareReset();  
      }
      else if (inStr.substring(0,6)  == "status"){
        logConfig();
      }
      else if (inStr.substring(0,4)  == "load"){
        loadConfig();
        if (swModeLocalState == SW_ON)loadSweepArray
          (sweepArrayPatternNbrLocal, invertCh2Local); 
        else if (swModeAutoState == SW_ON)
          loadSweepArray(sweepArrayPatternNbrAuto, invertCh2Auto); 
      }
      else if (inStr.substring(0,4)  == "save"){
        saveConfig();
      }
      else if (inStr.substring(0,7)  == "default"){
        defaultConfig();
        saveConfig();
        logConfig();
      }
      else if (inStr.substring(0,9)  == "watchdog "){
        if (inStr.substring(9,16) == "tickle "){
          if (inStr.substring(16,19) == "on"){
            tickleWatchdogEvent = ON;
            serialPort.println("Watchdog Tickle is turned ON");
          }
          else if (inStr.substring(16,19) == "off"){
            tickleWatchdogEvent = OFF;
            serialPort.println("Watchdog Tickle is turned OFF");
          }
        }
        else if (inStr.substring(9,14) == "trip "){
          if (inStr.substring(14,17) == "on"){
            tripWatchdogEvent = ON;
            serialPort.println("Watchdog Trip is turned ON");
          }
          else if (inStr.substring(14,17) == "off"){
            tripWatchdogEvent = OFF;
            serialPort.println("Watchdog Trip is turned OFF");
          }
        }
        else if (inStr.substring(9,14) == "reset"){
          resetWatchdog();        
        }
      }
      else if (inStr.substring(0,6)  == "sweep "){
        inCharArray[0] = inStr[6];
        inCharArray[1] = inStr[7];
        inCharArray[2] = inStr[8];
        inCharArray[3] = inStr[9];
        if (atoi(inCharArray) > SWEEP_TIME_MAX){
          serialPort.print("Max sweep time allowed is ");
          serialPort.print(SWEEP_TIME_MAX);
          serialPort.println(" seconds"); 
        }
        else if (atoi(inCharArray) < SWEEP_TIME_MIN){
          serialPort.print("Min sweep time allowed is ");
          serialPort.print(SWEEP_TIME_MIN);
          serialPort.println(" seconds"); 
        }
        else{
          sweepTotalTime = atof(inCharArray);
          serialPort.print("Sweep time set to "); serialPort.println(sweepTotalTime);
        }
      }
      else if (inStr.substring(0,5)  == "silo "){
        inCharArray[0] = inStr[5];
        inCharArray[1] = inStr[6];
        inCharArray[2] = inStr[7];
        inCharArray[3] = inStr[8];
        if (atoi(inCharArray) > SILO_TIME_MAX){
          serialPort.print("Max silo time allowed is ");
          serialPort.print(SILO_TIME_MAX);
          serialPort.println(" seconds"); 
        }
        else if (atoi(inCharArray) < SILO_TIME_MIN){
          serialPort.print("Min silo time allowed is ");
          serialPort.print(SILO_TIME_MIN);
          serialPort.println(" seconds"); 
        }
        else{
          siloTime = atof(inCharArray);
          siloDuration = siloTime * FF_CPU;
          serialPort.print("Silo time set to "); serialPort.println(siloTime);
        }
      }
      else if (inStr.substring(0,10) == "silopause "){
        inCharArray[0] = inStr[10];
        inCharArray[1] = inStr[11];
        inCharArray[2] = inStr[12];
        inCharArray[3] = inStr[13];
        if (atoi(inCharArray) > SILO_PAUSE_TIME_MAX){
          serialPort.print("Max silo pause time allowed is ");
          serialPort.print(SILO_PAUSE_TIME_MAX);
          serialPort.println(" seconds"); 
        }
        else if (atoi(inCharArray) < SILO_PAUSE_TIME_MIN){
          serialPort.print("Min silo pause time allowed is ");
          serialPort.print(SILO_PAUSE_TIME_MIN);
          serialPort.println(" seconds"); 
        }
        else{
          siloPauseTime = atof(inCharArray);
          siloPauseDuration = siloPauseTime * FF_CPU;
          serialPort.print("Silo Pause time set to ");     serialPort.println(siloPauseTime);
          //serialPort.print("Silo Pause Duration set to "); serialPort.println(siloPauseDuration);
        }
      }
      else if (inStr.substring(0,5)  == "auto "){
        inCharArray[0] = inStr[5];
        inCharArray[1] = inStr[6];
        inCharArray[2] = inStr[7];
        inCharArray[2] = inStr[8];
        if (atoi(inCharArray) > sweepArrayPatternsSize){
          serialPort.print("Max pattern number is ");
          serialPort.println(sweepArrayPatternsSize);
        }
        else {
          sweepArrayPatternNbrAutoFixed = atof(inCharArray);
          sweepArrayPatternNbrAuto      = sweepArrayPatternNbrAutoFixed;
          if (swModeAutoState==SW_ON){
            loadSweepArray(sweepArrayPatternNbrAuto, invertCh2Auto);
            invertCh2 = invertCh2Auto;
          }
          serialPort.print("AUTO Sweep pattern set to "); serialPort.println(sweepArrayPatternNbrAuto);
        }
      }
      else if (inStr.substring(0,6)  == "local "){
        inCharArray[0] = inStr[6];
        inCharArray[1] = inStr[7];
        inCharArray[2] = inStr[8];
        inCharArray[2] = inStr[9];
        if (atoi(inCharArray) > sweepArrayPatternsSize){
          serialPort.print("Max pattern number is ");
          serialPort.println(sweepArrayPatternsSize);
        }
        else {
          sweepArrayPatternNbrLocalFixed = atof(inCharArray);
          sweepArrayPatternNbrLocal      = sweepArrayPatternNbrLocalFixed;
          if (swModeLocalState==SW_ON){
            loadSweepArray(sweepArrayPatternNbrLocal, invertCh2Local);
            invertCh2 = invertCh2Local;
          }
          serialPort.print("LOCAL Sweep pattern set to "); serialPort.println(sweepArrayPatternNbrLocal);
        }
      }
      else if (inStr.substring(0,11) == "autoinvert "){
        if (inStr.substring(11,13) == "on"){
          invertCh2Auto = ON;
          serialPort.println("AUTO Invert Chanel 2 is turned ON");
        }
        else if (inStr.substring(11,14) == "off"){
          invertCh2Auto = OFF;
          serialPort.println("AUTO Invert Chanel 2 is turned OFF");
        }
        if (swModeAutoState==SW_ON){
          invertCh2 = invertCh2Auto;
        }
      }
      else if (inStr.substring(0,12) == "localinvert "){
        if (inStr.substring(12,14) == "on"){
          invertCh2Local = ON;
          serialPort.println("LOCAL Invert Chanel 2 is turned ON");
        }
        else if (inStr.substring(12,15) == "off"){
          invertCh2Local = OFF;
          serialPort.println("LOCAL Invert Chanel 2 is turned OFF");
        }
        if (swModeLocalState==SW_ON){
          invertCh2 = invertCh2Local;
        }
      }
      else if (inStr.substring(0,10) == "autocycle "){
        if (inStr.substring(10,12) == "on"){
          cyclePatternAuto = ON;
          loadSweepArray(sweepArrayPatternNbrAutoList[sweepArrayPatternNbrAutoListPos],invertCh2Auto);
          serialPort.println("AUTO Pattern Cycle is turned ON");
        }
        else if (inStr.substring(10,13) == "off"){
          cyclePatternAuto = OFF;
          loadSweepArray(sweepArrayPatternNbrAutoFixed,invertCh2Auto);
          serialPort.println("AUTO Pattern Cycle is turned OFF");
        }
      }
      else if (inStr.substring(0,11) == "localcycle "){
        if (inStr.substring(11,13) == "on"){
          cyclePatternLocal = ON;
          loadSweepArray(sweepArrayPatternNbrLocalList[sweepArrayPatternNbrLocalListPos],invertCh2Auto);
          serialPort.println("LOCAL Pattern Cycle is turned ON");
        }
        else if (inStr.substring(11,14) == "off"){
          cyclePatternLocal = OFF;
          loadSweepArray(sweepArrayPatternNbrLocalFixed,invertCh2Local);
          serialPort.println("LOCAL Pattern Cycle is turned OFF");
        }
      }
      else if (inStr.substring(0,9)  == "autotest "){
        if (inStr.substring(9,11) == "on"){
          testModeAuto   = ON;
          testModeLocal  = OFF;
          testModeRemote = OFF;
          serialPort.println("AUTO Test is turned ON");
          processNextSiloEvent();
        }
        else if (inStr.substring(9,12) == "off"){
          testModeAuto  = OFF;
          serialPort.println("AUTO Test is turned OFF");
        }
      }
      else if (inStr.substring(0,10) == "localtest "){
        if (inStr.substring(10,12) == "on"){
          testModeLocal  = ON;
          testModeAuto   = OFF;
          testModeRemote = OFF;
          serialPort.println("LOCAL Test is turned ON");
          processNextSiloEvent();
        }
        else if (inStr.substring(10,13) == "off"){
          testModeLocal = OFF;
          serialPort.println("LOCAL Test is turned OFF");
        }
      }
      else if (inStr.substring(0,11) == "remotetest "){
        if (inStr.substring(11,13) == "on"){
          testModeLocal  = OFF;
          testModeAuto   = OFF;
          testModeRemote = ON;
          serialPort.println("REMOTE Test is turned ON");
          processNextSiloEvent();
        }
        else if (inStr.substring(11,14) == "off"){
          testModeRemote = OFF;
          serialPort.println("REMOTE Test is turned OFF");
        }
      }
      else if (inStr.substring(0,9)  == "autolist "){
        serialPort.println("autolist command!!!");
        char inCharElementArray[10];
        char inCharArray[100];
        int newList[10];
        int i = 0;
        int ii = 0;
        int iii = 0;
        bool vallidCommand = true;
        for (i=8;i < inStr.length(); i++){
          inCharArray[i] = inStr[i];
          serialPort.print("inCharArray["); serialPort.print(i); serialPort.print("] = "); serialPort.println(inCharArray[i]);
        }
        i=8;
        while (i<=inStr.length()){
          if (inCharArray[i] == '('){
            i++;
            while ((inCharArray[i] != ')') && (i<=inStr.length())){
              ii = 0;
              while ((inCharArray[i] != ',') && (inCharArray[i] != ')') && (i<=inStr.length())){
                inCharElementArray[ii]=inCharArray[i];
                ii++;
                i++;
              }
              if ((i<inStr.length())){
                inCharElementArray[ii] = '\0';
                newList[iii] = atoi(inCharElementArray);
                iii++;
              }
              i++;
            }
            newList[iii] = -1;
          }
          i++;
        }
        
        if (vallidCommand == true){
          serialPort.print("newList = ");
          for (i=0;newList[i] != -1;i++){
            sweepArrayPatternNbrAutoList[i] = newList[i];
            serialPort.print(newList[i]);
            serialPort.print(" ");
          }
          serialPort.println();

          sweepArrayPatternNbrAutoListSize = i;
          sweepArrayPatternNbrAutoList[i]  = '#';
          sweepArrayPatternNbrAutoListPos  = 0;
          if (cyclePatternAuto == ON)
            loadSweepArray(sweepArrayPatternNbrAutoListPos, invertCh2Auto);
        }
        else{
          serialPort.println("Invallid Command!");
        }
      }
      else if (inStr.substring(0,10) == "locallist "){
        serialPort.println("locallist command!!!");
        char inCharElementArray[10];
        char inCharArray[100];
        int newList[10];
        int i = 0;
        int ii = 0;
        int iii = 0;
        bool vallidCommand = true;
        for (i=9;i < inStr.length(); i++){
          inCharArray[i] = inStr[i];
          serialPort.print("inCharArray["); serialPort.print(i); serialPort.print("] = "); serialPort.println(inCharArray[i]);
        }
        i=8;
        while (i<=inStr.length()){
          if (inCharArray[i] == '('){
            i++;
            while ((inCharArray[i] != ')') && (i<=inStr.length())){
              ii = 0;
              while ((inCharArray[i] != ',') && (inCharArray[i] != ')') && (i<=inStr.length())){
                inCharElementArray[ii]=inCharArray[i];
                ii++;
                i++;
              }
              if ((i<inStr.length())){
                inCharElementArray[ii] = '\0';
                newList[iii] = atoi(inCharElementArray);
                iii++;
              }
              i++;
            }
            newList[iii] = -1;
          }
          i++;
        }
        
        if (vallidCommand == true){
          serialPort.print("newList = ");
          for (i=0;newList[i] != -1;i++){
            sweepArrayPatternNbrLocalList[i] = newList[i];
            serialPort.print(newList[i]);
            serialPort.print(" ");
          }
          serialPort.println();

          sweepArrayPatternNbrLocalListSize = i;
          sweepArrayPatternNbrLocalList[i]  = '#';
          sweepArrayPatternNbrLocalListPos  = 0;
          if (cyclePatternLocal == ON)
            loadSweepArray(sweepArrayPatternNbrLocalListPos, invertCh2Local);
        }
        else{
          serialPort.println("Invallid Command!");
        }
      }
      else if (inStr.substring(0,4)  == "log "){
        if (inStr.substring(4,12) == "monitor "){
          if (inStr.substring(12,14) == "on"){
            if (logMonitorEvent == OFF){
              logMonitorEvent = ON;
              serialPort.println("STS>Log Monitor is turned ON");
            }
          }
          else if (inStr.substring(12,15) == "off"){
            logMonitorEvent = OFF;
            serialPort.println("Log Monitor is turned OFF");
          }
        }
        else if (inStr.substring(4,8)  == "led "){
          if (inStr.substring(8,10) == "on"){
            if (logLedEvent == OFF){
              logLedEvent = ON;
              serialPort.println("STS>Log LED is turned ON");
            }
          }
          else if (inStr.substring(8,11) == "off"){
            if (logLedEvent == ON){
              logLedEvent = OFF;
              serialPort.println("STS>Log LED is turned OFF");
            }
          }
        }
        else if (inStr.substring(4,8)  == "dio "){
          if (inStr.substring(8,10) == "on"){
            if (logDioEvent == OFF){
              logDioEvent = ON;
              serialPort.println("STS>Log DIO is turned ON");
            }
          }
          else if (inStr.substring(8,11) == "off"){
            if (logDioEvent == ON){
              logDioEvent = OFF;
              serialPort.println("STS>Log DIO is turned OFF");
            }
          }
        }
        else if (inStr.substring(4,11) == "status "){
          if (inStr.substring(11,13) == "on"){
            if (logStatusEvent == OFF){
              logStatusEvent = ON;
              serialPort.println("STS>Log Status is turned ON");
            }
          }
          else if (inStr.substring(11,14) == "off"){
            if (logStatusEvent == ON){
              logStatusEvent = OFF;
              serialPort.println("STS>Log Status is turned OFF");
            }
          }
        }
        else if (inStr.substring(4,11) == "remote "){
          if (inStr.substring(11,13) == "on"){
            if (logRemoteStatusEvent == OFF){
              logRemoteStatusEvent = ON;
              serialPort.println("STS>Log Remote Status is turned ON");
            }
          }
          else if (inStr.substring(11,14) == "off"){
            logRemoteStatusEvent = OFF;
            serialPort.println("STS>Log Remote Status is turned OFF");
          }
        }
        else if (inStr.substring(4,13) == "manifold "){
          if (inStr.substring(13,15) == "on"){
            if (logPickAndHoldEvent == OFF){
              logPickAndHoldEvent = ON;
              serialPort.println("STS>Log Pick & Hold is turned ON");
            }
          }
          else if (inStr.substring(13,16) == "off"){
            if (logPickAndHoldEvent == ON){
              logPickAndHoldEvent = OFF;
              serialPort.println("STS>Log Pick & Hold is turned OFF");
            }
          }
        }
        else if (inStr.substring(4,8)  == "dac "){
          if (inStr.substring(8,10) == "on"){
            if (logDacOutputEvent == OFF){
              logDacOutputEvent = ON;
              serialPort.println("STS>Log DAC Output is turned ON");
            }
          }
          else if (inStr.substring(8,11) == "off"){
            if (logDacOutputEvent == ON){
              logDacOutputEvent = OFF;
              serialPort.println("STS>Log DAC Output is turned OFF");
            }
          }
        }
        else if (inStr.substring(4,10) == "sweep "){
          if (inStr.substring(10,12) == "on"){
            if (logSweepStatus == OFF){
              logSweepStatus = ON;
              serialPort.println("STS>Log Sweep status is turned ON");
            }
          }
          else if (inStr.substring(10,13) == "off"){
            if (logSweepStatus == ON){
              logSweepStatus = OFF;
              serialPort.println("STS>Log Sweep status is turned OFF");
            }
          }
        }
        else if (inStr.substring(4,14) == "sweepstep "){
          if (inStr.substring(14,16) == "on"){
            if (logSweepStepStatus == OFF){
              logSweepStepStatus = ON;
              serialPort.println("STS>Log Sweep Step status is turned ON");
            }
          }
          else if (inStr.substring(14,17) == "off"){
            if (logSweepStepStatus == ON){
              logSweepStepStatus = OFF;
              serialPort.println("STS>Log Sweep Step status is turned OFF");
            }
          }
        }
        else if (inStr.substring(4,9)  == "silo "){
          if (inStr.substring(9,11) == "on"){
            if (logSiloStatus == OFF){
              logSiloStatus = ON;
              serialPort.println("STS>Log Silo status is turned ON");
            }
          }
          else if (inStr.substring(9,12) == "off"){
            if (logSiloStatus == ON){
              logSiloStatus = OFF;
              serialPort.println("STS>Log Silo status is turned OFF");
            }
          }
        }
      }
      else if (inStr.substring(0,7)  == "remote "){
        if (inStr.substring(7,13) == "status"){
          logRemoteStatus();
        }
        if (inStr.substring(7,13) == "state "){
          if (inStr.substring(13,15) == "on"){
            if (swModeRemoteState == SW_ON){
              if (remoteState != 1){
                serialPort.print("STS>Turning ON Remote opperation. ");
                remoteState = 1;
              }
//              else{
//                serialPort.print("STS>Remote allready ON. ");
//              }
              delay(10);
              remoteStateStart = now;
/*
              inCharArray[0] = inStr[17];
              inCharArray[1] = inStr[18];
              serialPort.print("Silo 1 inCharArray:"); serialPort.println(atoi(inCharArray));
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Silo 1.\n");
                rmSiloState[0] = SW_ON;
              }
              else
                rmSiloState[0] = SW_OFF;
              
              inCharArray[0] = inStr[19];
              inCharArray[1] = inStr[20];
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Silo 2.\n");
                rmSiloState[1] = SW_ON;
              }
              else
                rmSiloState[1] = SW_OFF;
              
              inCharArray[0] = inStr[21];
              inCharArray[1] = inStr[22];
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Silo 3.\n");
                rmSiloState[2] = SW_ON;
              }
              else
                rmSiloState[2] = SW_OFF;

              inCharArray[0] = inStr[23];
              inCharArray[1] = inStr[24];
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Silo 4.\n");
                rmSiloState[3] = SW_ON;
              }
              else
                rmSiloState[3] = SW_OFF;
              
              inCharArray[0] = inStr[25];
              inCharArray[1] = inStr[26];
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Silo 5.\n");
                rmSiloState[4] = SW_ON;
              }
              else
                rmSiloState[4] = SW_OFF;

              inCharArray[0] = inStr[27];
              inCharArray[1] = inStr[28];
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Silo 6.\n");
                rmSiloState[5] = SW_ON;
              }
              else
                rmSiloState[5] = SW_OFF;

              inCharArray[0] = inStr[31];
              inCharArray[1] = inStr[32];
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Vibe 1.\n");
                rmVib1State = SW_ON;
              }
              else
                rmVib2State = SW_OFF;

              inCharArray[0] = inStr[33];
              inCharArray[1] = inStr[34];
              if (atoi(inCharArray) == 1){
                serialPort.print("STS>Remote ON Vibe 2.\n");
                rmVib2State = SW_ON;
              }
              else
                rmVib2State = SW_OFF;
*/
              //serialPort.print("\ninStr:"); serialPort.println(inStr);
              //for (int a = 38;a <= 45;a++){
              //  serialPort.print("inStr["); serialPort.print(a); serialPort.print("]:["); 
              //  serialPort.print(inStr[a]);  serialPort.println("]");
              //}
              unsigned long temp_seconds  = 0;
              float temp_duration = 0;

              inCharArray[0] = inStr[38];
              inCharArray[1] = inStr[39];
              //serialPort.print("Hours   = ");       serialPort.println(atoi(inCharArray));
              temp_seconds += (atoi(inCharArray) * 60 * 60);
              //serialPort.print("Total-Seconds(hours):");   serialPort.println(temp_seconds);

              inCharArray[0] = inStr[41];
              inCharArray[1] = inStr[42];
              //serialPort.print("Minutes = ");       serialPort.println(atoi(inCharArray));
              temp_seconds += (atoi(inCharArray) * 60);
              //serialPort.print("Total-Seconds(hours & minutes):");   serialPort.println(temp_seconds);

              inCharArray[0] = inStr[44];
              inCharArray[1] = inStr[45];
              //serialPort.print("Seconds = ");       serialPort.println(atoi(inCharArray));
              temp_seconds += (atoi(inCharArray));
              //serialPort.print("Total-Seconds(hours & minutes & seconds):");   serialPort.println(temp_seconds);

              temp_duration = temp_seconds * FF_CPU;
              //serialPort.print("Total-Duration:");  serialPort.println(temp_duration);

//            remoteTimeoutTime       = temp_seconds;
              remoteTimeoutTime       = 60;

//            remoteTimeoutDuration   = temp_duration;
              remoteTimeoutDuration   = remoteTimeoutTime * FF_CPU;
              
              serialPort.print(" - Remote Time set to:");
              serialPort.print(remoteTimeoutTime);
            }
            else{
              if (swModeLocalState == SW_ON)
                serialPort.print("STS>Cannot turn on REMOTE opperation while in LOCAL Mode !");
              else
                serialPort.print("STS>Cannot turn on REMOTE opperation while in AUTO Mode !");
            }
          }
          else if (inStr.substring(13,16) == "off"){
            serialPort.print("STS>Turning OFF Remote opperation.");            
            remoteState = 0;
          }
        }
        else if (inStr.substring(7,12) == "silo "){
          if (inStr.substring(12,15) == "on "){
            inCharArray[0] = inStr[15];
            inCharArray[1] = inStr[16];
            if ((atoi(inCharArray) > 0) && (atoi(inCharArray) <= NBR_SILOS)){
              serialPort.print("STS>Adding Silo "); serialPort.print(inCharArray); serialPort.print(" to Active Remote Silo list.");
              rmSiloState[atoi(inCharArray)-1] = SW_ON;
            }
          }  
          else if (inStr.substring(12,16) == "off "){
            inCharArray[0] = inStr[16];
            inCharArray[1] = inStr[17];
            if ((atoi(inCharArray) > 0) && (atoi(inCharArray) <= NBR_SILOS)){
              serialPort.print("STS>Removing Silo "); serialPort.print(inCharArray); serialPort.print(" from Active Remote Silo list.");
              rmSiloState[atoi(inCharArray)-1] = SW_OFF;
            }
          }  
        }
        else if (inStr.substring(7,12) == "vibe "){
          if (inStr.substring(12,15) == "on "){
            inCharArray[0] = inStr[15];
            inCharArray[1] = inStr[16];
            if (atoi(inCharArray) == 1){
              serialPort.print("STS>Adding Vibe "); serialPort.print(inCharArray); serialPort.print(" to Active Remote Vibe list.");
              rmVib1State = SW_ON;
            }
            if (atoi(inCharArray) == 2){
              serialPort.print("STS>Adding Vibe "); serialPort.print(inCharArray); serialPort.print(" to Active Remote Vibe list.");
              rmVib2State = SW_ON;
            }
          }  
          else if (inStr.substring(12,16) == "off "){
            inCharArray[0] = inStr[16];
            inCharArray[1] = inStr[17];
            if (atoi(inCharArray) == 1){
              serialPort.print("STS>Removing Vibe "); serialPort.print(inCharArray); serialPort.print(" from Active Remote Vibe list.");
              rmVib1State = SW_OFF;
            }
            if (atoi(inCharArray) == 2){
              serialPort.print("STS>Removing Vibe "); serialPort.print(inCharArray); serialPort.print(" from Active Remote Vibe list.");
              rmVib2State = SW_OFF;
            }
          }  
        }
      }

      inStr="";
      serialPort.println();
      delay(100);
    }
  }
}

void logRemoteStatus(){
  delay(10);
  serialPort.print("RMT> ");
  serialPort.print(!swModeAutoState);    serialPort.print(" ");
  serialPort.print(!swModeLocalState);   serialPort.print(" ");
  serialPort.print(!swModeRemoteState);  serialPort.print(" ");
  serialPort.print("- ");

  if (openChuteCount > 0){
    swSiloState[openChuteSilo] = SW_ON;
  }
  
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print(!swSiloState[a]);   serialPort.print(" ");
  }
  serialPort.print("- ");

  if (swVibBothState == SW_ON){
    serialPort.print(1);   serialPort.print(" ");
    serialPort.print(1);   serialPort.print(" ");
  }
  else{
    serialPort.print(!swVib1State);      serialPort.print(" ");
    serialPort.print(!swVib2State);      serialPort.print(" ");
  }
  serialPort.print("- ");
  serialPort.print(remoteState);         serialPort.print(" ");
  serialPort.print("- ");
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print(!rmSiloState[a]);   serialPort.print(" ");
  }
  serialPort.print("- ");

  if (rmVibBothState == SW_ON){
    serialPort.print(1);   serialPort.print(" ");
    serialPort.print(1);   serialPort.print(" ");
  }
  else{
    serialPort.print(!rmVib1State);        serialPort.print(" ");
    serialPort.print(!rmVib2State);        serialPort.print(" ");
  }
  serialPort.print("- ");
  serialPort.print(currentSilo + 1);
  serialPort.print(" ");

  if ((manifold1State[currentSilo] >= HOLD) || (manifold2State[currentSilo] >= HOLD))
    serialPort.print(1);
  else
    serialPort.print(0);
  serialPort.print(" ");

  if (ledVib1State == BLINK)
    serialPort.print(1);
  else
    serialPort.print(0);
  serialPort.print(" ");

  if (ledVib2State == BLINK)
    serialPort.print(1);
  else
    serialPort.print(0);
  if (remoteState == 1){
    int remoteTimeoutTimeInt;
    remoteTimeoutTimeInt = int(remoteTimeoutTime) - int((now - remoteStateStart) / FF_CPU);
    if (remoteTimeoutTimeInt > 60){
      remoteTimeoutTimeInt    = 50;
      remoteTimeoutTime       = 50;
      remoteTimeoutDuration   = remoteTimeoutTime * FF_CPU;
    }
    
    sprintf(strBuf, " - %02d", remoteTimeoutTimeInt); 
    serialPort.print(strBuf); 
    
//    sprintf(strBuf, " - %02d", 
//      int(remoteTimeoutTime) - int((now - remoteStateStart) / FF_CPU));
//    serialPort.print(strBuf); 
  }
  else{
    serialPort.print(" - 00");
  }
  delay(10);
  serialPort.print("\n");
  delay(10);
}

//-------------------------------------------------------------
// Flush the serial buffer
//-------------------------------------------------------------
void flushSerial(){
  while (serialPort.available() > 0) {
    serialPort.read();
  }
  serialPort.flush();
}

//-------------------------------------------------------------
// Reset the CPU
//-------------------------------------------------------------
void softwareReset() {
  serialPort.println("Resetting CPU!");
  delay(500);
  NVIC_SystemReset();
}

//-------------------------------------------------------------
// Log current status to the LCD screen
//-------------------------------------------------------------
byte lcdBlinkState = ON;
void processLogLcdEvent(){
//  lcd.clear();
//  SCREEN_I2C_WIRE.begin();
  lcd.setCursor(0,0);
  lcd.print("Local");
  lcd.setCursor(5,0);
  if (ledLocalState == BLINK){
    if (lcdBlinkState){
      lcd.print("#");
    }
    else{
      lcd.print(" ");
    }
  }
  else if (ledLocalState == ON){
    lcd.print("#");
  }
  else{
    lcd.print("O");
  }

  lcd.setCursor(7,0);
  lcd.print("Auto:");
  lcd.setCursor(11,0);
  if (ledAutoState == BLINK){
    if (lcdBlinkState){
      lcd.print("#");
    }
    else{
      lcd.print(" ");
    }
  }
  else if (ledAutoState == ON){
    lcd.print("#");
  }
  else{
    lcd.print("O");
  }  

  lcd.setCursor(13,0);
  lcd.print("Vibe");
  lcd.setCursor(17,0);
  if (ledVib1State == BLINK){
    if (lcdBlinkState){
      lcd.print("#");
    }
    else{
      lcd.print(" ");
    }
  }
  else if (ledVib1State == ON){
    lcd.print("#");
  }
  else{
    lcd.print("O");
  }
  lcd.setCursor(19,0);
  if (ledVib2State == BLINK){
    if (lcdBlinkState){
      lcd.print("#");
    }
    else{
      lcd.print(" ");
    }
  }
  else if (ledVib2State == ON){
    lcd.print("#");
  }
  else{
    lcd.print("O");
  }
  
  lcd.setCursor(2,2);
  lcd.print("1  2  3  4  5  6");
  lcd.setCursor(2,3);
  for (a=0;a<NBR_SILOS;a++){
    lcd.setCursor((2 + a * 3),3);
    if (ledSiloState[a] == BLINK){
      if (lcdBlinkState){
        lcd.print("#");
      }
      else{
        lcd.print(" ");
      }
    }
    else if (ledSiloState[a] == ON){
      lcd.print("#");
    }
    else{
      lcd.print("O");
    }
  }

//  lcd.setCursor(0,1);
//  lcd.print("Sweep:");
//  lcd.setCursor(6,1);
//  lcd.print(1.0 * (now - lastSweepHop) / FF_CPU);
//  lcd.setCursor(10,1);
//  lcd.print(" Silo:");
//  lcd.setCursor(16,1);
//  lcd.print(1.0 * (now - lastSiloHop) / FF_CPU);
  
  lcdBlinkState = !lcdBlinkState;
}

//-------------------------------------------------------------
// Output raw values in a tab-seperated list
// to be used by Plotter
//-------------------------------------------------------------
void processLogMonitorEvent(){
  unsigned int spacing = 1000;
  unsigned int offset  = 0;

  serialPort.print("MTR>\t");

// Actual DAC-OUTPUT values, given system state
  offset = 0;
  serialPort.print(dacValOut[0] * 20 + spacing * offset);                         
  serialPort.print("\t");
  serialPort.print(dacValOut[1] * 20 + spacing * offset);                         
  serialPort.print("\t");

// Pick and Hold
  offset = 2;
  int value = 0;
  for (a=0;a<NBR_SILOS;a++){
    if (manifold1State[a] == RELEASE)
      value = HOLD;
    else
      value = manifold1State[a];
    serialPort.print(value * 300 + spacing * (offset + a));  
    serialPort.print("\t");
    if (manifold2State[a] == RELEASE)
      value = HOLD;
    else
      value = manifold2State[a];
    serialPort.print(value * 300 + spacing * (offset + a) + 50);  
    serialPort.print("\t");
  }

// LOCAL/AUTO Selection States  
// REMOTE state
  offset = 8;
  if (openChuteCount > 0)
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");// Auto state
  offset = 9;
  if (ledAutoState == BLINK)
    serialPort.print((monitorBlinkState * 800) + spacing * offset);
  else if (ledAutoState == ON)
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");
// Local State
  offset = 10;
  if (ledLocalState == BLINK)
    serialPort.print((monitorBlinkState * 800) + spacing * offset);
  else if (ledLocalState == ON)
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");
  serialPort.println("\n");
  delay(10);
  monitorBlinkState=!monitorBlinkState;
}

//-------------------------------------------------------------
// Output DETAILED raw values in a tab-seperated list 
// to be used by Plotter
//-------------------------------------------------------------
void processLogMonitorFullEvent(){
  unsigned int spacing = 1000;
  unsigned int offset  = 0;

  serialPort.print("MTR>\t");


// RAW Calculated DAC-OUTPUT values
//  offset = 0;
//  serialPort.print(dacVal[0] * 20 + spacing * offset);                         
//  serialPort.print("\t");
//  serialPort.print(dacVal[1] * 20 + spacing * offset);                         
//  serialPort.print("\t");

// Actual DAC-OUTPUT values, given system state
  offset += 2;
  serialPort.print(dacValOut[0] * 20 + spacing * offset);                         
  serialPort.print("\t");
  serialPort.print(dacValOut[1] * 20 + spacing * offset);                         
  serialPort.print("\t");

// LOCAL / AUTO / REMOTE Selection States  
// REMOTE state
  offset+=3;
  if ((openChuteCount > 0) || (swModeRemoteState == SW_ON)){
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
    ledAutoState  = OFF;
    ledLocalState = OFF;
  }
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");
// Auto state
  offset+=1;  
  if (ledAutoState == BLINK)
    serialPort.print((monitorBlinkState * 800) + spacing * offset);
  else if (ledAutoState == ON)
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");
// Local State
  offset+=1;
  if (ledLocalState == BLINK)
    serialPort.print((monitorBlinkState * 800) + spacing * offset);
  else if (ledLocalState == ON)
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");

// Vibrator selection states
  offset+=2;
  if (ledVib1State == BLINK)
    serialPort.print((monitorBlinkState * 800) + spacing * offset);
  else if (ledVib1State == ON)
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");
  offset+=1;
  if (ledVib2State == BLINK)
    serialPort.print((monitorBlinkState * 800) + spacing * offset);
  else if (ledVib2State == ON)
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");


// Silo selection/active states
  offset+=2;
  for (a=0;a<NBR_SILOS;a++){
    if (ledSiloState[a] == BLINK)
      serialPort.print((monitorBlinkState * 800) + spacing * (offset + a));
    else if (ledSiloState[a] >= ON)
      serialPort.print((monitorBlinkState * 400) + spacing * (offset + a));
    else
      serialPort.print(0 + spacing * (offset + a));
    serialPort.print("\t");
  }

// Pick and Hold
//  offset += 2;
  unsigned long value = 0.0;
  for (a=0;a<NBR_SILOS;a++){
    value = manifold1State[a];
    serialPort.print(value * 300 + spacing * (offset + a));  
    serialPort.print("\t");

    value = manifold2State[a];
    serialPort.print(value * 300 + spacing * (offset + a) + 50);  
    serialPort.print("\t");
  }

/*
// Watchdog Indicators
  offset = 24;
  if (watchdogTripCount > 0) 
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");
  offset = 25;
  if (watchdogTripTotalCount > 0) 
    serialPort.print((monitorBlinkState * 400) + spacing * offset);
  else
    serialPort.print(0 + spacing * offset);
  serialPort.print("\t");
*/

  serialPort.println("\n");
  delay(10);
  monitorBlinkState=!monitorBlinkState;
}

//-------------------------------------------------------------
// Output DETAILED raw values in a tab-seperated list 
// to be used by Plotter
//-------------------------------------------------------------
unsigned int  plotOutCnt = 0;
//float      plotOutLst[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
const unsigned int plotOutLstSize = 20;
unsigned int plotOutLst[plotOutLstSize];
void processLogMonitorPlotEvent(){
  unsigned int spacing = 1000;
  unsigned int offset  = 0;

  if (plotOutCnt >= plotOutLstSize){
    delay(20);
    serialPort.print("PLT>(");
    for (plotOutCnt=0;plotOutCnt<plotOutLstSize;plotOutCnt++){
//      serialPort.print(int(plotOutLst[plotOutCnt]));
      serialPort.print(plotOutLst[plotOutCnt]);
      serialPort.print(", ");
//      delay(2);
    }
    serialPort.print(")");
    plotOutCnt = 0;
    delay(5);
    serialPort.print("\n");
    delay(10);
  }
  plotOutLst[plotOutCnt] = (dacValOut[0] / DAC_MAX * DAC_FREQ_MAX);                         
  plotOutLst[plotOutCnt+1] = (dacValOut[1] / DAC_MAX * DAC_FREQ_MAX);                         
  plotOutCnt+=2;
}

//-------------------------------------------------------------
// Log the status to the serial console
//-------------------------------------------------------------
void processLogStatusEvent(){
  char strState[16];
  char strVibe[16];
  char strMode[16];
  char strHeader[16] = "STS>";
  
  if (openChuteCount > 0){
    sprintf(strMode, "CHUTE -  ");
  }
  else{
    if (swModeLocalState == SW_ON)
      sprintf(strMode, "LOCAL -  ");
    else if (swModeAutoState == SW_ON)
      sprintf(strMode, "AUTO  -  ");
    else{
      sprintf(strMode, "REMOTE-  ");
    }
  }

  sprintf(strVibe, "UNSURE  -  ");
  if (swVibBothState   == SW_ON)
    sprintf(strVibe, "Vib 1&2 -  ");
  else if (swVib1State == SW_ON)
    sprintf(strVibe, "Vib 1   -  ");
  else if (swVib2State == SW_ON)
    sprintf(strVibe, "Vib 2   -  ");

  if (siloPauseEvent == OFF){
    int statusMinutesTotal;
    int statusSecondsTotal;
    int statusMinutes;
    int statusSeconds;
    if (openChuteCount == 0){
      statusMinutesTotal = (1.0 * siloDuration   / FF_CPU / 60);
      statusSecondsTotal = (1.0 * siloDuration   / FF_CPU) - (statusMinutesTotal * 60);
      statusMinutes      = (1.0 * (now - lastSiloHop) / FF_CPU / 60);
      statusSeconds      = (1.0 * (now - lastSiloHop) / FF_CPU) - (statusMinutes      * 60);
    }
    else{
      statusMinutesTotal = (1.0 * OPEN_CHUTE_DURATION   / FF_CPU / 60);
      statusSecondsTotal = (1.0 * OPEN_CHUTE_DURATION   / FF_CPU) - (statusMinutesTotal * 60);
      if ((now - lastSiloHop) <= OPEN_CHUTE_DURATION){
        statusMinutes      = (1.0 * (now - lastSiloHop) / FF_CPU / 60);
        statusSeconds      = (1.0 * (now - lastSiloHop) / FF_CPU) - (statusMinutes      * 60);
      }
      else{
        statusMinutes      = statusMinutesTotal;
        statusSeconds      = statusSecondsTotal;
      }
    }
    if ((manifold1State[currentSilo] >= HOLD) || (manifold2State[currentSilo] >= HOLD))
      sprintf(strState, "[  ACTIVE  ]");
    else
      sprintf(strState, "[ INACTIVE ]");

    sprintf(strBuf, "%s%s%sSilo Time  :[%02d:%02d / %02d:%02d] Current Silo :%5s - %s", 
      strHeader,
      strMode, 
      strVibe, 
      statusMinutes, 
      statusSeconds, 
      statusMinutesTotal, 
      statusSecondsTotal, 
      SILO_NAME[currentSilo], 
      strState);
    serialPort.print(strBuf);
  }
  else{
    int statusPauseMinutesTotal = (1.0 * siloPauseDuration    / FF_CPU / 60);
    int statusPauseSecondsTotal = (1.0 * siloPauseDuration    / FF_CPU) - (statusPauseMinutesTotal * 60);
    int statusPauseMinutes      = (1.0 * (now - siloPauseHop) / FF_CPU / 60);
    int statusPauseSeconds      = (1.0 * (now - siloPauseHop) / FF_CPU) - (statusPauseMinutes      * 60);
    sprintf(strBuf, "%s%s%sPause Time :[%02d:%02d / %02d:%02d]  -  Silo Duration  :[%010d / %010d] Current Silo :%s", 
      strHeader,
      strMode, 
      strVibe, 
      statusPauseMinutes, 
      statusPauseSeconds, 
      statusPauseMinutesTotal, 
      statusPauseSecondsTotal, 
      (now - siloPauseHop), 
      siloPauseDuration, 
      SILO_NAME[currentSilo]);
    serialPort.print(strBuf);
  }
  sprintf( strBuf, " - %06d",
    int(remoteTimeoutTime) - int((now - remoteStateStart) / FF_CPU) );
  serialPort.print(strBuf); 

  delay(10);
  serialPort.println();
  delay(10);
}

//-------------------------------------------------------------
// Log the state of all Digital Inputs
//-------------------------------------------------------------
void processLogDioEvent(){
// Log DIO States
  serialPort.print("DIO - ");
  if (swModeLocalState==SW_ON)
    serialPort.print("LCL -");
  else if (swModeAutoState==SW_ON)
    serialPort.print("AUT -");
  else if (swModeRemoteState==SW_ON)
    serialPort.print("REM -");
  else
    serialPort.print("?????:");
  delay(20);
    if (swVib1State==SW_ON)
    serialPort.print("Vib1 - ");
  else if (swVib2State==SW_ON)
    serialPort.print("Vib2 - ");
  else if (swVibBothState==SW_ON)
    serialPort.print("V1&2 - ");
  else
    serialPort.print("???  - ");

  delay(10);
  
//  for (a=0;a<NBR_SILOS;a++){
  for (a=0;a<6;a++){
//    serialPort.print(SILO_NAME[a]); serialPort.print(":");
    serialPort.print("["); serialPort.print(a+1); serialPort.print(":");
    if (swSiloState[a]==SW_ON)
      serialPort.print("ON |");
    else
      serialPort.print("OFF|");
      
    if (swChuteState[a]==OPEN)
      serialPort.print("O|");
    else
      serialPort.print("C|");

    if (swChuteLockState[a]==LOCKED)
      serialPort.print("L] ");
    else
      serialPort.print("U] ");
    
    delay(10);
  }
  delay(10);
  serialPort.print("\n");
}

//-------------------------------------------------------------
// Log the state of the front-pannel LEDs
//-------------------------------------------------------------
void processLogLedEvent(){
  serialPort.print("STS>LEDs - ");
  serialPort.print("AUTO :["); serialPort.print(ledAsciiLookup[ledAutoState]);  serialPort.print("] ");
  serialPort.print("LOCAL:["); serialPort.print(ledAsciiLookup[ledLocalState]); serialPort.print("] ");
  serialPort.print("VIB-1:["); serialPort.print(ledAsciiLookup[ledVib1State]);  serialPort.print("] ");
  serialPort.print("VIB-2:["); serialPort.print(ledAsciiLookup[ledVib2State]);  serialPort.print("] ");
  delay(10);
  serialPort.print("SILOS:");
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("["); serialPort.print(ledAsciiLookup[ledSiloState[a]]); serialPort.print("]");
  }
  serialPort.print("\n");
}

//-------------------------------------------------------------
// Read the state of all Digital inputs
//-------------------------------------------------------------
void processDioReadEvent(){
//  serialPort.println("DIO READ EVENT");
// Temporarily fudge the values to emulate the INPUTS.
  if      (testModeLocal  == ON){
    insertInputValuesLocal();
  }
  else if (testModeAuto   == ON){
    insertInputValuesAuto();
  }
  else if (testModeRemote == ON){
    insertInputValuesRemote();
  }
  else{
    readInputValues();
  }

#ifdef OPEN_CHUTE_TEST
  processOpenChuteTest();
#endif  

// Testing
//  swModeAutoState  = SW_ON;
//  swModeLocalState = SW_OFF;
  
  if (swModeLocalState==swModeAutoState){
    swModeAutoState   = SW_OFF;
    swModeLocalState  = SW_OFF;
    swModeRemoteState = SW_ON;
    Serial.print("LOCAL == AUTO so REMOTE = ON\n");
  }
  else{
    if (swModeRemoteState == SW_ON){
      swModeRemoteState = SW_OFF;
      serialPort.println("STS>################################################");
      delay(10);
      serialPort.println("STS>Turning OFF remote state (processDioReadEvent)");  
      delay(10);
      serialPort.println("STS>################################################");
      delay(10);
      remoteState = 0;
    }
  }

  // State of the Local Mode Switch has changed to ON
  if ((swModeLocalState==SW_ON) && (swModeLocalStatePrevious!=SW_ON)){
    loadSweepArray(sweepArrayPatternNbrLocal, invertCh2Local);
    invertCh2 = invertCh2Local;
    currentSilo = -1;
    processNextSiloEvent();
  }
  // State of the Auto Mode Switch has changed to ON
  else if ((swModeAutoState==SW_ON) && (swModeAutoStatePrevious!=SW_ON)){
    loadSweepArray(sweepArrayPatternNbrAuto, invertCh2Auto);
    invertCh2 = invertCh2Auto;
    currentSilo = -1;
    processNextSiloEvent();
  }
  // State of the Remote Mode Switch has changed to ON
  else if ((swModeRemoteState==SW_ON) && (swModeRemoteStatePrevious!=SW_ON)){
    loadSweepArray(sweepArrayPatternNbrAuto, invertCh2Auto);
    invertCh2 = invertCh2Auto;
    currentSilo = -1;
    processNextSiloEvent();
  }

  if (swVib1State==swVib2State){
//    swVib1State     = SW_OFF;
//    swVib2State     = SW_OFF;
    swVibBothState  = SW_ON;
  }
  else{
    swVibBothState  = SW_OFF;
  }
  
  if ((rmVib1State==SW_ON) && (rmVib2State==SW_ON)){
//    rmVib1State     = SW_OFF;
//    rmVib2State     = SW_OFF;
    rmVibBothState  = SW_ON;
  }
  else{
    rmVibBothState  = SW_OFF;
  }

  if (swModeRemoteState == SW_ON){

    // Instead of using the Silo and Vibe Switch states
    // we will use the states set by the remote protocal
    if (remoteState == ON){
      Serial.print("Remote ON so using rmSiloStates[] and rmVib#States\n");
      swVib1State    = rmVib1State;
      swVib2State    = rmVib2State;
      swVibBothState = rmVibBothState;
   
      for (a=0;a<NBR_SILOS;a++){
        swSiloState[a] = rmSiloState[a];
      }
    }
    else{
      Serial.print("Remote OFF so using swSiloStates[] and swVib#States\n");
      for (a=0;a<NBR_SILOS;a++){
        swSiloState[a] = SW_OFF;
      }
    }
  }
  
  bool siloSelectionHasChanged = false;
  for (a=0;a<NBR_SILOS;a++)
    if (swSiloStatePrevious[a] != swSiloState[a])
      siloSelectionHasChanged == true;
  
  if (siloSelectionHasChanged == true)
    processNextSiloEvent();
  
        
  //--------------------------------------------
  // Check if a chute has just openned or closed
  //--------------------------------------------
  openChuteCount = 0;
  openChuteSilo  = -1;
  for (a=0;a<NBR_SILOS;a++){
    //--------------
    // Chute is OPEN
    //--------------
    if (swChuteState[a]         == OPEN){
      openChuteCount++;
      openChuteSilo=a;
      for (int aa=0;aa<NBR_SILOS;aa++){
        if (aa != openChuteSilo){
          swSiloState[aa] = SW_OFF;
        }
      }
      swVib1State    = SW_ON;
      swVib2State    = SW_ON;
      swVibBothState = SW_ON;
    }

    //-----------------------
    // Chute has just OPENNED
    //-----------------------
    if ((swChuteState[a]         == OPEN) &&
        (swChuteStatePrevious[a] != OPEN)){
      openChuteTime = now;
      beforeOpenChuteSilo = currentSilo;
      if (logSiloStatus == ON){
        serialPort.print("STS>Moved to Silo:"); 
        serialPort.print(SILO_NAME[openChuteSilo]);
        serialPort.print(" - Chute openned");
        serialPort.println();
        delay(10);
      }
      
      // Load the REMOTE sweep array
      //----------------------------
      loadSweepArray(sweepArrayPatternNbrRemote, invertCh2Local);

      // Store the current state, so we can return to it
      //------------------------------------------------
      openChuteSiloPauseEvent = siloPauseEvent;
      openChuteSiloTime       = lastSiloHop;
      openChuteSilo           = a;

      processDioWriteEvent();
      
//#########################################################################
/*      
      if (siloPauseEvent == OFF){
        if (now - vib1ReleaseTime > RAMP_DURATION){
          // Force a soft release of previously held Silo
          //---------------------------------------------
          vib1ReleaseTime = now;          
        }
        if (now - vib2ReleaseTime > RAMP_DURATION){
          // Force a soft release of previously held Silo
          //---------------------------------------------
          vib2ReleaseTime = now;
        }
      }
*/
//#########################################################################

    }

    //----------------------
    // Chute has just CLOSED
    //----------------------
    else if ((swChuteState[a]    == CLOSED) &&
        (swChuteStatePrevious[a] != CLOSED)){

      // Restore the state we were in before the chute openned
      //------------------------------------------------------ 
      currentSilo     = beforeOpenChuteSilo;
      siloPauseEvent  = openChuteSiloPauseEvent;
      lastSiloHop     = openChuteSiloTime + (now - openChuteTime);

      if (logSiloStatus == ON){
        serialPort.print("STS>Moved to Silo:"); 
        serialPort.print(SILO_NAME[currentSilo]);
        serialPort.print(" - Chute closed");
        serialPort.println();
        delay(10);
      }

      if (swModeLocalState == SW_ON){
        // Load the LOCAL sweep array
        //---------------------------
        loadSweepArray(sweepArrayPatternNbrLocal, invertCh2Local);
      }
//      else if (swModeAutoState == SW_ON){
      else{
        // Load the AUTO sweep array
        //--------------------------
        loadSweepArray(sweepArrayPatternNbrAuto, invertCh2Auto);
      }      
      
      // Only start to ramp down if we are not already
      if (now - vib1ReleaseTime > RAMP_DURATION){
        // Force a soft release of previously held Silo          
        //---------------------------------------------
        vib1ReleaseTime = now;
      }
      if (now - vib2ReleaseTime > RAMP_DURATION){
        // Force a soft release of previously held Silo
        //---------------------------------------------
        vib2ReleaseTime = now;
      }


      // We were already vibrating this Silo
      // Move to the next
      //------------------------------------
      if ((currentSilo == a) && (siloPauseEvent == ON)){
        processNextSiloEvent();
      }

      // If there is less than minAfterChuteVibrationTime seconds Vibration-Time left 
      // on this silo, just call it done and move to the next
      //-----------------------------------------------------------------------------
      if ( (now - lastSiloHop) > (siloDuration - (minAfterChuteVibrationTime * FF_CPU)) ){ 
        lastSiloHop = now - siloDuration;
      }
    }
  }

  // Store the States
  swModeLocalStatePrevious  = swModeLocalState;
  swModeAutoStatePrevious   = swModeAutoState;
  swModeRemoteStatePrevious = swModeRemoteState;
  for (a=0;a<NBR_SILOS;a++){
    swSiloStatePrevious[a]    = swSiloState[a];
    swChuteStatePrevious[a]   = swChuteState[a];
  }
}

//-------------------------------------------------------------
// Read the actual switch states
//-------------------------------------------------------------
void readInputValues(){
  swModeLocalState = digitalRead(SW_MODE_LOCAL_PIN);
  swModeAutoState  = digitalRead(SW_MODE_AUTO_PIN);
 
  swVib1State  = digitalRead(SW_VIB_1_PIN);
  swVib2State  = digitalRead(SW_VIB_2_PIN);

  siloCount = 0;
  for (int a=0;a<NBR_SILOS;a++){
    swSiloState[a]      = !digitalRead(SW_SILO_PIN[a]);
    if (swSiloState[a]==SW_ON){
      siloCount++;
    }

#ifndef OPEN_CHUTE_TEST    
    if (swModeAutoState == SW_ON){
      swChuteState[a]     = CLOSED;
      swChuteLockState[a] = LOCKED;
    }
    else if (swModeLocalState == SW_ON){
      swChuteState[a]     = OPEN;
      swChuteLockState[a] = UNLOCKED;
    }
    else if (swModeRemoteState == SW_ON){
      swChuteState[a]     = CLOSED;
      swChuteLockState[a] = LOCKED;
    }
#else
  #ifdef CHUTE_AND_LOCK_CONNECTED
    swChuteState[a]     = digitalRead(SW_CHUTE_PIN[a]);
    swChuteLockState[a] = digitalRead(SW_CHUTE_LOCK_PIN[a]);
  #else
    swChuteState[a]     = CLOSED;
    swChuteLockState[a] = LOCKED;
  #endif
#endif  

  }
}

//-------------------------------------------------------------
// Fudge reading the switch states and use set pre-defined 
// values for testing
//-------------------------------------------------------------
void insertInputValuesLocal(){
  swModeLocalState    = SW_ON;
  swModeAutoState     = SW_OFF;
  swModeRemoteState   = SW_OFF;

  swVib1State         = SW_OFF;
  swVib2State         = SW_OFF;
  swVibBothState      = SW_ON;
  
  swSiloState[0]      = SW_ON;
  swSiloState[1]      = SW_ON;
  swSiloState[2]      = SW_ON;
  swSiloState[3]      = SW_ON;
  swSiloState[4]      = SW_ON;
  swSiloState[5]      = SW_ON;
}

//-------------------------------------------------------------
// Fudge reading the switch states and use set pre-defined 
// values for testing
//-------------------------------------------------------------
void insertInputValuesAuto(){
  swModeLocalState    = SW_OFF;
  swModeAutoState     = SW_ON;
  swModeRemoteState   = SW_OFF;

  swVib1State         = SW_OFF;
  swVib2State         = SW_OFF;
  swVibBothState      = SW_ON;
  
  swSiloState[0]      = SW_ON;
  swSiloState[1]      = SW_ON;
  swSiloState[2]      = SW_ON;
  swSiloState[3]      = SW_ON;
  swSiloState[4]      = SW_ON;
  swSiloState[5]      = SW_ON;
}

//-------------------------------------------------------------
// Simulate openning and closing one Silo's chute  for testing
//-------------------------------------------------------------
void processOpenChuteTest(){
  // Open and close the chute at a regular interval
  if (now - openChuteTestTimeOff > OPEN_CHUTE_TEST_DURATION_OFF){     // Open the chute 
    if (swChuteState[chuteTestSilo] != OPEN){
      swChuteState[chuteTestSilo]     = OPEN;
      swChuteLockState[chuteTestSilo] = UNLOCKED;
      openChuteTime = now;
      openChuteTestTimeOn = now;

      for (a=0;a<NBR_SILOS;a++){
        if (a != chuteTestSilo){
          swChuteState[a]     = CLOSED;
          swChuteLockState[a] = LOCKED;
        }
      }
    
    }
    else if (now - openChuteTestTimeOn > OPEN_CHUTE_TEST_DURATION_ON){     // Close the chute 
      swChuteState[chuteTestSilo]     = CLOSED;
      swChuteLockState[chuteTestSilo] = LOCKED;
      openChuteTestTimeOff = now;

      if (chuteTestSilo == 0){
        chuteTestSilo = 5;
      }
      else{
        chuteTestSilo--;
      }
    }
  }
}

//-------------------------------------------------------------
// Fudge reading the switch states and use set pre-defined 
// values for testing
// Changing vibrators with each Silo change
//-------------------------------------------------------------
void insertInputValuesAutoSwitchingVibA(){
  swModeLocalState  = SW_OFF;
  swModeAutoState   = SW_ON;
  swModeRemoteState = SW_OFF;

  if      ((currentSilo == 0) || (currentSilo == 3)){
    swVib1State    = SW_ON;
    swVib2State    = SW_OFF;
    swVibBothState = SW_OFF;
  }
  else if ((currentSilo == 1) || (currentSilo == 4)){
    swVib1State    = SW_OFF;
    swVib2State    = SW_ON;
    swVibBothState = SW_OFF;
  }
  else if ((currentSilo == 2) || (currentSilo == 5)){
    swVib1State    = SW_OFF;
    swVib2State    = SW_OFF;
    swVibBothState = SW_ON;
  }

  swSiloState[0] = SW_ON;
  swSiloState[1] = SW_ON;
  swSiloState[2] = SW_ON;
  swSiloState[3] = SW_ON;
  swSiloState[4] = SW_ON;
  swSiloState[5] = SW_ON;

  swChuteState[0] = CLOSED;
  swChuteState[1] = CLOSED;
  swChuteState[2] = CLOSED;
  swChuteState[3] = CLOSED;
  swChuteState[4] = CLOSED;
  swChuteState[5] = CLOSED;
  
  swChuteLockState[0] = LOCKED;
  swChuteLockState[1] = LOCKED;
  swChuteLockState[2] = LOCKED;
  swChuteLockState[3] = LOCKED;
  swChuteLockState[4] = LOCKED;
  swChuteLockState[5] = LOCKED;
}

//-------------------------------------------------------------
// Fudge reading the switch states and use set pre-defined 
// values for testing
// Changing vibrators during each Silo Sweep
//-------------------------------------------------------------
void insertInputValuesAutoSwitchingVibB(){
  swModeLocalState  = SW_OFF;
  swModeAutoState   = SW_ON;
  swModeRemoteState = SW_OFF;

  if (now - lastSiloHop >= (siloDuration / 3 * 2)){
    swVib1State    = SW_OFF;
    swVib2State    = SW_ON;
    swVibBothState = SW_OFF;
  }
  else if (now - lastSiloHop >= (siloDuration / 3 * 1)){
    swVib1State    = SW_OFF;
    swVib2State    = SW_OFF;
    swVibBothState = SW_ON;
  }
  else{
    swVib1State    = SW_ON;
    swVib2State    = SW_OFF;
    swVibBothState = SW_OFF;
  }
  
  swSiloState[0] = SW_ON;
  swSiloState[1] = SW_OFF;
  swSiloState[2] = SW_ON;
  swSiloState[3] = SW_OFF;
  swSiloState[4] = SW_ON;
  swSiloState[5] = SW_OFF;

  swChuteState[0] = CLOSED;
  swChuteState[1] = CLOSED;
  swChuteState[2] = CLOSED;
  swChuteState[3] = CLOSED;
  swChuteState[4] = CLOSED;
  swChuteState[5] = CLOSED;
  
  swChuteLockState[0] = LOCKED;
  swChuteLockState[1] = LOCKED;
  swChuteLockState[2] = LOCKED;
  swChuteLockState[3] = LOCKED;
  swChuteLockState[4] = LOCKED;
  swChuteLockState[5] = LOCKED;
}

//-------------------------------------------------------------
// Fudge reading the switch states and use set pre-defined 
// values for testing
//-------------------------------------------------------------
void insertInputValuesRemote(){
  swModeLocalState  = SW_OFF;
  swModeAutoState   = SW_OFF;
  swModeRemoteState = SW_ON;

  swVib1State       = SW_OFF;
  swVib2State       = SW_OFF;
  swVibBothState    = SW_ON;

  swSiloState[0]    = SW_OFF;
  swSiloState[1]    = SW_OFF;
  swSiloState[2]    = SW_OFF;
  swSiloState[3]    = SW_OFF;
  swSiloState[4]    = SW_OFF;
  swSiloState[5]    = SW_OFF;
}

//-------------------------------------------------------------
// Write the appropriate state to all digital outputs
//-------------------------------------------------------------
void processDioWriteEvent(){
  ledLocalState = !swModeLocalState;
  ledAutoState  = !swModeAutoState;

 if (swVibBothState == SW_ON){
    ledVib1State = ON;
    ledVib2State = ON;
  }
  else{
    ledVib1State = !swVib1State;
    ledVib2State = !swVib2State;
  }
  if ((ledVib1State == ON) && (manifold1State[currentSilo] >= HOLD)){
    ledVib1State = BLINK;
  }
  if ((ledVib2State == ON) && (manifold2State[currentSilo] >= HOLD)){
    ledVib2State = BLINK;
  }

  
  // Light the selected SILO's LED's
  if (siloPauseEvent == OFF){
    for (a=0;a<NBR_SILOS;a++){
      ledSiloState[a] = !swSiloState[a];
    }
    // Blink the current active SILO's LED's (if not in OFF state)
//    if (swModeRemoteState == SW_OFF){
//      ledSiloState[currentSilo] = BLINK;
//    }
    ledSiloState[currentSilo] = BLINK;
  }
 
  processPickAndHold();
}
  
//-------------------------------------------------------------
// Direct air to the current Silo if the Silo is set to active.
// This is done in a number of stages.
//   1. The DAC output is ramped down to nothing.
//   2. The HELD Silo is RELEASED.
//   3. The new Silo is PICKED
//   4. The DAC output is ramped back up.
//   5. The new Silo is HELD
//-------------------------------------------------------------
void processPickAndHold(){
  // Process those which require releasing first
  for (a=0;a<NBR_SILOS;a++){
    if ((a != currentSilo) && ( a != openChuteSilo)){
      processPickAndHoldOneSilo(a);
    }
  }
  
  if      ( (manifold1State[currentSilo] >= OFF) || 
            (manifold2State[currentSilo] >= OFF) )
    processPickAndHoldOneSilo(currentSilo);
  else if ( (manifold1State[openChuteSilo] >= OFF) ||
            (manifold2State[openChuteSilo] >= OFF) )
    processPickAndHoldOneSilo(openChuteSilo);
}

void processPickAndHoldOneSilo(int a){
  //---------------------------------------------
  // If in LOCAL or AUTO and a Silo is selected,
  // and that Silo has the Chute,
  // and Chute-Lock in the appropriate state
  //---------------------------------------------
  if ( ( (swModeLocalState    == SW_ON)   &&      
         (currentSilo         == a)       &&     
         (swSiloState[a]      == SW_ON)   &&    // In Local mode, Silo is selected, Chute is closed and locked
         (swChuteState[a]     == CLOSED)  &&     
         (swChuteLockState[a] == LOCKED)       
       )                                  ||     
       ( (swModeAutoState     == SW_ON)   &&     
         (currentSilo         == a)       &&     
         (swSiloState[a]      == SW_ON)   &&    // In Auto mode,  Silo is selected, Chute is closed and locked
         (swChuteState[a]     == CLOSED)  &&     
         (swChuteLockState[a] == LOCKED)         
       )                                  ||
       ( (swModeRemoteState   == SW_ON)   &&
         (remoteState         == ON)      &&     
         (currentSilo         == a)       &&     
         (swSiloState[a]      == SW_ON)   &&    // In Remote mode,  Silo is selected, Chute is closed and locked
         (swChuteState[a]     == CLOSED)  &&     
         (swChuteLockState[a] == LOCKED)         
       )                                  ||
       ( (openChuteCount      > 0)        &&    // A Chute is open and unlocked
         (openChuteSilo       == a)
       )
     ){
    //---------------------
    // If Vibrator 1 is OFF
    //---------------------
    if ((swVib1State != SW_ON) && (swVibBothState != SW_ON)){                                         // If Vibrator IS NOT set to active
      //ledVib1State = BLINK;

      //----------------------------
      // If a Silo is currently held
      // Start Releasing.
      //----------------------------
      if (manifold1State[a] >= HOLD){                                                                 // Time to start releasing Vibrator 1
        if (logPickAndHoldEvent == ON){
          serialPort.print("RELEASEING - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:1");
        }
        manifold1State[a] = RELEASE;
        vib1ReleaseTime = now;
      }

      //---------------------------------
      // If a Silo has finished releasing
      // Fully Release.
      //---------------------------------
      else if ((manifold1State[a] >= RELEASE) && (now - vib1ReleaseTime >= RAMP_DURATION)){           // Time to Release the solanoid for Vibrator 1
        if (logPickAndHoldEvent == ON){
          serialPort.print("RELEASED   - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:1");
        }
        digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], OFF);
        digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], OFF);
        manifold1State[a] = OFF;
      }
    }

    //---------------------
    // If Vibrator 2 is OFF
    //---------------------
    if ((swVib2State != SW_ON) && (swVibBothState != SW_ON)){                                         // If Vibrator 2 IS NOT set to active
      //ledVib2State = BLINK;

      //----------------------------
      // If a Silo is currently held
      // Start Releasing.
      //----------------------------
      if (manifold2State[a] >= HOLD){                                                                 // Time to start releasing Vibrator 2
        if (logPickAndHoldEvent == ON){
          serialPort.print("RELEASING  - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:2");
        }
        manifold2State[a] = RELEASE;
        vib2ReleaseTime = now;
      }

      //---------------------------------
      // If a Silo has finished releasing
      // Fully Release.
      //---------------------------------
      else if ((manifold2State[a] >= RELEASE) && (now - vib2ReleaseTime >= RAMP_DURATION)){           // Time to Release the solanoid for Vibrator 2
        if (logPickAndHoldEvent == ON){
          serialPort.print("RELEASED   - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:2");
        }
        digitalWrite(AIR_MANIFOLD_2_PICK_PIN[a], OFF);
        digitalWrite(AIR_MANIFOLD_2_HOLD_PIN[a], OFF);
        manifold2State[a] = OFF;
      }
    }

    //---------------------
    // If Vibrator 1 is ON
    //---------------------
    if ((swVib1State == SW_ON) || (swVibBothState == SW_ON)){                                         // If Vibrator 1 IS set to active  
      //ledVib1State = BLINK;
      
      //---------------------------
      // If Silo is Released. (OFF)
      //---------------------------
      if ((manifold1State[a] == OFF) &&
          (now - vib1ReleaseTime >= RAMP_DURATION) && 
          (now - vib2ReleaseTime >= RAMP_DURATION)){                                                  // Time to PICK solanoid for Vibrator 1
        if ((currentSilo==0) && (manifold1State[previousSilo] == HOLD)){
          if (logPickAndHoldEvent == ON){
            //serialPort.println("Waiting...");
          }
        }

        //--------------------------------
        // If system is NOT in PAUSE state
        //--------------------------------
        else if (siloPauseEvent == OFF){
          if (logPickAndHoldEvent == ON){
            serialPort.print("PICK       - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:1");
          }
          digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], ON);
          digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], OFF);
          manifold1State[a] = PICK;
          vib1PickTime = now;
          lastPickHop = now;
        }
      }
      
      //------------------------------------------------
      // If Silo is PICKed and it's time to move to HOLD
      //------------------------------------------------
      else if ((manifold1State[a] == PICK) && 
               (now - lastPickHop >= PICK_DURATION)){                                                 // Time to HOLD solanoid for Vibrator 1
        if (logPickAndHoldEvent == ON){
          serialPort.print("HOLD       - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:1");
        }
        digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], ON);
        digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], OFF);
        manifold1State[a] = HOLD;
      }
    }
    
    //---------------------
    // If Vibrator 2 is ON
    //---------------------
    if ((swVib2State == SW_ON) || (swVibBothState == SW_ON)){                                         // If Vibrator 2 IS set to active
      //ledVib2State = BLINK;

      //---------------------------
      // If Silo is Released. (OFF)
      //---------------------------
      if ((manifold2State[a] == OFF) && 
          (now - vib1ReleaseTime >= RAMP_DURATION) && 
          (now - vib2ReleaseTime >= RAMP_DURATION)){                                                  // Time to PICK solanoid for Vibrator 2
        if ((currentSilo==0) && (manifold2State[previousSilo] == HOLD)){
          if (logPickAndHoldEvent == ON){
            //serialPort.println("Waiting...");
          }
        }
        
        //--------------------------------
        // If system is NOT in PAUSE state
        //--------------------------------
        else if (siloPauseEvent == OFF){
          if (logPickAndHoldEvent == ON){
            serialPort.print("PICK       - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:2");
          }
          digitalWrite(AIR_MANIFOLD_2_PICK_PIN[a], ON);
          digitalWrite(AIR_MANIFOLD_2_HOLD_PIN[a], OFF);
          manifold2State[a] = PICK;
          vib2PickTime = now;
          lastPickHop = now;
        }
      }
      
      //------------------------------------------------
      // If Silo is PICKed and it's time to move to HOLD
      //------------------------------------------------
      else if ((manifold2State[a] == PICK) && 
               (now - lastPickHop >= PICK_DURATION)){                                                 // Time to HOLD solanoid for Vibrator 2
        if (logPickAndHoldEvent == ON){
          serialPort.print("HOLD       - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:2");
        }
        digitalWrite(AIR_MANIFOLD_2_HOLD_PIN[a], ON);
        digitalWrite(AIR_MANIFOLD_2_PICK_PIN[a], OFF);
        manifold2State[a] = HOLD;
      }
    }
  }
  
  //------------------------------------------------
  // If NOT in LOCAL or AUTO or NO Silo is selected,
  // or a selected Silo has the Chute,
  // or Chute-Lock OPEN
  //------------------------------------------------
  else{
    //----------------------------
    // If a Silo is currently held
    // Start Releasing.
    //----------------------------
    if (manifold1State[a] >= HOLD){                                                                   // Time to start releasing Vibrator 1
      if (logPickAndHoldEvent == ON){
        serialPort.print("RELEASING  - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:1");
      }
      digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], OFF);
      digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], OFF);
      manifold1State[a] = RELEASE;
      vib1ReleaseTime = now;
    }
    
    //---------------------------------
    // If a Silo has finished releasing
    // Fully Release.
    //---------------------------------
    else if ((manifold1State[a] >= RELEASE) && (now - vib1ReleaseTime >= RAMP_DURATION)){             // Time to release solanoid for Vibrator 1
      if (logPickAndHoldEvent == ON){
        serialPort.print("RELEASED   - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:1");
      }
      digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], OFF);
      digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], OFF);
      manifold1State[a] = OFF;
    }

    //----------------------------
    // If a Silo is currently held
    // Start Releasing.
    //----------------------------
    if (manifold2State[a] >= HOLD){                                                                   // Time to start releasing Vibrator 2
      if (logPickAndHoldEvent == ON){
        serialPort.print("RELEASING  - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:2");
      }
      digitalWrite(AIR_MANIFOLD_2_PICK_PIN[a], OFF);
      digitalWrite(AIR_MANIFOLD_2_HOLD_PIN[a], OFF);
      manifold2State[a] = RELEASE;
      vib2ReleaseTime = now;
    }

    //---------------------------------
    // If a Silo has finished releasing
    // Fully Release.
    //---------------------------------
    else if ((manifold2State[a] >= RELEASE) && (now - vib2ReleaseTime >= RAMP_DURATION)){             // Time to release solanoid for Vibrator 2
      if (logPickAndHoldEvent == ON){
        serialPort.print("RELEASED   - Silo:"); serialPort.print(a); serialPort.print("-"); serialPort.print(SILO_NAME[a]); serialPort.println(" Vib:2");
      }
      digitalWrite(AIR_MANIFOLD_2_PICK_PIN[a], OFF);
      digitalWrite(AIR_MANIFOLD_2_HOLD_PIN[a], OFF);
      manifold2State[a] = OFF;
    }
  }
}

//-------------------------------------------------------------
// Check if it is time to Pause,
// before starting a new cycle when in Auto Mode
//-------------------------------------------------------------
void checkSiloPause(){
#ifdef SILO_PAUSE_EVENT
  if ( (siloPauseEvent  == OFF)           &&
       (currentSilo     <  previousSilo)  && 
       (previousSilo    == NBR_SILOS - 1) &&
       (swModeAutoState == SW_ON) )       {
    if (logSiloStatus   == ON){
      serialPort.println("STS>Turning siloPauseEvent ON");
      serialPort.print("STS>Pausing for ["); 
      serialPort.print(siloPauseTime);
      serialPort.println("] seconds");
    }
    siloPauseEvent = ON;
    siloPauseHop = now;
  }
#endif  
}

//-------------------------------------------------------------
//Output using Event-Loop timing
//-------------------------------------------------------------
void processOutputEvent(){
//  serialPort.println("OUTPUT EVENT");
  writeDacs();
}

//-------------------------------------------------------------
//Output using interupt-timer timing
//-------------------------------------------------------------
//  static void timer_dac_out_handler(stimer_t *htim) {
//    UNUSED(htim);
//  //  serialPort.println("INTERUPT OUTPUT EVENT");
//    writeDacs();
//  }
static void timer_dac_out_callback(void)
{
//  serialPort.println("INTERUPT OUTPUT EVENT");
  writeDacs();
}

void establishFrequency(){
// Establish the frequency
  if (sweepStartFrequency < sweepEndFrequency){
    frequency = 1.0 * (sweepStepPercent / 100) * sweepFrequencyRange + sweepStartFrequency;
  }
  else if (sweepStartFrequency > sweepEndFrequency){
    frequency = sweepStartFrequency - 1.0 * (sweepStepPercent / 100) * sweepFrequencyRange;
  }
  else{
    frequency = 1.0 * sweepStartFrequency;
  }
}

//-------------------------------------------------------------
//Write values to DACs
//-------------------------------------------------------------
void writeDacs(){
  if ((currentSilo < 0) || (currentSilo >= NBR_SILOS))
    return;  
    
// Establish the sweepStepPercent
  now=micros();
  sweepStepPercent = 1.0 * (now - lastSweepStepHop) / (sweepStepDuration) * 100;
  if (sweepStepPercent >= 100){
    sweepStepPercent = 99;
    processNextSweepStepEvent();
    lastSweepStepHop = now;
    return;
  }
  

  establishFrequency();
  
// Establish the sweep percent
  sweepPercent = 1.0 * (now - lastSweepHop) / (sweepCompleteTime * FF_CPU) * 100;

// Establish the ramp1Percent
  // Ramp UP
  if (manifold1State[currentSilo] >= HOLD){
    if ((now - vib1PickTime) <= RAMP_DURATION){
      ramp1Percent = 1.0 * (now - vib1PickTime) / (RAMP_DURATION) * 100;
    }
    else{
      ramp1Percent = 100;
    }
  }
  // Ramp DOWN
  else if ( (manifold1State[currentSilo] <= RELEASE) &&                                           // Just beginning to release solanoid
            ( (swSiloState[previousSilo] == SW_ON) || (now - lastSiloHop > RAMP_DURATION) ) ) {   // Previous Silo was active or we are partially through this silo
    if ((now - vib1ReleaseTime) <= RAMP_DURATION){
      ramp1Percent = 1.0 * (now - vib1ReleaseTime) / (RAMP_DURATION) * 100;
      ramp1Percent = 100 - ramp1Percent;
    }
  }
  else{
    ramp1Percent = 0;
  }

// Establish the ramp2Percent
  // Ramp UP
  if (manifold2State[currentSilo] >= HOLD){
    if ((now - vib2PickTime) <= RAMP_DURATION){
      ramp2Percent = 1.0 * (now - vib2PickTime) / (RAMP_DURATION) * 100;
    }
    else{
      ramp2Percent = 100;
    }
  }
  // Ramp DOWN
  else if ( (manifold2State[currentSilo] <= RELEASE) &&                                           // Just beginning to release solanoid
            ( (swSiloState[previousSilo] == SW_ON) || (now - lastSiloHop > RAMP_DURATION) ) ) {   // Previous Silo was active or we are partially throgh this silo
    if ((now - vib2ReleaseTime) <= RAMP_DURATION){
      ramp2Percent = 1.0 * (now - vib2ReleaseTime) / (RAMP_DURATION) * 100;
      ramp2Percent = 100 - ramp2Percent;
    }
  }
  else{
    ramp2Percent = 0;
  }
  
// Establish DAC percent
  dacPercent = 100 * (frequency - DAC_FREQ_MIN) / (DAC_FREQ_RANGE);

// Establish dacVal[0]
  dacVal[0] = dacPercent * dacRange / 100 + DAC_MIN;
  if (dacVal[0] > DAC_MAX)
    dacVal[0] = DAC_MAX;
  else if (dacVal[0] < DAC_MIN)
    dacVal[0] = DAC_MIN;
    
// Establish dacVal[1]
  if (invertCh2 == ON){
    dacVal[1] = DAC_MAX - dacVal[0] + sweepArrayOffset;
    if (dacVal[1] > DAC_MAX)
      dacVal[1] = DAC_MAX;
    else if (dacVal[1] < DAC_MIN)
      dacVal[1] = DAC_MIN;
  }
  else{
    dacVal[1] = dacVal[0];
  }

// Ensure change is never too rapid. 
// This caters for the inversion being turned on/off while outputing
  unsigned long maxDacChange;
  if ( (now - vib1ReleaseTime < RAMP_DURATION) ||
       (now - vib1ReleaseTime < RAMP_DURATION) ||
       (now - vib1PickTime    < RAMP_DURATION) ||
       (now - vib2PickTime    < RAMP_DURATION) ){
    maxDacChange = 5;
  }
  else{
    maxDacChange = 1;
  }
  
  if (dacValOut[0] > DAC_MAX)
    dacValOut[0] = DAC_MAX;  
  if (dacValOut[1] > DAC_MAX)
    dacValOut[1] = DAC_MAX;  
  if (dacVal[0] > (dacValOut[0] + maxDacChange))
    dacValOut[0] += maxDacChange;
  else if (dacVal[0] < (dacValOut[0] - maxDacChange))
    dacValOut[0] -= maxDacChange;
  else
    dacValOut[0] = dacVal[0];
  if (dacVal[1] > (dacValOut[1] + maxDacChange))
    dacValOut[1] += maxDacChange;
  else if (dacVal[1] < (dacValOut[1] - maxDacChange))
    dacValOut[1] -= maxDacChange;
  else
    dacValOut[1] = dacVal[1];

// Apply the ramp
//  dacValOut[0] = dacValOut[0] * ramp1Percent / 100 * rampPercent / 100;
//  dacValOut[1] = dacValOut[1] * ramp2Percent / 100 * rampPercent / 100;
  dacValOut[0] = dacValOut[0] * ramp1Percent / 100;
  dacValOut[1] = dacValOut[1] * ramp2Percent / 100;
  
// Write to the DACs
  DAC_I2C_WIRE.beginTransmission(DAC_ADDR1);
  DAC_I2C_WIRE.write(DAC_CMD_WRITEDAC);
  DAC_I2C_WIRE.write(int(dacValOut[0]));
#ifdef DAC_MCP4725
  DAC_I2C_WIRE.write((uint8_t)0x0);  // bottom four bits are 0x0
#endif
  DAC_I2C_WIRE.endTransmission();

  DAC_I2C_WIRE.beginTransmission(DAC_ADDR2);
  DAC_I2C_WIRE.write(DAC_CMD_WRITEDAC);
  DAC_I2C_WIRE.write(int(dacValOut[1]));
#ifdef DAC_MCP4725
  DAC_I2C_WIRE.write((uint8_t)0x0);  // bottom four bits are 0x0
#endif
  DAC_I2C_WIRE.endTransmission();

#ifdef LOG_DAC_OUTPUT_RT
  logDacOutput();
  serialPort.println();
#endif
}

//-------------------------------------------------------------
//Log DAC output to the serial console
//-------------------------------------------------------------
void logDacOutput(){
  serialPort.print("# ");
  serialPort.print(sweepPercent);           serialPort.print("%  "); 
  serialPort.print(sweepStepPercent);       serialPort.print("%  "); 
  serialPort.print(rampPercent);            serialPort.print("A  ");
  serialPort.print(frequency);              serialPort.print("Hz - ");
  serialPort.print(sweepFrequencyRange);    serialPort.print("R "); 
  serialPort.print(sweepStartFrequency);    serialPort.print("S "); 
  serialPort.print(sweepEndFrequency);      serialPort.print("E "); 
  serialPort.print(dacPercent);             serialPort.print("%  [");
  serialPort.print(dacValOut[0]);           serialPort.print("|");
  serialPort.print(dacValOut[1]);           serialPort.print("]");
  serialPort.println("\n");
  delay(10);
}

//-------------------------------------------------------------
//Load the next part of the sweep definition
//-------------------------------------------------------------
void processNextSweepStepEvent(){
  sweepArrayPos++;
  getNextSweepStep();

  sweepStepCompleteMsg            = true;
  sweepStepCompleteStartFrequency = sweepStartFrequency;
  sweepStepCompleteEndFrequency   = sweepEndFrequency;
  sweepStepCompleteFrequencyRange = sweepFrequencyRange;
  sweepStepCompleteTime           = sweepStepTime;
}

//-------------------------------------------------------------
//Get the next part of the sweep
//-------------------------------------------------------------
void getNextSweepStep(){
  if (sweepArrayPos >= sweepArraySize){
    getStartSweep();
  }

  sweepStartFrequency = sweepArray[sweepArrayPos][0];
  sweepEndFrequency   = sweepArray[sweepArrayPos][1];
  sweepStepTime       = sweepArray[sweepArrayPos][2];

// Adjust the sweepTime based on the sweepTotalTime
  sweepTimeAllSteps = 0;
  for (a=0;a<sweepArraySize;a++){
    sweepTimeAllSteps += sweepArray[a][2];
  }
  sweepStepTime = 1.0 * sweepStepTime * sweepTotalTime / sweepTimeAllSteps;
  sweepStepDuration = sweepStepTime * FF_CPU;
  
  if (sweepStartFrequency < sweepEndFrequency){
    sweepFrequencyRange = sweepEndFrequency - sweepStartFrequency;
  }
  else{
    sweepFrequencyRange = sweepStartFrequency - sweepEndFrequency;
  }
}

//-------------------------------------------------------------
//Get the start of the sweep
//-------------------------------------------------------------
void getStartSweep(){
  sweepArrayPos = 0;
  sweepCompleteTime = (1.0 * (now - lastSweepHop) / FF_CPU);
  sweepCompleteMsg = true;
  lastSweepHop = now;
}

//-------------------------------------------------------------
//Establish which Silo if any to vibrate next
//-------------------------------------------------------------
void processNextSiloEvent(){
  previousSilo = currentSilo;
  currentSilo++;

// If in AUTO mode return the next Silo  
  if (swModeAutoState == SW_ON){
    if (currentSilo >= NBR_SILOS){
      currentSilo = 0;
    }
    if ((currentSilo == 0) && (cyclePatternAuto == ON)){
      getNextSweepPatternAuto = true;
    }
    checkSiloPause();
  }

// else If in LOCAL mode return the next selected Silo.
  else if (swModeLocalState == SW_ON){           
    while ((swSiloState[currentSilo]==SW_OFF) && (currentSilo < NBR_SILOS))
      currentSilo++;
    if (currentSilo >= NBR_SILOS){
      currentSilo = 0;
      if ((currentSilo == 0) && (cyclePatternLocal == ON)){
        getNextSweepPatternLocal = true;
      }
      while ((swSiloState[currentSilo]==SW_OFF) && (currentSilo < NBR_SILOS)){
        currentSilo++;
      }
      if ((swSiloState[currentSilo]==SW_OFF)){
        currentSilo = -1;
      }
    }
    int activeSiloCount = 0;
    int newSilo = -1;
    for (a=0;a<NBR_SILOS;a++){
      if (swSiloState[a]==SW_ON){
        activeSiloCount++;
        newSilo = a;
      }
    }
    if (activeSiloCount==0){
      currentSilo = -1;
    }
    else if (activeSiloCount == 1){
      currentSilo = newSilo;
    }
    swVib1State = SW_OFF;
    swVib2State = SW_OFF;

    vib1ReleaseTime = now;  // Force a soft release of previously held Silo          
    vib2ReleaseTime = now;  // Force a soft release of previously held Silo
  }
  else  if (swModeRemoteState == SW_ON){
    if (currentSilo >= NBR_SILOS){
      currentSilo = 0;
    }
    if ((currentSilo == 0) && (cyclePatternAuto == ON)){
      getNextSweepPatternAuto = true;
    }
  }

  if (logSiloStatus == ON){
    serialPort.print("STS>Moved to Silo:"); 
    serialPort.print(SILO_NAME[currentSilo]);
    serialPort.println();
  }

  //processDioWriteEvent();
}

//-------------------------------------------------------------
// Move to the next AUTO sweep pattern
//-------------------------------------------------------------
void nextSweepPatternAuto(){
  sweepArrayPatternNbrAutoListPos++;
  if (sweepArrayPatternNbrAutoListPos >= sweepArrayPatternNbrAutoListSize)
    sweepArrayPatternNbrAutoListPos = 0;

  sweepArrayPatternNbrAuto = sweepArrayPatternNbrAutoList[sweepArrayPatternNbrAutoListPos];

  if (sweepArrayPatternNbrAuto > sweepArrayPatternsSize - 1){
    sweepArrayPatternNbrAuto = 0;
  }
  
  if ((logSweepStatus == ON) || (logSiloStatus == ON)){
    serialPort.print("STS>Cycling to next AUTO pattern:[");
    serialPort.print(sweepArrayPatternNbrAuto);
    serialPort.print("]\n");
  }
  loadSweepArray(sweepArrayPatternNbrAuto, invertCh2Auto);
  getStartSweep();  // Reset the sweep pattern to the start
}

//-------------------------------------------------------------
// Move to the next LOCAL sweep pattern
//-------------------------------------------------------------
void nextSweepPatternLocal(){
  sweepArrayPatternNbrLocalListPos++;
  if (sweepArrayPatternNbrLocalListPos >= sweepArrayPatternNbrLocalListSize)
    sweepArrayPatternNbrLocalListPos = 0;

  sweepArrayPatternNbrLocal = sweepArrayPatternNbrLocalList[sweepArrayPatternNbrLocalListPos];

  if (sweepArrayPatternNbrLocal > sweepArrayPatternsSize - 1){
    sweepArrayPatternNbrLocal = 0;
  }
  if ((logSweepStatus == ON) || (logSiloStatus == ON)){
    serialPort.print("STS>Cycling to next LOCAL pattern:[");
    serialPort.print(sweepArrayPatternNbrLocal);
    serialPort.print("]\n");
  }
  loadSweepArray(sweepArrayPatternNbrLocal, invertCh2Local);
  getStartSweep();  // Reset the sweep pattern to the start
}

//-------------------------------------------------------------
// Search for devices on the I2C bus
//-------------------------------------------------------------
void scanI2C(){
  byte error, address;
  int nDevices;
 
  serialPort.println("Scanning...");
  delay(10);

  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    DAC_I2C_WIRE.beginTransmission(address);
    error = DAC_I2C_WIRE.endTransmission();
 
    if (error == 0)
    {
      serialPort.print("I2C device found at address 0x");
      if (address<16)
        serialPort.print("0");
      serialPort.print(address,HEX);
      serialPort.println("  !");
      delay(10);
      nDevices++;
    }
    else if (error==4)
    {
      serialPort.print("Unknown error at address 0x");
      if (address<16)
        serialPort.print("0");
      serialPort.println(address,HEX);
      delay(10);
    }    
  }
  if (nDevices == 0)
    serialPort.println("No I2C devices found\n");
  else
    serialPort.println("done\n");
  delay(10);
}

//-------------------------------------------------------------
// Test function for the DACs
//-------------------------------------------------------------
void testI2cDac(){
  unsigned int a = 0;
  unsigned int b = 0;
  unsigned int c = 125;

  while (1){
    a=0;
    while (a<100){
      b=c-a;
      serialPort.print("DAC at 0x60 value:"); serialPort.print(a);
      DAC_I2C_WIRE.beginTransmission(DAC_ADDR1);
      DAC_I2C_WIRE.write(DAC_CMD_WRITEDAC);
      DAC_I2C_WIRE.write((uint8_t)a);
#ifdef DAC_MCP4725
      DAC_I2C_WIRE.write((uint8_t)0x0);  // bottom four bits are 0x0
#endif
      DAC_I2C_WIRE.endTransmission();
      delay(20);
  
      serialPort.print("  -  DAC at 0x61 value:"); serialPort.println(b);
      DAC_I2C_WIRE.beginTransmission(DAC_ADDR2);
      DAC_I2C_WIRE.write(DAC_CMD_WRITEDAC);
      DAC_I2C_WIRE.write((uint8_t)b);
#ifdef DAC_MCP4725
      DAC_I2C_WIRE.write((uint8_t)0x0);  // bottom four bits are 0x0
#endif
      DAC_I2C_WIRE.endTransmission();
      delay(20);
  
      a+=5;
      if (a>c){
        a=0;
      }
      delay(10);
    }
  }
}

//-------------------------------------------------------------
// Log PIN assignments to the serial console
//-------------------------------------------------------------
void logPinAssignments(){
//--------------------------------------------
// Testing PINs
//--------------------------------------------
  unsigned int minPin;
  unsigned int maxPin;
  unsigned int a;

// INPUT PINs
  serialPort.println("Digital Input Switch PIN definitions.");
  serialPort.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
  serialPort.print("SW_MODE_LOCAL_PIN \t\t ");  serialPort.print(SW_MODE_LOCAL_PIN_NAME); serialPort.print(" \t"); serialPort.println(SW_MODE_LOCAL_PIN);
  serialPort.print("SW_MODE_AUTO_PIN \t\t ");   serialPort.print(SW_MODE_AUTO_PIN_NAME);  serialPort.print(" \t"); serialPort.println(SW_MODE_AUTO_PIN);
  serialPort.print("SW_VIB_1_PIN \t\t\t ");     serialPort.print(SW_VIB_1_PIN_NAME);      serialPort.print(" \t"); serialPort.println(SW_VIB_1_PIN);
  serialPort.print("SW_VIB_2_PIN \t\t\t ");     serialPort.print(SW_VIB_2_PIN_NAME);      serialPort.print(" \t"); serialPort.println(SW_VIB_2_PIN);
  serialPort.println();

  serialPort.println("Digital Input Control PIN definitions.");
  serialPort.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("SW_SILO_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t\t\t ");
    serialPort.print(SW_SILO_PIN_NAME[a]);
    serialPort.print("\t");
    serialPort.print(SW_SILO_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("SW_CHUTE_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t\t ");
    serialPort.print(SW_CHUTE_PIN_NAME[a]);
    serialPort.print("\t");
    serialPort.print(SW_CHUTE_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("SW_CHUTE_LOCK_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t\t ");
    serialPort.print(SW_CHUTE_LOCK_PIN_NAME[a]);
    serialPort.print("\t");
    serialPort.print(SW_CHUTE_LOCK_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  
// OUTPUT PINs
  serialPort.println("Digital Output LED PIN definitions.");
  serialPort.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
  serialPort.print("CPU_LED_PIN_2   \t\t\t "); serialPort.print(CPU_LED_PIN_2_NAME);   serialPort.print(" \t"); serialPort.println(CPU_LED_PIN_2);
  serialPort.print("CPU_LED_PIN_3   \t\t\t "); serialPort.print(CPU_LED_PIN_3_NAME);   serialPort.print(" \t"); serialPort.println(CPU_LED_PIN_3);
  serialPort.print("CPU_LED_PIN_4   \t\t\t "); serialPort.print(CPU_LED_PIN_4_NAME);   serialPort.print(" \t"); serialPort.println(CPU_LED_PIN_4);
  serialPort.print("CPU_LED_PIN_1   \t\t\t "); serialPort.print(CPU_LED_PIN_1_NAME);   serialPort.print(" \t"); serialPort.println(CPU_LED_PIN_1);
  serialPort.print("LOCAL_LED_PIN \t\t\t "); serialPort.print(LOCAL_LED_PIN_NAME); serialPort.print(" \t"); serialPort.println(LOCAL_LED_PIN);
  serialPort.print("AUTO_LED_PIN  \t\t\t "); serialPort.print(AUTO_LED_PIN_NAME);  serialPort.print(" \t"); serialPort.println(AUTO_LED_PIN);
  serialPort.print("VIB_LED_1_PIN \t\t\t "); serialPort.print(VIB_LED_1_PIN_NAME); serialPort.print(" \t"); serialPort.println(VIB_LED_1_PIN);
  serialPort.print("VIB_LED_2_PIN \t\t\t "); serialPort.print(VIB_LED_2_PIN_NAME); serialPort.print(" \t"); serialPort.println(VIB_LED_2_PIN);
  serialPort.println();
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("SILO_LED_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t\t ");
    serialPort.print(SILO_LED_PIN_NAME[a]);
    serialPort.print("\t");
    serialPort.print(SILO_LED_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  serialPort.println("Digital Output AIR PIN definitions.");
  serialPort.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
  serialPort.print("AIR_SOL_PIN   \t\t\t "); serialPort.print(AIR_SOL_PIN_NAME);   serialPort.print(" \t"); serialPort.println(AIR_SOL_PIN);
  serialPort.print("AIR_REG_1_PIN \t\t\t "); serialPort.print(AIR_REG_1_PIN_NAME); serialPort.print(" \t"); serialPort.println(AIR_REG_1_PIN);
  serialPort.print("AIR_REG_2_PIN \t\t\t "); serialPort.print(AIR_REG_2_PIN_NAME); serialPort.print(" \t"); serialPort.println(AIR_REG_2_PIN);
  serialPort.println();
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("AIR_MANIFOLD_1_PICK_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t ");
    serialPort.print(AIR_MANIFOLD_1_PICK_PIN_NAME[a]);
    serialPort.print(" \t");
    serialPort.print(AIR_MANIFOLD_1_PICK_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("AIR_MANIFOLD_1_HOLD_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t ");
    serialPort.print(AIR_MANIFOLD_1_HOLD_PIN_NAME[a]);
    serialPort.print(" \t");
    serialPort.print(AIR_MANIFOLD_1_HOLD_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("AIR_MANIFOLD_2_PICK_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t ");
    serialPort.print(AIR_MANIFOLD_2_PICK_PIN_NAME[a]);
    serialPort.print(" \t");
    serialPort.print(AIR_MANIFOLD_2_PICK_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  for (a=0;a<NBR_SILOS;a++){
    serialPort.print("AIR_MANIFOLD_2_HOLD_PIN["); 
    serialPort.print(a);
    serialPort.print("] \t ");
    serialPort.print(AIR_MANIFOLD_2_HOLD_PIN_NAME[a]);
    serialPort.print(" \t");
    serialPort.print(AIR_MANIFOLD_2_HOLD_PIN[a]);
    serialPort.print("\t Silo:");
    serialPort.println(SILO_NAME[a]);
  }
  serialPort.println();
  return;
}

//-------------------------------------------------------------
// Test digital output pins
//-------------------------------------------------------------
void testPins(){
  a=PC6;
  pinMode(a, OUTPUT);
  pinMode(CPU_LED_PIN_2, OUTPUT);
  pinMode(CPU_LED_PIN_3, OUTPUT);
  while (1){
    serialPort.println("LED goes ON");
    digitalWrite(CPU_LED_PIN_2, LOW);
    digitalWrite(CPU_LED_PIN_3, LOW);
    digitalWrite(a, LOW);
    delay(500);

    serialPort.println("LED goes OFF");
    digitalWrite(CPU_LED_PIN_2, HIGH);
    digitalWrite(CPU_LED_PIN_3, HIGH);
    digitalWrite(a, HIGH);
    delay(500);
  }
}

//-------------------------------------------------------------
// Test the Pick&Hold opperation
//-------------------------------------------------------------
void testPickAndHold(){
  int a = 0;
  while (1){
    for (a=0;a<6;a++){
      serialPort.print("AIR_MANIFOLD_1_PICK_PIN["); 
      serialPort.print(a);
      serialPort.print("] \t ");
      serialPort.print(AIR_MANIFOLD_1_PICK_PIN_NAME[a]);
      serialPort.print(" \t");
      serialPort.print(AIR_MANIFOLD_1_PICK_PIN[a]);
      serialPort.print("\t Silo:");
      serialPort.println(SILO_NAME[a]);
      digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], OFF);
      digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], ON);
      delay(2000);
      
      serialPort.print("AIR_MANIFOLD_1_HOLD_PIN["); 
      serialPort.print(a);
      serialPort.print("] \t ");
      serialPort.print(AIR_MANIFOLD_1_HOLD_PIN_NAME[a]);
      serialPort.print(" \t");
      serialPort.print(AIR_MANIFOLD_1_HOLD_PIN[a]);
      serialPort.print("\t Silo:");
      serialPort.println(SILO_NAME[a]);
      digitalWrite(AIR_MANIFOLD_1_PICK_PIN[a], OFF);
      digitalWrite(AIR_MANIFOLD_1_HOLD_PIN[a], ON);
      delay(2000);
  
      serialPort.print("Silo:");
      serialPort.print(SILO_NAME[a]);
      serialPort.println(":RELEASE");
      delay(2000);
    }
  }  
}




/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/
/*
#include <Arduino.h>

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  Serial.println("Hello");           // Test Serial print line
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  Serial.println("Worldd \r\n");     // Test Serial print line
  delay(1000);                       // wait for a second
}
*/