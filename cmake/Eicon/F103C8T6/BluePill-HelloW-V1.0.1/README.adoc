# CMakeArduino Project Template

Arduino-CMake Build System for http://www.eicon-vic.com/[Eicon(Vic) Pty Ltd] Boards DinRDuino etc  +
Eicon (Vic) PTY Ltd are located https://www.google.com/search?client=firefox-b-d&q=Eicon+vic[here]


# Introduction

This repo holds a Master Template for creating a new Arduino CMake project build system  +
Development environment is on Windows and is on a second Drive Partition and set up as Drive W:   +
Arduino is installed to W:\arduino-1.8.9 and STM32 core is installed to W:\arduino-1.8.9\portable\packages   +
See https://github.com/stm32duino/wiki/wiki/Getting-Started[Getting Started] for instructions on installing Arduino and STM32 cores  +

This repo is cloned by the "createproject bash scriptfor creating new Arduino CMake projects see https://gitlab.com/noeldiviney/createproject[CreateProject] for details  +

# Creating the Gitlab ProjectTemplate repository

Login to https://gitlab.com  +
Click the "new" button located on the Right Hand side of the menu bar and select "new project"  +
Name the project "ProjectTemplate" ans set visibilit to "Private" +
Leave the project blank for now  +
Press "Create project" button to create  +

#  Cloning the "ProjectTemplate" to Local Drive W:

Launch Msys64 terminal and execute the following cmdline commands  +
....
	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab
    $: cd /mnt/w/Dev/Gitlab
    $: git clone https://gitlab.com/username/ProjectTemplate.git
	Cloning into 'testtemplate'...
	Username for 'https://gitlab.com':
	Password for 'https://noeldiviney@gitlab.com':
	warning: You appear to have cloned an empty repository
	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab
	$: cd ProjectTemplate
 
 	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ ls -al
	total 12
	drwxrwxr-x+ 1 noel None 0 Aug 19 18:28 .
	drwxrwx---+ 1 noel None 0 Aug 19 17:52 ..
	drwxrwxr-x+ 1 noel None 0 Aug 19 17:52 .git
 
 	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
....
#  Updating the Gitlab Local Project with required content

Add the following files and directories to the Cloned ProjectTemplate  +
see the https://gitlab.com/noeldiviney/ArduinoCMakeFiles[ArduinoCMakeFiles] for more info  +

	.gitignore
	CMake
	CMakeLists.txt
	project.cpp
	project.ino
	README.adoc
....
	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ ls -al
	total 33
	drwxrwxr-x+ 1 noel None    0 Aug 19 18:20 .
	drwxrwx---+ 1 noel None    0 Aug 19 17:52 ..
	drwxrwxr-x+ 1 noel None    0 Aug 19 17:52 .git
	-rwxrwxr-x+ 1 noel None  438 Aug 19 16:16 .gitignore
	drwxrwxr-x+ 1 noel None    0 Aug 19 18:19 CMake
	-rwxrwxr-x+ 1 noel None 2839 Aug 19 16:16 CMakeLists.txt
	-rwxrwxr-x+ 1 noel None 1248 Aug 19 16:16 project.cpp
	-rwxrwxr-x+ 1 noel None 1248 Aug 19 16:16 project.ino
	-rwxrwxr-x+ 1 noel None 2240 Aug 19 16:16 README.adoc

	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
....

# Creating the project.tar.bz2 Tarball

Execute the following bash commands to create the tarball, sha256 crc and size  +
....
        noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
        $ rm -rf project.tar.bz2

        noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
        $ tar --exclude=project.tar.bz2 -cjvf project.tar.bz2 * .gitignore
        a CMake
        a CMake/ArduinoStm32Lib.cmake
        a CMake/Message-1.cmake
        a CMake/Message-2.cmake
        a CMake/Message-3.cmake
        a CMake/Message-4.cmake
        a CMake/Modules
        a CMake/STM32Toolchain.cmake
        a CMake/Modules/FindOpenocd.cmake
        a CMakeLists.txt
        a project.cpp
        a README.adoc
        a .gitignore
    
        noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
        $ sha256deep64.exe project.tar.bz2
        bbcb0d6da0cfaa634075722126079ce897fb2a95aa4c59f49acdfc4c5e18df4d  W:\Dev\Gitlab\projecttemplate\project.tar.bz2
        noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
        $ size --target=binary project.tar.bz2
        text    data     bss     dec     hex filename
            0    7435       0    7435    1d0b project.tar.bz2....
#  Committing the Changes
....
    noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ ls -al
	total 41
	drwxrwxr-x+ 1 noel None    0 Aug 19 18:41 .
	drwxrwx---+ 1 noel None    0 Aug 19 17:52 ..
	drwxrwxr-x+ 1 noel None    0 Aug 19 17:52 .git
	-rwxrwxr-x+ 1 noel None  438 Aug 19 16:16 .gitignore
	drwxrwxr-x+ 1 noel None    0 Aug 19 18:38 CMake
	-rwxrwxr-x+ 1 noel None 2839 Aug 19 16:16 CMakeLists.txt
	-rwxrwxr-x+ 1 noel None 1248 Aug 19 16:16 project.cpp
	-rwxrwxr-x+ 1 noel None 1248 Aug 19 16:16 project.ino
	-rwxrwxr-x+ 1 noel None 5850 Aug 19 18:41 project.tar.bz2
	-rwxrwxr-x+ 1 noel None 2240 Aug 19 16:16 README.adoc

	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git status
	On branch master

	No commits yet

	Untracked files:
	  (use "git add <file>..." to include in what will be committed)

        .gitignore
        CMake/
        CMakeLists.txt
        README.adoc
        project.cpp
        project.ino
        project.tar.bz2

	nothing added to commit but untracked files present (use "git add" to track)

	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git add * .gitignore

	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git status
	On branch master
	Your branch is up to date with 'origin/master'.

	Changes to be committed:
	  (use "git reset HEAD <file>..." to unstage)

        new file:   .gitignore
        new file:   CMake/ArduinoStm32Lib.cmake
        new file:   CMake/Message-1.cmake
        new file:   CMake/Message-2.cmake
        new file:   CMake/Message-3.cmake
        new file:   CMake/Message-4.cmake
        new file:   CMake/Modules/FindOpenocd.cmake
        new file:   CMake/STM32Toolchain.cmake
        new file:   CMakeLists.txt
        new file:   README.adoc
        new file:   project.cpp
        new file:   project.ino

	Changes not staged for commit:
 	 (use "git add/rm <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)

	        deleted:    README.md

	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git commit -m "initial commit"
	[master (root-commit) 2cc755b] initial commit
	12 files changed, 512 insertions(+)
 	create mode 100755 .gitignore
 	create mode 100755 CMake/ArduinoStm32Lib.cmake
 	create mode 100755 CMake/Message-1.cmake
 	create mode 100755 CMake/Message-2.cmake
 	create mode 100755 CMake/Message-3.cmake
 	create mode 100755 CMake/Message-4.cmake
 	create mode 100755 CMake/Modules/FindOpenocd.cmake
 	create mode 100755 CMake/STM32Toolchain.cmake
 	create mode 100755 CMakeLists.txt
 	create mode 100755 README.adoc
 	create mode 100755 project.cpp
 	create mode 100755 project.ino
....
#  Pushing the commits to Gitlab.cpm
....
	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git status
	On branch master
	Your branch is ahead of 'origin/master' by 1 commit.
	  (use "git push" to publish your local commits)

	Changes not staged for commit:
	  (use "git add/rm <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)

	        deleted:    README.md

	no changes added to commit (use "git add" and/or "git commit -a")
	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git push
	fatal: The current branch master has no upstream branch.
	To push the current branch and set the remote as upstream, use

	    git push --set-upstream origin master


	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git push
	Username for 'https://gitlab.com':
	Password for 'https://noeldiviney@gitlab.com':
	Enumerating objects: 16, done.
	Counting objects: 100% (16/16), done.
	Delta compression using up to 2 threads
	Compressing objects: 100% (14/14), done.
	Writing objects: 100% (15/15), 7.61 KiB | 1.09 MiB/s, done.
	Total 15 (delta 1), reused 0 (delta 0)
	To https://gitlab.com/noeldiviney/testtemplate.git
	   cf69a49..8fed024  master -> master

	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$ git status
	On branch master
	Your branch is up to date with 'origin/master'.

	nothing to commit, working tree clean

	noel@DESKTOP-0AFLL5T MSYS /mnt/w/Dev/Gitlab/ProjectTemplate
	$
....
#  All done
