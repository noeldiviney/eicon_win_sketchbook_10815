#---------------  Add Executeable and Linker Definitions  ---------------------------------------------------------------------#
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   ---------------------------------------------------------------------------------")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   CMAKE_LINKER_FLAGS           = ${CMAKE1_LINKER_FLAGS}")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   Add Executeable")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   Add executeable              = (${PROJECT_NAME}.elf SOURCE_FILES)")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   target_link_libraries          (${PROJECT_NAME}.elf arduinolib)")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   CMAKE_LINKER_FLAGS             ${CMAKE_EXE_LINKER_FLAGS}")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   set(HEX_FILE                 = ${PROJECT_SOURCE_DIR}/build/${PROJECT_NAME}.hex)")
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}   set(BIN_FILE                 = ${PROJECT_SOURCE_DIR}/build/${PROJECT_NAME}.bin)")
#---------------  Add Post Commands Definitions  ------------------------------------------------------------------------------#
