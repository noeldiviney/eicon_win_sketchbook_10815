# the name of the target operating system
if (_IS_TOOLCHAIN_PROCESSED)
    return()
endif ()
set(_IS_TOOLCHAIN_PROCESSED True)


set(CMAKE_SYSTEM_NAME      Generic)
set(CMAKE_SYSTEM_VERSION   1)
set(CMAKE_SYSTEM_PROCESSOR ARM)
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    ---------------------------------------------------------------------------------")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    Entering                         STM32Toolchain.cmake")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_SYSTEM_NAME              = ${CMAKE_SYSTEM_NAME}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_SYSTEM_VERSION           = ${CMAKE_SYSTEM_VERSION}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_SYSTEM_PROCESSOR         = ${CMAKE_SYSTEM_PROCESSOR}")

# which compilers to use for C and C++
set(CMAKE_C_COMPILER   ${STM32_TOOLCHAIN_PATH}/arm-none-eabi-gcc${GCC_SUFFIX}      CACHE PATH "" FORCE)
set(CMAKE_CXX_COMPILER ${STM32_TOOLCHAIN_PATH}/arm-none-eabi-g++${GCC_SUFFIX}      CACHE PATH "" FORCE)
set(CMAKE_ASM_COMPILER ${STM32_TOOLCHAIN_PATH}/arm-none-eabi-gcc${GCC_SUFFIX}      CACHE PATH "" FORCE)
set(CMAKE_OBJCOPY      ${STM32_TOOLCHAIN_PATH}/arm-none-eabi-objcopy${GCC_SUFFIX}  CACHE PATH "" FORCE)
set(CMAKE_OBJDUMP      ${STM32_TOOLCHAIN_PATH}/arm-none-eabi-objdump${GCC_SUFFIX}  CACHE PATH "" FORCE)
set(CMAKE_AR           ${STM32_TOOLCHAIN_PATH}/arm-none-eabi-ar${GCC_SUFFIX}       CACHE PATH "" FORCE)

MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_COMPILER               = ${CMAKE_C_COMPILER}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_SXX_COMPILER             = ${CMAKE_CXX_COMPILER}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_COMPILER             = ${CMAKE_ASM_COMPILER}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_OBJCOPY                  = ${CMAKE_OBJCOPY}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_OBJDUMP                  = ${CMAKE_OBJDUMP}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_AR                       = ${CMAKE_AR}")

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_TRY_COMPILE_TARG_TYPE    = ${CMAKE_TRY_COMPILE_TARGET_TYPE}")

# core flags
if(${MCU_FAMILY} STREQUAL STM32F4xx)
	set(CORTEX_FLAGS "-mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard")
elseif(${MCU_FAMILY} STREQUAL STM32F1xx)
	set(CORTEX_FLAGS "-mcpu=cortex-m3 -mfloat-abi=soft")
else()
	MESSAGE(FATAL "Stm32 Family type not set")
endif()

set(CORE_FLAGS "${CORTEX_FLAGS} -mthumb -mthumb-interwork -mlittle-endian --specs=nano.specs --specs=nosys.specs ${ADDITIONAL_CORE_FLAGS}")

MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CORE_FLAGS                     = ${CORTEX_FLAGS} -mthumb -mlittle-endian -mthumb-interwork")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CORE_FLAGS CONTINUED           = --specs=nano.specs --specs=nosys.specs ${ADDITIONAL_CORE_FLAGS}")

# compiler: language specific flags
set(CMAKE_C_FLAGS "${CORE_FLAGS} -fno-builtin -Wall -std=gnu99 -fdata-sections -ffunction-sections -g3 -gdwarf-2" CACHE INTERNAL "c compiler flags")
set(CMAKE_C_FLAGS_DEBUG "" CACHE INTERNAL "c compiler flags: Debug")
set(CMAKE_C_FLAGS_RELEASE "" CACHE INTERNAL "c compiler flags: Release")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS  CORE_FLAGS1     = ${CORTEX_FLAGS} -mthumb -mlittle-endian -mthumb-interwork")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS  CORE_FLAGS2     = --specs=nano.specs --specs=nosys.specs ${ADDITIONAL_CORE_FLAGS}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS  C_FLAGS         = -fno-builtin -Wall -std=gnu99 -fdata-sections -ffunction-sections -g3 -gdwarf-2")


set(CMAKE_CXX_FLAGS "${CORE_FLAGS} -fno-rtti -fno-exceptions -fno-builtin -Wall -std=gnu++11 -fdata-sections -ffunction-sections -g -ggdb3" CACHE INTERNAL "cxx compiler flags")
set(CMAKE_CXX_FLAGS_DEBUG "" CACHE INTERNAL "cxx compiler flags: Debug")
set(CMAKE_CXX_FLAGS_RELEASE "" CACHE INTERNAL "cxx compiler flags: Release")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  CORE_FLAGS1   = ${CORTEX_FLAGS} -mthumb -mlittle-endian -mthumb-interwork")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  CORE_FLAGS2   = --specs=nano.specs --specs=nosys.specs ${ADDITIONAL_CORE_FLAGS}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  C_FLAGS       = -fno-rtti -fno-exceptions -fno-builtin -Wall -std=gnu++11 -fdata-sections -ffunction-sections -g -ggdb3")

set(CMAKE_ASM_FLAGS "${CORE_FLAGS} -g -ggdb3 -D__USES_CXX" CACHE INTERNAL "asm compiler flags")
set(CMAKE_ASM_FLAGS_DEBUG "" CACHE INTERNAL "asm compiler flags: Debug")
set(CMAKE_ASM_FLAGS_RELEASE "" CACHE INTERNAL "asm compiler flags: Release")


MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  CORE_FLAGS1   = ${CORTEX_FLAGS} -mthumb -mlittle-endian -mthumb-interwork")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  CORE_FLAGS2   = --specs=nano.specs --specs=nosys.specs ${ADDITIONAL_CORE_FLAGS}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  C_FLAGS       = -g -ggdb3 -D__USES_CXX")

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# find additional toolchain executables
find_program(ARM_SIZE_EXECUTABLE arm-none-eabi-size)
find_program(ARM_GDB_EXECUTABLE arm-none-eabi-gdb)
find_program(ARM_OBJCOPY_EXECUTABLE arm-none-eabi-objcopy)
find_program(ARM_OBJDUMP_EXECUTABLE arm-none-eabi-objdump)
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    Leaving                          STM32Toolchain.cmake")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    ---------------------------------------------------------------------------------")
