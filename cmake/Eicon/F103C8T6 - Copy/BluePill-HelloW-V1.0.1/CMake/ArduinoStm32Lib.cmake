SET(CCLL                                "CMAKE_CURRENT_LIST_LINE")

#---  project Definition  ---#
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}      --------------------------------------------------------------------------------------------------------------------------------------------")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}      Processing                     = ArduinoStm32Lib.cmake")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}      MCU_LINKER_SCRIPT              = ${MCU_LINKER_SCRIPT}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}      ARDUINO_VERSION                = ${ARDUINO_VERSION}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}      CMAKE_C_COMPILER               = ${CMAKE_C_COMPILER}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}      CMAKE_CXX_COMPILER             = ${CMAKE_CXX_COMPILER}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     objcopy                        = ${CMAKE_OBJCOPY}")

#--- Add Definitions  ---#
# -DSTM32F4xx -DARDUINO=10812 -DARDUINO_F427ZIT6_DRD_Blink -DARDUINO_ARCH_STM32 "-DBOARD_NAME=\"F427ZIT6_DRD_Blink\"" -DSTM32F427xx -DHAL_UART_MODULE_ENABLED

add_definitions(${DEFINITION_1})
add_definitions(${DEFINITION_2})
add_definitions(${DEFINITION_3})
add_definitions(${DEFINITION_4})
add_definitions(${DEFINITION_5})
add_definitions(${DEFINITION_6})
add_definitions(${DEFINITION_7})
add_definitions(${DEFINITION_8})

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     ---")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     Add CMake Definitions with the following CMake commands")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_1})")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_2})")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_3})")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_4})") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_5})") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_6})") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_7})") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     add_definitions(${DEFINITION_8})") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     End of the Definitions")


#---  Add Include Directories  ---#

#include_directories(${SKETCH_PATH})
include_directories(${SKETCH_PATH}/${PROJECT_NAME})
include_directories(${STM32_CORE_PATH}/variants/${MCU_FAMILY}/${CPU}_${BOARD_NAME}_${SKETCH_NAME})
include_directories(${STM32_CORE_PATH}/cores/arduino)
include_directories(${STM32_CORE_PATH}/cores/arduino/avr)
include_directories(${STM32_CORE_PATH}/cores/arduino/stm32)
include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/LL)
include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/usb)
include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/usb/hid)
include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/usb/cdc)
include_directories(${STM32_CORE_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Inc/)
include_directories(${STM32_CORE_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Src/)
include_directories(${STM32_CORE_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Include/)
include_directories(${STM32_CORE_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Source/Templates/gcc/)
include_directories(${STM32_CORE_PATH}/system/${MCU_FAMILY}/)
include_directories(${STM32_CORE_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Inc)
include_directories(${STM32_CORE_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Src)
include_directories(${STM32_CORE_PATH}/libraries/SrcWrapper/src)
include_directories(${CORE_PACKAGER_PATH}/${BOARD_VENDOR}/tools/CMSIS/CMSIS/Core/Include/)
include_directories(${CORE_PACKAGER_PATH}/${BOARD_VENDOR}/tools/CMSIS/CMSIS/DSP/Include/)

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     ---")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     Add Include Directories with the following CMake commands")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${SKETCH_PATH}/${PROJECT_NAME})")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/variants/${MCU_FAMILY}/${CPU}_${BOARD_NAME}_${SKETCH_NAME}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/cores/arduino)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/cores/arduino/avr)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/cores/arduino/stm32)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/LL)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/usb)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/usb/hid)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/cores/arduino/stm32/usb/cdc)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Inc/)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Src/)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Include/)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Source/Templates/gcc/)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/system/${MCU_FAMILY}/)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Inc)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Src)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${STM32_CORE_PATH}/libraries/SrcWrapper/src)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${CORE_PACKAGER_PATH}/${BOARD_VENDOR}/tools/CMSIS/CMSIS/Core/Include/)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     include_directories(${CORE_PACKAGER_PATH}/${BOARD_VENDOR}/tools/CMSIS/CMSIS/DSP/Include/)")


#--- Add Sources  ---#

#file(GLOB_RECURSE SKETCH_CPP_SOURCES 		    ${SKETCH_PATH}/*.cpp)
file(GLOB_RECURSE SKETCH_CPP_SOURCES 		    C:/DinRDuino/arduino-1.8.18/portable/sketchbook/cmake/Eicon/F103C8T6/BluePill-HelloW-V1.0.1/${PROJECT_NAME}.cpp)

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     SKETCH_PATH                   =  ${SKETCH_PATH}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     SKETCH_CPP_SOURCES            =  ${SKETCH_CPP_SOURCES}")

file(GLOB_RECURSE VARIANTS_CPP_SOURCES    	    ${STM32_CORE_PATH}/variants/${MCU_FAMILY}/${CPU}_${BOARD_NAME}_${SKETCH_NAME}/*.cpp)
file(GLOB_RECURSE VARIANTS_C_SOURCES    	    ${STM32_CORE_PATH}/variants/${MCU_FAMILY}/${CPU}_${BOARD_NAME}_${SKETCH_NAME}/*.c)

file(GLOB_RECURSE STARTUP_SOURCES               ${STM32_CORE_PATH}/cores/arduino/stm32/*.S)
file(GLOB_RECURSE ARDUINO_C_SOURCES    		    ${STM32_CORE_PATH}/cores/arduino/*.c)
file(GLOB_RECURSE ARDUINO_CPP_SOURCES    	    ${STM32_CORE_PATH}/cores/arduino/*.cpp)
file(GLOB_RECURSE AVR_C_SOURCES    		        ${STM32_CORE_PATH}/cores/arduino/avr/*.c)
file(GLOB_RECURSE STM32_USB_C_SOURCES    	    ${STM32_CORE_PATH}/cores/arduino/stm32/usb/*.c)
file(GLOB_RECURSE STM32_USB_HID_C_SOURCES       ${STM32_CORE_PATH}/cores/arduino/stm32/usb/hid/*.c)
file(GLOB_RECURSE STM32_USB_CDC_C_SOURCES       ${STM32_CORE_PATH}/cores/arduino/stm32/usb/cdc/*.c)
file(GLOB_RECURSE SRCWRAPPER_C_SOURCES          ${STM32_CORE_PATH}/libraries/SrcWrapper/*.c)
file(GLOB_RECURSE SRCWRAPPER_HAL_C_SOURCES    	${STM32_CORE_PATH}/libraries/SrcWrapper/src/HAL/*.c)
file(GLOB_RECURSE SRCWRAPPER_LL_C_SOURCES    	${STM32_CORE_PATH}/libraries/SrcWrapper/src/LL/*.c)
file(GLOB_RECURSE SRCWRAPPER_STM32_C_SOURCES    ${STM32_CORE_PATH}/libraries/SrcWrapper/src/stm32/*.c)
file(GLOB_RECURSE SRCWRAPPER_STM32_CPP_SOURCES  ${STM32_CORE_PATH}/libraries/SrcWrapper/src/stm32/*.cpp)


MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ---")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Add Source to be compiled with the following CMake commands")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE SKETCH_CPP_SOURCES            ${SKETCH_PATH}/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE VARIANTS_CPP_SOURCES          ${STM32_CORE_PATH}/variants/${MCU_FAMILY}/${CPU}_${BOARD_NAME}_${SKETCH_NAME}/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE VARIANTS_C_SOURCES            ${STM32_CORE_PATH}/variants/${MCU_FAMILY}/${CPU}_${BOARD_NAME}_${SKETCH_NAME}/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE STARTUP_SOURCES               ${STM32_CORE_PATH}/cores/arduino/stm32/*.S)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE ARDUINO_C_SOURCES             ${STM32_CORE_PATH}/cores/arduino/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE ARDUINO_CPP_SOURCES           ${STM32_CORE_PATH}/cores/arduino/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE AVR_C_SOURCES                 ${STM32_CORE_PATH}/cores/arduino/avr/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE STM32_USB_C_SOURCES           ${STM32_CORE_PATH}/cores/arduino/stm32/usb/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE STM32_USB_HID_C_SOURCES       ${STM32_CORE_PATH}/cores/arduino/stm32/usb/hid/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE STM32_USB_CDC_C_SOURCES       ${STM32_CORE_PATH}/cores/arduino/stm32/usb/cdc/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE SRCWRAPPER_C_SOURCES          ${STM32_CORE_PATH}/libraries/SrcWrapper/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE SRCWRAPPER_HAL_C_SOURCES      ${STM32_CORE_PATH}/libraries/SrcWrapper/src/HAL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE SRCWRAPPER_LL_C_SOURCES       ${STM32_CORE_PATH}/libraries/SrcWrapper/src/LL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE SRCWRAPPER_STM32_C_SOURCES    ${STM32_CORE_PATH}/libraries/SrcWrapper/src/LL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    file(GLOB_RECURSE SRCWRAPPER_STM32_CPP_SOURCES  ${STM32_CORE_PATH}/libraries/SrcWrapper/src/LL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ---")

set(ARDUINOLIB_SOURCES   ${SKETCH_CPP_SOURCES} ${VARIANTS_CPP_SOURCES} ${VARIANTS_C_SOURCES} ${STARTUP_SOURCES} ${ARDUINO_CPP_SOURCES} ${ARDUINO_C_SOURCES} ${AVR_C_SOURCES} ${STM32_USB_C_SOURCES} ${STM32_USB_HID_C_SOURCES})
set(ARDUINOLIB_SOURCES   ${ARDUINOLIB_SOURCES} ${STM32_USB_CDC_C_SOURCES} ${SRCWRAPPER_C_SOURCES} ${SRCWRAPPER_HAL_C_SOURCES} ${SRCWRAPPER_LL_C_SOURCES} ${SRCWRAPPER_STM32_C_SOURCES} ${SRCWRAPPER_STM32_CPP_SOURCES}) 

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ---")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    create ARDUINOLIB_SOURCES by combining all of the Library Sources")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ARDUINOLIB_SOURCES             = STARTUP_SOURCES VARIANTS_CPP_SOURCES ARDUINO_CPP_SOURCES VARIANTS_C_SOURCES ARDUINO_C_SOURCES ")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ARDUINOLIB_SOURCES contd       = AVR_C_SOURCES STM32_C_SOURCES ARDUINOLIB_SOURCES STM32_HAL_C_SOURCES STM32_LL_C_SOURCES ")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ARDUINOLIB_SOURCES contd       = STM32_USB_C_SOURCES STM32_USB_HID_C_SOURCES STM32_USB_CDC_C_SOURCES")

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    create APP_SOURCES by combining all of the App Sources and the Linker Script")
set(APP_SOURCES     ${SKETCH_CPP_SOURCES} ${MCU_LINKER_SCRIPT})
#file(GLOB_RECURSE  APP_SOURCES                 ${SKETCH_CPP_SOURCES} ${MCU_LINKER_SCRIPT})
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    SKETCH_CPP_SOURCES             = ${SKETCH_CPP_SOURCES} ")

#MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ARDUINOLIB_SOURCES                    = ${ARDUINOLIB_SOURCES} ")


#MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   App_SOURCES                    = ${MCU_LINKER_SCRIPT} ")


MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    All done                       = Arduino.cmake")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    --------------------------------------------------------------------------------------------------------------------------------------------")
 
#---------------  All Done  ---------------------------------------------------------------------------------------------------#

