# 1 "C:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\F427ZIT6_SerLed\\F427ZIT6_SerLed.ino"
/*

  Blink



  Turns an LED on for one second, then off for one second, repeatedly.



  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO

  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to

  the correct LED pin independent of which board is used.

  If you want to know what pin the on-board LED is connected to on your Arduino

  model, check the Technical Specs of your board at:

  https://www.arduino.cc/en/Main/Products



  modified 8 May 2014

  by Scott Fitzgerald

  modified 2 Sep 2016

  by Arturo Guadalupi

  modified 8 Sep 2016

  by Colby Newman



  This example code is in the public domain.



  http://www.arduino.cc/en/Tutorial/Blink

*/
# 24 "C:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\F427ZIT6_SerLed\\F427ZIT6_SerLed.ino"
# 25 "C:\\msys64\\opt\\arduino-1.8.13\\portable\\sketchbook\\arduino\\F427ZIT6_SerLed\\F427ZIT6_SerLed.ino" 2

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(33 /* LED_GREEN*/, 0x1);
  Serial3.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(33 /* LED_GREEN*/, 0x1); // turn the LED on (HIGH is the voltage level)
  Serial3.println("Hello"); // Test Serial print line
  delay(1000); // wait for a second
  digitalWrite(33 /* LED_GREEN*/, 0x0); // turn the LED off by making the voltage LOW
  Serial3.println("Worldd \r\n"); // Test Serial print line
  delay(1000); // wait for a second
}
